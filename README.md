About
==========
Many of the projects I work on for other companies can't be released into the wild at this time :( But I have decided to share an in-depth code sample that shows some PHP code, demonstrates a knowledge of design patterns (e.g. Front Controller pattern - see `index.php` and `.htaccess`), architectural patterns (e.g. MVC), routing strategies, and more.


I will supplement this code with more recent examples soon. Also, the UI presents a dummy course website, that was originally prototyped for a tutoring center in John's Creek, GA.


Note: this project makes use of [Scotch Box](https://github.com/scotch-io/scotch-box)


### Install

```bash
git clone git@bitbucket.org:mitchseymour/code-samples-php.git
cd code-samples-php
vagrant up
```

Then visit http://192.168.33.10/ to view the UI, and the `public` directory for the actual code.