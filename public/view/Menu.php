<?php
/**
 * Simple Menu class
 *
 * @author Mitch Seymour <me@mitchseymour.com>
 * @copyright Copyright (c) 2014, Mitch Seymour
 */

class Menu {

	private static $dbh;
	private static $cache;
	
	public static function getDatabase(){
	
		if (isset(self::$dbh))
			return self::$dbh;
			
		self::$dbh = Config::getDatabase('mcom_metrics');
		return self::$dbh;
	
	}

	/**
	 * Retrieves Menu from Cache
	 *
	 */

	public static function fetch($parent) {
		
		if (!$menu = Cache::get($parent))
			$menu = Menu::refreshMenu($parent);
			
		return $menu;

	}
	
	public static function refreshMenu($parent) {
		$dbh = self::getDatabase();
		
		$sql = "select m.parent, i.id, i.position, i.category, i.name, i.link
				from menu_items i, menu m
				where i.menu_id = m.menu_id
				and m.parent = :parent
				and i.active='1'
				order by i.category, i.position";
		
		$params = array('parent' => $parent);
		
		$results = $dbh->runQuery($sql, $params);
		
		foreach ($results as $row) {
			$menu[$row['category']][$row['name']] = $row['link'];
		}
        
        return Cache::set($parent,$menu);

	}
	
}


?>
