<?php
/**
 * Simple page class
 *
 * @author Mitch Seymour <me@mitchseymour.com>
 * @copyright Copyright (c) 2014, Mitch Seymour
 */

require_once('etc/parsers/Plugin.php');
require_once('etc/parsers/ThemeConfig.php');
require_once('util/Minify/CSSmin.php');
require_once('util/Minify/JSMin.php');

class Page {

	/**
	 * You can change these defaults,
	 * but please do not remove them
	 */
	 
	public $activePage            = '';
	public $adminOnly             = false;
	public $author                = '';
	public $cacheExpiration		  = 0;
	public $cacheOn				  = false;
	public $content               = '';
	public $contentLength         = 0;
	public $copyright             = '';
	public $copyrightWithDate     = '';
	public $css                   = array();
	public $cssPath               = 'www/css';
	public $debug				  = false;
	public $description           = '';
	public $headAppend			  = array();
	public $bodyAppend			  = array();
	public $js                    = array();
	public $jsPath                = 'www/js';
	public $keywords              = '';
	public $pathOverride          = '';
	public $pluginDependencies    = true;
	public $pluginBase            = '/www/';
	public $preventCaching        = false;
	public $redirect              = 'index';
	public $restrictedTo          = array();
	public $requireLogin          = false;
	public $robots                = true;
	public $siteName              = '';
	public $skeletonMode          = false;
	public $step				  = false;
	public $steps				  = array();
	public $stepCount			  = 0;
	public $trackingScripts       = array();
	public $theme                 = 'default';
	public $toClient              = array();
	public $tagLine               = '';
	public $themeSettings         = array();
	public $title                 = '';
	public $webAddress            = 'localhost';
	
	public static $capturedContent    = '';
	public static $beforeEnd = false;
	
	private static $user;
	private $limitContentBooleans = array();
	private $lastCodeType;

	/**
	 * Page initialization
	 *
	 * @param string $title
	 */

	public function __construct($title='') {
		
		$this->step("__construct");
		$this->title = $title;
		$this->activePage = $title;

		$this->webAddress = Config::WebAddress();

		if (substr($this->webAddress, -1) !== '/') {
			$this->webAddress .= '/';
		}

		$this->author            = Config::getDevelopers();
		$this->copyright         = Config::getCopyright();
		$this->copyrightWithDate = $this->copyright . ' ' . Date('Y', strtotime('now'));
		$this->siteName          = Config::getSiteName();
		$this->tagLine           = Config::getTagLine();
		$this->trackingScripts   = Config::getTrackingScripts();
		$this->contactEmail      = Config::getContactEmail();
		
		foreach ($this->trackingScripts as $t)
			$this->appendToBody("<script>" . strtr($t, array('<script>' => '', '</script>' => '')) . "</script>");

		// Add default stylesheets and javascript
		$settings   = Config::getSettings('Application');
		$settings   = $settings[0];
		$css        = $settings['defaults']['stylesheets'];
		$js         = $settings['defaults']['javascripts'];
		$plugins    = $settings['defaults']['plugins'];

		// get cache settings
		$this->preventCaching = $settings['preventCaching'];

		// Default plugins
		foreach ($plugins as $plugin)
			$this->addPlugin($plugin);

		// Default css
		foreach ($css as $stylesheet)
			$this->addCSS($stylesheet);

		// Default js
		foreach ($js as $script)
			$this->addJS($script);

		// Set the theme path
		$theme = Config::getTheme();
		
		if ($theme && trim($theme) !== '')
			$this->setTheme($theme);

	}
	
	public function debug($debug=true) {
	
		$this->debug = $debug;
	}
	
	public function step($message) {
		
		$this->stepCount++;
		$this->steps[] = "<pre>{$this->stepCount}) $message</pre>";
	}

	/**
	 *  Admin Only
	 *
	 *  Some areas of the site will be limited to admin access only. This method will
	 *  redirect the user to a special login page if the appropriate session variable
	 *  is not set.
	 */

	 public function adminOnly($redirect='index'){

	 	$this->adminOnly = true;
	 	$this->redirect = $redirect;

	 }

	/**
	 * Add CSS stylesheets to this page. Note: to add theme-specific stylesheets,
	 * use the Page::addThemeCSS class method.
	 *
	 * @param string $href | the location of the stylesheet
	 * @param bool   $path | an optional path, if the default javascript path is not to be used
	 */

	public function addCSS($href, $path=false) {
		
		$this->step("adding stylesheet: $href");
		if (!$this->isAbsolute($href)) {
				$path ? $href = $path . '/' . $href : $href = $this->cssPath . '/' . $href;
				$href = str_replace('//', '/', $href);
		}

		array_push($this->css, $href);

	}

	/**
	 * Add javascript files to this page. Note: to add theme-specific javascript,
	 * use the Page::addThemeJS class method.
	 *
	 * @param string $src  | the location of the javascript file
	 * @param bool   $path | an optional path, if the default javascript path is not to be used
	 */

	public function addJS($src, $path=false) {
	
		$this->step("adding js: $src");
		
		if (!$this->isAbsolute($src)){
				$path ? $src = $path . '/' . $src : $src = $this->jsPath . '/' . $src;
				$src = str_replace('//', '/', $src);
		}
		array_push($this->js, $src);

	}

	/**
	 * Add a plugin to this page. A plugin is simply a collection of stylesheets (optional),
	 * javascript files (optional), and dependencies (other plugins, optional).
	 *
	 * @param string $name  | the name of the plugin, defined in etc/plugins.json
	 */

	public function addPlugin($name, $path=false) {
		
		$this->step("adding plugin $name");
		
		if (!$path)
			$path = $this->pluginBase;
		
		if ($plugin = Plugin::find($name)) {

			if (isset($plugin['dependencies']) && $this->pluginDependencies) {

					$dependencies = $plugin['dependencies'];
					
					if (is_array($dependencies)) {

						// add the dependencies (stylesheets and js) first
						foreach ($dependencies as $dep)
							self::addPlugin($dep, $path);

					}
			}
			
			if (isset($plugin['stylesheets'])) {
				
				$path ? $cssPath = $path . '/css/' : $cssPath = '';

				$stylesheets = $plugin['stylesheets'];

				if (is_array($stylesheets)) {

						// add the plugin's css
						foreach ($stylesheets as $href) {
							
							if (!$this->isAbsolute($href)) {
								self::addCSS('lib/' . $href, $cssPath);
							} else {
								self::addCSS($href, $cssPath);
							}

						}

				}

			}

			if (isset($plugin['javascripts'])){
			
				$path ? $jsPath = $path . '/js/' : $jsPath = '';

				$javascripts = $plugin['javascripts'];

				if (is_array($javascripts)) {

						// add the plugin's javascript
						foreach ($javascripts as $src) {

							if (!$this->isAbsolute($src)) {
								self::addJS('lib/' . $src, $jsPath);
							} else {
								self::addJS($src, $jsPath);
							}

						}
				}

			}

		}

	}
	
	public function isAbsolute($url){
	
		if (substr($url, 0, 4) !== 'http' && substr($url, 0, 4) !== 'www' && $url[0] !== '/')
			return false;
		
		return true;
	}
	
	public function pluginBase($path){
		$this->pluginBase = $path;
	}

	public function build() {
		
		$this->js  = array_unique($this->js);
		$this->css = array_unique($this->css);

		if ($this->robots) {
			$robots = "<meta name=\"robots\" content=\"index,follow\">\n";
		} else {
			$robots = "<meta name=\"robots\" content=\"noindex,nofollow\">\n";
		}
		

		include('core.h.php');
		include('core.f.php');
		
		$this->step("build complete");
		if ($this->debug){
		
			foreach ($this->steps as $step)
				echo $step;
		}
		
		//
		return ob_get_contents();
				
	}

	/**
	 * Determines whether or not the current user has access to this page
	 *
	 * @param  string $role          | the role which is used to determine page-access
	 * @return bool   $accessAllowed | whether the current user's role allows for the page to be built
	 */

	public function validateBuild($role){
		
		$this->step("validating build");
		if (in_array($role, $this->restrictedTo)) {
			return true;

		} else {
			return false;
		}

	}
	
	public function cache($timeString){
			
		$this->cacheExpiration = $timeString;
		$this->cacheOn		   = true;
		
	}

	/**
	 * Forces the user to be authenticated before the page can be viewed
	 *
	 */

	public function requireLogin($redirect='index'){

		$this->requireLogin = true;
		$this->redirect = $redirect;

	}
	
	public static function getCapturedContent(){
		return self::$capturedContent;
	}

	public function getAdditionalHeadContent() {
	
		foreach ($this->headAppend as $str) {
		
			echo $str, " ";
		}
	
	}
	
	public function getAdditionalBodyContent() {
	
		foreach ($this->bodyAppend as $str) {
		
			echo $str, " ";
		}
	
	}
	
	/**
	 * Echos all of the formatted CSS links to the output buffer
	 *
	 */

	public function getCSS() {
		
		$this->step("echoing css");
		$count = 0;

		$this->pathOverride !== '' ? $path = $this->pathOverride . DIRECTORY_SEPARATOR : $path = '';
		$this->css = array_unique($this->css); // prevent duplicates
		
		$cssMiniName = 'www/css/minify/'.$this->title.'.css';

		if (file_exists($cssMiniName) && !isset($_GET['minify'])){
		
			echo '<link rel="stylesheet" type="text/css" href="/' .  $cssMiniName . '"/>' . "\n";
			return;
		}
		
		// Minify the css files
		if (isset($_GET['minify'])) {
			
			$str = implode("", $this->css);
			$cssMinAll = '';
			
			foreach($this->css as $index => $href) {
				
				if (substr($href, 0, 4) == 'view')
					$href = DIRECTORY_SEPARATOR . $href;
					
				$file = strtr($_SERVER['DOCUMENT_ROOT'].$href, array('//' => '/'));

				$nonMinCss = file_get_contents($file);
				

				// Check if the css was already minified (assumes minified files contain '.min.' in file name
				if (strpos($href, '.min.') !== FALSE){
				
					$cssMinAll .= $nonMinCss . "\n";
					continue;
					
				}
				
				$cssMinifier = new CSSmin();
				$cssMinAll .= $cssMinifier->run($nonMinCss) . "\n";
					
			}

			file_put_contents($cssMiniName,$cssMinAll);
				
			echo '<link rel="stylesheet" type="text/css" href="/' .  $cssMiniName . '"/>' . "\n";
			
			return;
		
		}
		
		// Don't minify
		foreach($this->css as $href) {

			$count++;
			$count > 1 ? $indent = "\t" : $indent = '';

			// add a time stamp to prevent caching if the user set the preventCaching setting to true
			$this->preventCaching? $href = $href . '?' . time() : $href .= '';
 		  	echo $indent . '<link rel="stylesheet" type="text/css" href="' . $path .  $href . '"/>' . "\n";

		}

	}

	public function getJS() {
		
		$this->step("echoing js");
		$indent = "\t";
		$this->pathOverride !== '' ? $path = $this->pathOverride . DIRECTORY_SEPARATOR : $path = '';
		$this->js = array_unique($this->js); // prevent duplicates
		
		if ($this->toClient) {
			
			$toClient = json_encode($this->toClient);
			echo <<<EOD
				<script>
				function Page(){this.serverVars={}}Page.prototype.exists=function(e){return typeof [this.serverVars[e]]!=="undefined"};Page.prototype.set=function(e){this.serverVars=e};Page.prototype.get=function(e){if(this.exists(e))return this.serverVars[e];return false}
				var page = new Page();
				page.set({$toClient});
				</script>
EOD;
		}
		
		$jsMiniName = 'www/js/minify/'.$this->title.'.js';
		
		if (file_exists($jsMiniName)){
		
			echo $indent . '<script type="text/javascript" src="/' . $jsMiniName . '"></script>' . "\n";
			return;
		}
		
		// Minify the js files
		if (isset($_GET['minify'])) {
			
			$str = implode("", $this->js);
			$jsMinAll = '';
			
			foreach($this->js as $index => $src) {
				
				if (substr($src, 0, 4) == 'view')
					$src = DIRECTORY_SEPARATOR . $src;
					
				$file = strtr($_SERVER['DOCUMENT_ROOT'].$src, array('//' => '/'));
				$nonMinJs = file_get_contents($file);

				// Check if the js was already minified (assumes minified files contain '.min.' in file name
				if (strpos($src, '.min.') !== FALSE){
				
					$jsMinAll .= $nonMinJs . "\n";
					continue;
					
				}
				
				$jsMinAll .= JSMin::minify($nonMinJs) . "\n";
			}
				
			file_put_contents($jsMiniName,$jsMinAll);
				
			echo $indent . '<script type="text/javascript" src="/' . $jsMiniName . '"></script>' . "\n";
			return;
		
		}
		
		// Don't minify
		foreach($this->js as $src) {

			// add a time stamp to prevent caching if the user set the preventCaching setting to true
			$this->preventCaching? $src = $src . '?' . time() : $src .= '';
        	echo $indent . '<script type="text/javascript" src="' . $path . $src . '"></script>' . "\n";

		}

	}
	
	/**
	 * include the theme's header, located at view/themes/themename/header.php
	 *
	 */

	public function getThemeHeader(){
	
		$this->step("getting theme header");
		$path  = $this->getThemePath();
		$file = $path . 'header.php';

		if(file_exists($file))
			include($file);
			
	}

	/**
	 * include the theme's footer, located at view/themes/themename/footer.php
	 *
	 */

	public function getThemeFooter(){
	
		$this->step("getting theme footer");
		$path  = $this->getThemePath();
		$file = $path . 'footer.php';

		if(file_exists($file))
			include($file);
	}
	
	/**
	 * Get the path of the currently selected theme
	 *
	 * @return string $path
	 */

	public function getThemePath(){
		
		$this->step("getting theme path");
		$path = __DIR__  . DIRECTORY_SEPARATOR .
				'themes' . DIRECTORY_SEPARATOR . $this->theme . DIRECTORY_SEPARATOR;
				
		return $path;
	}
	
	public function appendToHead($str){
	
		$this->headAppend[] = $str;
	}
	
	public function appendToBody($str){
	
		$this->bodyAppend[] = $str;
	}
	
	public function pluginDependencies($include=true){
		
		$this->pluginDependencies = $include;
	}

	public function navLinks(){
		
		$this->step("getting nav links");
		$settings = Config::getSettings('*');
		$navLinks = array();
		$pages = array();

		if (isset($settings['Pages']))
			$pages = $settings['Pages'][0];

		foreach ($pages as $text=>$arr){

			isset($arr['location']) ? $location = $arr['location'] : $location = "";
			isset($arr['icon'])     ? $icon = $arr['icon'] : $icon = "";

			$active = false;

			if (trim(strtolower($this->activePage)) == trim(strtolower($text))) {
				$active = true;
			}

			$navLinks[] = array("location" => $location, "text" => $text, "icon" => $icon, "active" => $active);
		}
		return $navLinks;
	}

	/**
	 * Prevent search engines and other robots from indexing this page
	 *
	 */

	public function noRobots(){
		$this->robots = false;
	}

	/**
	 * Static function that is called when a page is not found. The user is redirected to
	 * 404.php. The primary place this method is used is in the front controller, index.php
	 */

	public static function notFound($requestedPath=''){

		die("<pre>404. Page not found :|</pre>");

	}
	
	public function removeAllCSS(){
		$this->css = array();
	}
	
	public function removeAllJS(){
		$this->js = array();
	}

	/**
	 * Skeleton mode renders a stripped down version of the page
	 */

	public function skeletonMode($includeDefaults=true){
	
		$this->skeletonMode = true;
		$this->css = array();
		$this->js = array();
		
		if ($includeDefaults)
			$this->setTheme($this->theme, "skeletonMode");
	}

	/**
	 * Sets the active page, which can be read by our theme's header to set special
	 * style rules to the currently selected page.
	 */

	public function setActivePage($page){
		$this->activePage = $page;
	}

	/**
	 * Updates the author meta data in core.h
	 */

	public function setAuthor($author){
		$this->author = $author;
	}

	/**
	 * Updates the description meta data in core.h
	 */

	public function setDescription($desc){
		$this->description = $desc;
	}

	/**
	 * Updates the keywords meta data in core.h
	 */

	public function setKeywords($keywords){
		$this->keywords = $keywords;
	}

	/**
	 * Define a custom footer file for this page
	 */

	public function setFooter($file){
		$this->footerFile = $file;
	}

	/**
	 * Define a custom header file for this page
	 */

	public function setHeader($file){
		$this->headerFile = $file;
	}

	/**
	 * Set the current path for all resources
	 *
	 * @param string $path
	 */

	public function setPath($path){
		$this->pathOverride = $path;
	}

	/**
	 * Set the page's theme, which can include custom stylesheets, javascript files,
	 * a header file, and a footer file.
	 *
	 * @param string $theme
	 */

	/**
	 * Set the page's theme, which can include custom stylesheets, javascript files,
	 * a header file, and a footer file.
	 *
	 * @param string $theme
	 */

	public function setTheme($theme, $group="defaults") {
		
		$this->step("Setting theme");
		$this->theme = $theme;
		
		$this->css = array();
		$this->js = array();
	
		ThemeConfig::setTheme($theme);
		$this->themeSettings = ThemeConfig::getThemeSettings($theme);
		
		// set the base href
		if (isset($this->themeSettings['www'])) {
		
			$this->webAddress = $this->themeSettings['www'];
			$this->www($this->themeSettings['www']);
		
		}


		// add the theme's plugins
		$plugins = ThemeConfig::getPlugins($group);
		
		if (is_array($plugins)) {
		
			foreach ($plugins as $plugin)
				$this->addPlugin($plugin);
		}
		
		// get the stylesheets
		$css = ThemeConfig::getCSS($group);
		
		if (is_array($css)) {
		
			foreach ($css as $stylesheet)
				$this->addCSS($stylesheet);
		}
		
		// get the javascript files
		$js = ThemeConfig::getJS($group);

		if (is_array($js)) {
		
			foreach ($js as $script)
				$this->addJS($script);
		}
	}
	
	public function toClient($key, $val){
	
		$this->toClient[$key] = $val;
	}
	
	public static function __getUser(){
	
		if (isset(self::$user))
			return self::$user;
			
		return false;
	
	}
	
	public function getUser(){
		
		if (isset(self::$user))
			return self::$user;
			
		return false;
	
	}
	
	public static function setUserObj(AppUser $user) {
	
		self::$user = $user;
	}
	

	/**
	 * Restrict access to the current page to only the roles specified
	 *
	 * @params array $roles | all of the roles that will be allowed to view the current page
	 */

	public function restrictTo($roles) {
		
		if (!is_array($roles)){
			
			$roles = explode(',', $roles);
			$roles = array_map('trim', $roles);
	
		}
		
		$user      = $this->getUser();
		$userRoles = $user->getRoles();
		
		if (array_intersect($userRoles, $roles))
			return true;
		
		$user->accessDenied();

	}

	/**
	 * Place initialization code here.
	 */
	public function startContent(){
		
		ob_start();
		$continue = true;
		
		if ($this->restrictedTo && $user = $this->getUser()) {
			
			$continue = false;
			$roles    = array_flip($user->getRoles());

			foreach ($this->restrictedTo as $role) {
				
				if (isset($roles[$role]))
					$continue = true;
			}
		}
		
		if (!$continue)
			return header('Location: ' . $user->getAccessDenied());
		
			
		$this->step("starting content");
	}
	
	public function start(){
	
		return $this->startContent();
	}
	
	public function startCSS(){
	
		ob_start();
	}
	
	public function startCode($type=''){
	    
	    $this->lastCodeType = '';
	    ob_start();
	    
	}
	
	public function endCode(){
	
	    $code = ob_get_clean();
	    $code = strtr($code, array('<' => '&lt;'));
	    echo "<pre><code class='{$this->lastCodeType}'>" . trim($code) . "</code></pre>";
	}
	
	public function endCSS(){
	
		$css = ob_get_clean();
		$css = strtr($css, array('<style>' => '', '</style>' => ''));
		$css = '<style>' . $css . '</style>';
		$this->appendToHead($css);
		
		
	}
	
	public function startJS(){
	
		ob_start();
	}
	
	public function endJS(){
	
		$js = ob_get_clean();
		$js = strtr($js, array('<script>' => '', '</script>' => ''));
		$js = '<script>' . $js . '</script>';
		$this->appendToBody($js);
		
		
	}
	
	public static function beforeEnd($callable){
	
		self::$beforeEnd = $callable;
	}

	/**
	 * Clean the output buffer, and save the contents to a variable
	 */
	public function endContent($capture=false){
				
		$this->contentLength  = ob_get_length(); 
		$this->content 		  = ob_get_clean();
		
		if (is_callable(self::$beforeEnd)){
			
			call_user_func(self::$beforeEnd, $this);
		
		}
		
		$content = $this->build();
		
		if ($this->cacheOn && $this->cacheExpiration)
			Cache::generateCacheFile($content, $this->cacheExpiration);

		if ($capture) {
			ob_end_clean();
			return $content;
		}
		
		ob_end_flush();

	}
	
	public function end(){
	
		return $this->endContent();
	}
	
	public function captureContent(){
		self::$capturedContent = $this->endContent(true);
	}
	
	public function limitContent($roles){
		
		ob_start();
		
		if (!is_array($roles)){
			
			$roles = explode(',', $roles);
			$roles = array_map('trim', $roles);
		}
		
		// Every one gets the 'all' role
		$userRoles = array('anyGroup');
		
		if (isset($_SESSION['dashUserRoles']))
			$userRoles = array_merge($userRoles, $_SESSION['dashUserRoles']);

		if (array_intersect($userRoles, $roles)) {

			//echo "show content";
			$this->limitContentBooleans[] = true;

		} else {

			//echo "do not show content";
			$this->limitContentBooleans[] = false;
		}
	
	}
	
	public function endLimitContent(){
	
		// Get all data in the outpout buffer
		$content = ob_get_clean();

		// Get the most recent boolean
		$show = array_pop($this->limitContentBooleans);
		
		if ($show)
			echo $content;
		
	}
	
	public function link($url, array $getParams=array()){
		
		if (!$this->isAbsolute($url))
			$url =  rtrim($this->webAddress, '/') . '/'  . ltrim($_SERVER['REQUEST_URI'], '/') . $url;	
		
		if ($getParams) {
		
			$i    = 0;
			$url .= '?';
			
			foreach ($getParams as $key => $val) {
				
				$i++;
				
				if ($i > 1)
					$url .= '&';

				$url .= $key . '=' . urlencode($val);
				
			}
		}
		
		return $url;
	}
	
	public function www($path){
		$this->pluginBase($path);
		$this->cssPath = rtrim($path, '/') . '/css';
		$this->jsPath  = rtrim($path, '/') . '/js';
	}
}


?>
