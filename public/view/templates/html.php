<?php

$page = new Page('HTML');
$page->setDescription('Topics');
$page->addCSS('docs.css');
$page->startContent();

$icon = new Icon($page);

?>



<div class="container">
	<div class="row">
	
		<div class="col-md-10 col-md-offset-1">
			
			<!-- Intro -->
			<div class="row">
		
				<div class="col-md-10 col-md-offset-1">
					<h1 class="course-title">HTML</h1>
					<br>
					<ul style="list-style:circle;">
					    <li>What is HTML?</li>
					    <li>Building HTML documents</li>
					    <li>Important HTML tags</li>
					    <li>Inspecting HTML with Firebug</li>
					    <li>HTML attributes</li>
					    <li>
					        Intro to styling HTML  - <b>Note:</b> the CSS deep dive will be covered in the
					        <a href="/css">CSS course</a>
					    </li>
					</ul>
					<br><br><br><br><br>
				</div>
			</div>
		
		</div>
	</div>
</div>
<?php
$page->end();
?>
