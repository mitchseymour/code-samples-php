<?php

$page = new Page('Home');
$page->setDescription('Course Outline');
$page->addCSS('docs.css');
$page->startContent();

$icon = new Icon($page);


$dialog = new Dialog($page);
?>
<div id="dialog-template" class="dialog">
	<div class="dialog__overlay"></div>
	<div class="dialog__content">
		&nbsp;
		<a id="close">close</a>
	</div>
</div>


<div class="container">
	<div class="row">
	
		<div class="col-md-10 col-md-offset-1">
			
			<!-- Intro -->
			<div class="row">
		
				<div class="col-md-11 col-md-offset-1">
					<h1 class="course-title">About the Instructor</h1>
					<br>
					<a href="/www/img/mitchseymour.jpg">
					    <img src="/www/img/mitchseymour.jpg" width="300px" style="border:1px solid #ccc;">
					</a>
					<p class="course-description">
						My name is <a href="https://www.linkedin.com/in/mitchseymour" target="_blank">Mitch Seymour</a>, 
						and I am a senior developer at Macy's.
						I build monitoring software for two large
						eCommerce websites, <a href="http://www.macys.com/" target="_blank">macys.com</a>
						and <a href="http://www.bloomingdales.com/" target="_blank">bloomingdales.com</a>.
						My recent projects include an iPhone app, Android app, a statistical
						library for calculating the health of these two websites, and more.
					</p>
					<p>
					    In my free time, I enjoy playing the guitar, spending time with
					    my wife and dog, and helping others learn the wonderful art
					    of programming.
					</p>
				</div>
			</div>
		
		</div>
	</div>
</div>
<?php
$page->end();
?>
