<?php

$page = new Page('CSS');
$page->setDescription('Coming Soon');
$page->addCSS('docs.css');
$page->startContent();

$icon = new Icon($page);

?>



<div class="container">
	<div class="row">
	
		<div class="col-md-10 col-md-offset-1">
			
			<!-- Intro -->
			<div class="row">
		
				<div class="col-md-10 col-md-offset-1">
					<h1 class="course-title">CSS</h1>
					<br>
					<p>
					     The CSS course has been planned, but is currently not scheduled. If
					    you are interested in this course, please let 
					     <a href="http://xlacademyga.com/contact">XL Academy</a> know. Currently,
					     the only course that has been scheduled is the <a href="/html">HTML course</a>. To
					     learn more about the HTML topics that we'll be covering, please 
					     <a href="/html">click here</a>.
					</p>
					<br><br><br><br><br>
				</div>
			</div>
		
		</div>
	</div>
</div>
<?php
$page->end();
?>
