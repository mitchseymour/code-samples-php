<?php

$page = new Page('Home');
$page->setDescription('Course Outline');
$page->addCSS('docs.css');
$page->addJS('docs.js');
$page->startContent();

$icon = new Icon($page);


$dialog = new Dialog($page);
?>
<div id="dialog-template" class="dialog">
	<div class="dialog__overlay"></div>
	<div class="dialog__content">
		&nbsp;
		<a id="close">close</a>
	</div>
</div>


<div class="container">
	<div class="row course-section">
	
		<div class="col-md-9 col-md-offset-1">
			
			<!-- Intro -->
			<div class="row">
		
				<div class="col-md-11 col-md-offset-1">
					<h1 class="course-title">HTML</h1>
					<span class="course-part">Part 1</span>
					<p class="course-description">
						In this course, students will learn the basics of creating 
						<a href="http://en.wikipedia.org/wiki/HTML" target="_blank">HTML</a> documents.
						We will be exploring the various HTML elements, attributes, and structures that we will
						be using to build our mobile app. This course contains 4 sessions, each lasting 1 hour.
					</p>
					<ul class="course-icons prereqs" data-course="HTML">
					    <li>
					        <i class="fa fa-list"></i> 
					        <span>&nbsp; Prerequisites</span>
					    </li>
					</ul>
					<ul>
                        <li class="course-date" data-day="1" data-course="HTML">
					        June 8th
					    </li>
					    <li class="course-date" data-day="2" data-course="HTML">
					        June 15th
					    </li>
					    <li class="course-date" data-day="3" data-course="HTML">
					        June 22nd
					    </li>
					    <li class="course-date" data-day="4" data-course="HTML">
					        June 29th
					    </li>
					</ul>
				</div>
			</div>
		
		</div>
	</div>
	<hr>
	<div class="row course-section">
	
		<div class="col-md-9 col-md-offset-1">
			
			<!-- Intro -->
			<div class="row">
		
				<div class="col-md-11 col-md-offset-1">
					<h1 class="course-title">CSS</h1>
					<span class="course-part">Part 2</span>
					<p class="course-description">
						In this course, students will learn how to style HTML documents using
						<a href="http://en.wikipedia.org/wiki/Cascading_Style_Sheets" target="_blank">CSS</a>.
						This course will contain 4 sessions, each lasting one hour.
						
					</p>
					<ul class="course-icons prereqs" data-course="CSS">
					    <li>
					        <i class="fa fa-list"></i> 
					        <span>&nbsp; Prerequisites</span>
					    </li>
					</ul>
					<ul>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					</ul>
				</div>
			</div>
		
		</div>
	</div>
	<hr>
	<div class="row course-section">
	
		<div class="col-md-9 col-md-offset-1">
			
			<!-- Intro -->
			<div class="row">
		
				<div class="col-md-11 col-md-offset-1">
					<h1 class="course-title">Javascript</h1>
					<span class="course-part">Part 3</span>
					<p class="course-description">
						In the third course, we will be covering the basics of 
						<a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">javascript programming.</a>
						Whereas HTML and CSS will control the look and feel of our mobile app, javascript 
						will act as the "brains" for our mobile app. This course will cover more difficult concepts
						than the first two, so we'll cover this material in 6 sessions, each lasting one hour.
					</p>
					<ul class="course-icons prereqs" data-course="Javascript">
					    <li>
					        <i class="fa fa-list"></i> 
					        <span>&nbsp; Prerequisites</span>
					    </li>
					</ul>
					<ul>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					</ul>
				</div>
			</div>
		
		</div>
	</div>
	<hr>
	<div class="row course-section">
	
		<div class="col-md-9 col-md-offset-1">
			
			<!-- Intro -->
			<div class="row">
		
				<div class="col-md-11 col-md-offset-1">
					<h1 class="course-title">Cordova</h1>
					<span class="course-part">Part 4</span>
					<p class="course-description">
						Finally, the students will take their new HTML, CSS, and javascript
						skills, and build a native 
						<a href="http://en.wikipedia.org/wiki/Android_%28operating_system%29" target="_blank">Android</a> 
						app using
						<a href="https://cordova.apache.org/" target="_blank">Cordova</a>. This course
						will contain 4 sessions, each lasting one hour.
						<b>Note:</b> Android phones are optional. Students without Android phones
						will be able to deploy their apps to an Android simulator that will
						run on their computer.
					</p>
					<ul class="course-icons prereqs" data-course="Cordova">
					    <li>
					        <i class="fa fa-list"></i> 
					        <span>&nbsp; Prerequisites</span>
					    </li>
					</ul>
					<ul>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					    <li class="course-date tentative">
					        Date TBD
					    </li>
					</ul>
				</div>
			</div>
		
		</div>
	</div>
</div>
<?php
$page->end();
?>
