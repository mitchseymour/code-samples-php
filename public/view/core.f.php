<?php
/*!
 * Note: you should never have to edit this file.
 * Please edit the theme's footer.php file instead
 */
if (!$this->skeletonMode)
	$this->getThemeFooter();
	
echo $this->getJS();
echo $this->getAdditionalBodyContent()
?>

</body>
</html>


