<?php

class View {

	private static $path;
	private static $viewData;
	
	public static function getView() {
	
		// Get the relevant data, and include the file
		if (isset(self::$viewData)) {
			
			foreach (self::$viewData as $name => $val)
				${$name} = $val;
		}
		
		return include(self::$path);
	}
	
	public static function makeAvailable(array $data){
		if (is_array(self::$viewData))
			array_merge(self::$viewData, $data);
		else
			self::$viewData = $data;
	}
	
	public static function setTemplate($template){
		
		
		if (strpos($template, '.') == false){
			
			// no extension specified, add .php
			$template .= '.php';
			
		}
		
		$find = "view/templates/" . $template;
		
		if (self::viewExists($find))
			self::getView();
		else
			Page::notFound();
	
	}
	
	
	public static function viewExists($find){
	
		$paths = explode(PATH_SEPARATOR, get_include_path());
		$found = false;

		foreach($paths as $p) {
  
  			$fullname = $p.DIRECTORY_SEPARATOR.$find;
  
  			if(is_file($fullname)) {
  			
    			$found = $fullname;
    			break;
  			
  			}
		}

		if ($found) {
			
			self::$path = $found;
			return $found;
			
		}

		return false;
		
	}
}

?>