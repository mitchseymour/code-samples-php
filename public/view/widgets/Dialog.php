<?php
/**
 * Dialog
 *
 * Wrapper for codrops dialog FX
 *
 * Usage
 * ------------------------------------------------
<?php

$dialog = new Dialog($page);
$dialog->animation('don'); // sandra, sally, don, ken, or alex
$dialog->trigger('.trigger');
$dialog->closeSelector('#closeme');
$dialog->startContent();

?>
<h2><strong>Howdy</strong>, I'm a dialog box</h2>
<button id="closeme">Close me</button>

<?php
$dialog->endContent();
echo $dialog->render();
?>
 * ------------------------------------------------
 *
 * @author <mitchseymour@gmail.com>
 * @url http://tympanus.net/Development/DialogEffects/
 * @version 0.1
 */

 
class Dialog extends App_Widget {
	
	private $content;
	
	public function __construct(Page $page){
	
		$page->addPlugin('modernizr');
		$page->addPlugin('classie');
		$page->addCSS('dialogFX.css');
		$page->addJS('dialogFX.js');

		parent::__construct($page);
		
				
	}
	
	public function startContent(){
	
		ob_start();
	}	

	public function endContent(){
	
		$this->setContent(ob_get_clean());
	}
	
	public function setContent($str){
	
		$this->content = $str;
	}
	
	public function closeSelector($selector){
	
		$this->setWidgetOption('closeSelector', $selector);
	}
	
	public function trigger($selector){
	
		$this->setWidgetOption('trigger', $selector);
	}
	
	public function animation($type){
	
		$this->setWidgetOption('animation', $type);
	}
	
	public function render(){
	
		// Initialization
		$options = json_encode($this->getWidgetOptions());
		
		$id = $this->getDynamicVal('dialog_');
		$attrStr = $this->attrStr();
		$classStr = $this->classStr();
		
		$this->initWithJS("new DialogFx('#{$id}', $options)");
		
		$html = <<<EOD
		
			<div id="{$id}" class="dialog {$classStr}" {$attrStr}>
				<div class="dialog__overlay"></div>
				<div class="dialog__content">
					{$this->content}
				</div>
			</div>
EOD;
		
		return $html;
	
	}

}

?>