<?php
/**
 * Icon class
 *
 * Currently acts as a font-awesome wrapper. May be
 * modified to support icon images in the future.
 *
 * @author <mitchseymour@gmail.com>
 * @version 0.2
 */

class Icon extends App_Widget {
	
	private $icon;
	
	public function __construct(Page $page, $icon=''){
		
		$page->addPlugin('font-awesome');
		parent::__construct($page);
		
	}
	
	private function formatIconName($icon){
	
		return strtr($icon, array('fa-' => ''));
	}

	public function render($icon='', $additional=false){
		
		if (!$icon && $this->icon)
			$icon = $this->icon;
			
		if (!$icon)
			return false;
			
		$iconName = $this->formatIconName($icon);
		$class    = $this->classStr();
		$attr     = $this->attrStr();
		
		$html = "<i class='fa fa-{$iconName} {$class}' {$attr}></i>";
		
		if ($additional)
			$html .= "&nbsp " . $additional;
			
		return $html;
		
	}
}

?>