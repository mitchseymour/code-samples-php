<?php
/**
 * App Widget
 *
 * @author <mitchseymour@gmail.com>
 * @version 0.3
 */
abstract class App_Widget {
	
	/**
	 * @var boolean $admin
	 * 
	 * Specifies whether or not the view is in admin mode.
	 */
	public static $admin = false;
	
	/**
	 * @var array $attr
	 * 
	 * An array of html attribute name/value pairs to be 
	 * applied to this widget's primary html element.
	 */
	protected $attr = array();
	
	/**
	 * @var array $attrGroups
	 * 
	 * A multi-dimensional array containing name/value pairs
	 * for HTML attributes, indexed by group name. This allows
	 * widgets to group attributes by arbitrary values, e.g. 
	 * "chartAttributes" or "alertAttributes", which can then 
	 * be applied to a specialized set of DOM elements.
	 */
	protected $attrGroups = array();
	
	/**
	 * @var array $classes
	 * 
	 * An array of classes to be applied to this widget's
	 * primary html element.
	 */
	protected $classes = array();
	
	/**
	 * @var array $containerAttr
	 * 
	 * An array of html attribute name/value pairs to be 
	 * applied to this widget's containing element
	 */
	protected $containerAttr = array();
	
	/**
	 * @var array $containerClasses
	 * 
	 * An array of classes to be applied to this widget's
	 * containing html element.
	 */
	protected $containerClasses = array();
	
	/**
	 * @var boolean $debug
	 * 
	 * Whether or not the view is in debug mode.
	 */
	public static $debug = false;
		
	/**
	 * @var string $dynamicClass
	 * 
	 * The name of a dynamically generated class
	 * that can be used by the widget
	 */
	protected $dynamicClass;
	
	/**
	 * @var integer $dynamicCount
	 * @deprecated
	 *
	 * The number of dynamic values created by the
	 * getDynamicVal method.
	 *
	 */
	protected static $dynamicCount = 1;
	
	/**
	 * @var string $dynamicID
	 * 
	 * The name of a dynamically generated id
	 * that can be used by the widget
	 */
	protected $dynamicID;
	
	/**
	 * @var array $groupClasses
	 * @deprecated
	 *
	 * A multi-dimensional array containing name/value pairs
	 * for css classes, indexed by group name. This allows
	 * widgets to apply classes to a specialized group of
	 * DOM elements.
	 */
	protected $groupClasses  = array();
	
	/**
	 * @var array $groupOptions
	 * 
	 * A multi-dimensional array containing name/value pairs
	 * for options, indexed by group name. This allows
	 * widgets to access options by function or group.
	 */
	protected $groupOptions  = array();
		
	/**
	 * @var string $jsInit
	 *
	 * The javascript to be executed when this widget
	 * is initialized
	 */
	protected $jsInit = "";
	
	/**
	 * @var array $options
	 * 
	 * An array of key/value pairs to be used by the widgets. These
	 * are set using the M_View::setWidgetOption() method.
	 */
	protected $options = array();
	
	/**
	 * @var array $optionLookup
	 * 
	 * An array of key/value pairs, with the key denoting an 
	 * option alias, and the value containing the actual
	 * key to be used.
	 */
	protected $optionLookup = array();
	
	/**
	 * @var Page $page
	 * 
	 * A Page object that can be used to add CSS, JS,
	 * plugins, etc.
	 */
	protected $page;
	
	/**
	 * @var array $placeholderLookup
	 * 
	 * An associative array whose values can be substituted
	 * by the associated keys using the following syntax:
	 *
	 * {{key}}
	 */
	private $placeholderLookup;
	
	
	/**
	 * @var string $tooltipContent
	 * 
	 * The html or plaintext that will be rendered in this
	 * widget's tooltip.
	 */
	public $tooltipContent;
	
	/**
	 * Child classes must implement this method. Note: render()
	 * should never echo the html, but should only return an html
	 * string, so the client code can decide where to place it.
	 *
	 * @return string $html
	 */
	 
	abstract function render();
	
	/**
	 * Constructor
	 *
	 * Note: The dynamic class and id properties are deprecated
	 * since version 0.2. Extending classes should use the 
	 * M_View::getDynamicVal() for this purpose
	 *
	 */
	public function __construct(Page $page=null) {
	
		$this->page = $page;
		
		$dynamicBase = rand(0, 99) . time() . rand(0, 99);
		$this->dynamicClass = 'c_' . $dynamicBase;
		$this->dynamicID    = 'i_' . $dynamicBase;
	
	}
	
	
	/**
	 * Constructor
	 *
	 * Note: The dynamic class and id properties are deprecated
	 * since version 0.2. Extending classes should use the 
	 * M_View::getDynamicVal() for this purpose
	 *
	 */
	public function loadOptions(array $arr){
	
		$this->options = $arr;
	}
	
	/**
	 * Retrieve all of the options set for this widget, regardless
	 * of the option group.
	 *
	 * @return array $options
	 */
	public function getAllOptions(){
		
		
		return $this->options + $this->groupOptions;
	}
	
	/**
	 * Simple method for retrieving a dynamic value. Since the
	 * $dyanmicCount class property is set and incremeneted,
	 * we can guarantee the uniqueness of the value.
	 *
	 * @return string $dynamicVal
	 */
	public function getDynamicVal($prefix='') {
	
		self::$dynamicCount += 1;
		
		$dynamicBase = 'm_widget_' . self::$dynamicCount . '_' . time();
		
		return $prefix . $dynamicBase;
	
	}
		
	/**
	 * Set the html attributes for this widget.
	 *
	 * @param string $name - The name of the html attribute to be set
	 * @param string $value - The attribute's value
	 * @param optional string $group - The group that these attributes
	 *								 will be isolated to. When using
	 *								 the group parameter, you can 
	 *								 access the attribute string by
	 *								 calling $this->attrStr($group)
	 * @return void
	 */
	public function attr($name, $value, $group=false){
		
		if (trim(strtolower($name)) == 'class') {
			
			if ($group)
				$this->groupClasses[$group][] = $value;
			else
				$this->classes[] = $value;
		
		 } else {
			
			if ($group)
				$this->attrGroups[$group][$name] = $value;
			else
				$this->attr[$name] = $value;
			
		}
		
		return $this;
	}
	
	/**
	 * Retrieve a string version for the attribute/value pairs.
	 *
	 * @return string $attrStr - The html attribute and value
	 * 							 that are to be used with this
	 *							 widget's primary html element.					 
	 */
	public function attrStr($group=false){
		
		$attr    = array();
		$attrStr = "";
		
		if (!$group)
			$attr = array_unique($this->attr);
		else if (isset($this->attrGroups[$group]))
			$attr = array_unique($this->attrGroups[$group]);

		
		foreach ($attr as $name => $value)
			$attrStr .= "$name='$value' ";
			
		return $attrStr;
	}
	
	/**
	 * Set the html attributes for this widget's container
	 *
	 * @param string $name  - The name of the html attribute to be set
	 * @param string $value - The attribute's value
	 * @return void				 
	 */
	public function containerAttr($name, $value){
		
		if (trim(strtolower($name)) == 'class')
			$this->containerClasses[] = $value;
		else
			$this->containerAttr[$name] = $value;
	}
	
	/**
	 * Retrieve a string version for the attribute/value pairs.
	 *
	 * @return string $containerAttrStr - The html attribute and value
	 * 									  that are to be used with this 
	 *									  widget's containing html element.					 
	 */
	public function containerAttrStr(){
		
		$containerAttrStr = "";
		$this->containerAttr = array_unique($this->containerAttr);
		foreach ($this->containerAttr as $name => $value)
			$containerAttrStr .= "{$name}='{$value}' ";
			
		return $containerAttrStr;
	
	}
	
	public function classSelectors($group=false){
	
		$arr = explode(' ', $this->classStr($group));
		array_walk($arr, function(&$v){
			$v ='.' . $v;
		});
		
		return $arr;
	}
	
	/**
	 * Retrieve a string version of all of the classes that have
	 * been added to this widget's primary html element
	 *
	 * @return string $classStr - The class declarations to be used
	 *							  on this widget's primary html element
	 */
	public function classStr($group=false){
		
		if (!$group)
			return implode(' ', array_unique($this->classes));
	
		if (isset($this->groupClasses[$group]))
			return implode(' ', array_unique($this->groupClasses[$group]));
		
		return '';
	
	}
	
	/**
	 * Retrieve a string version of all of the classes that have
	 * been added to this widget's containing html element
	 *
	 * @return string $containerClassStr - The class declarations to be used
	 *							  		   on this widget's containing html element
	 */
	public function containerClassStr(){
	
		return implode(' ', array_unique($this->containerClasses));
		
	}
	
	/** 
	 * Disables all options passed to this method.
	 *
	 * @return void
	 */
	public function disable(){
	
		$args = func_get_args();
		
		if (!$args)
			return;
			
		foreach ($args as $str) {
		
			$arr = explode(',', $str);
			
			foreach ($arr as $option) {
				
				$option = $this->lookupOption($option);
				
				if ($option)
					$this->setWidgetOption($option, false);
			}
		}
	}
	
	/** 
	 * Enables all options passed to this method.
	 *
	 * @return void
	 */
	public function enable(){
	
		$args = func_get_args();
		
		if (!$args)
			return;
			
		foreach ($args as $str) {
		
			$arr = explode(',', $str);
			
			foreach ($arr as $option) {
				
				$option = $this->lookupOption($option);
				
				if ($option)
					$this->setWidgetOption($option, false);
			}
		
		}
	}
	
	/** 
	 * Accessor method for widget attributes
	 *
	 * @return mixed $attr - The attribute value, or false if the
	 *						 the attribute was never configured
	 *
	 */
	public function getAttr($name, $group=false){
		
		if (!$group) {
		
			if (isset($this->attr[$name]))
				return $this->attr[$name];
				
			return false;
		}
		
		if (isset($this->attrGroups[$group][$name]))
			return $this->attrGroups[$group][$name];
			
		return false;
	}
	
	/** 
	 * Gets the alias (if applicable) of the option
	 * provided
	 *
	 * @return string $option
	 */
	protected function lookupOption($_option){
	
		$option = trim($_option);
		$lower  = strtolower($option);
		
		if (isset($this->optionLookup[$option]))
			return $this->optionLookup[$option];
		else if (isset($this->optionLookup[$lower]))
			return $this->optionLookup[$lower];
			
		return $_option;
		
	}
	
	/** 
	 * Appends the initialization code to the page object
	 *
	 * @return void
	 */
	public function initWithJS($str){
		
		$this->jsInit = $str;
		
		if (self::$debug) {
		
			$str = "var t0 = performance.now(); {$str} var t1 = performance.now(); console.log('widget build time: ' + (t1 - t0) + ' ms');";
		}
		
		$this->page->appendToBody("<script>{$str}</script>");
	}
	
	/** 
	 * Resolves whether or not the user accessing the widget
	 * is an admin
	 *
	 * @return boolean
	 */
	public function isAdmin(){
	
		return self::$admin;
	}
	
	/** 
	 * Resolves the debug status of the current widget
	 *
	 * @return boolean
	 */
	public function debugging(){
	
		return self::$debug;
	
	}
	
	/** 
	 * Sets the admin status for all widgets in the current process
	 *
	 * @param boolean $bool - Admin status of the viewer
	 * @return void
	 */
	public static function setAdmin($bool){
	
		self::$admin = $bool;
	}
	
	/** 
	 * Sets debug mode - which widgets can use for logging purposes,
	 * adding/removing features, etc
	 *
	 * @param boolean $bool - Debug status of all widgets in the current process
	 * @return void
	 */
	public static function setDebugMode($bool){
		
		self::$debug = $bool;
	}
	
	/** 
	 * Allows clients to reference options using aliases
	 *
	 * @param array $arr - Key value pairs of options and their aliases
	 * @return void
	 */
	public function setOptionLookup(array $arr){
	
		$this->optionLookup = $arr;
	}
	
	/** 
	 * Get the options
	 *
	 * @param string $key - The name of the option
	 * @param optional string $group - The name of the option group
	 *
	 * @return mixed $option - The previously configured option for key $key,
	 *						   or false if this option wasn't set
	 */
	public function getWidgetOption($key, $group=false){
	
		if ($group && isset($this->groupOptions[$group][$key]))
			return $this->groupOptions[$group][$key];
		else if (isset($this->options[$key]))
			return $this->options[$key];
			
		return false;
	}
	
	/** 
	 * Get the all widget options
	 *
	 * @group optional string $group - The option group
	 *
	 * @return mixed $options - The options configured for the current
	 *							widget, or false is none exist
	 */
	public function getWidgetOptions($group=false){
		
		if (!$group)
			return $this->options;
			
		if (isset($this->groupOptions[$group]))
			return $this->groupOptions[$group];
			
		return false;
	}
	
	/** 
	 * Setter method for widget options
	 *
	 * @param string $key - The name that will be used to retrieve the option value
	 * @param mixed $val  - The value of the option
	 * @param optional string $group - The group that the option belongs to
	 *
	 * @return void
	 */
	public function setWidgetOption($key, $val, $group=false){
		
		$key = $this->lookupOption($key);
		
		if ($group)
			$this->groupOptions[$group][$key] = $val;
		else
			$this->options[$key] = $val;
	
	}
	
	/**
	 * Simple template parser for statistical widgets
	 *
	 * @param array $templateStr 	-  The string to be parsed
	 *					   	   
	 * @param mixed $placeholders    - Either an array of key/value pairs, or
	 *								   the group that was used to previously
	 *								   set these values using the 
	 *								   M_View::setPlaceholderLookup() method
	 *
	 * @return void
	 */
	public function parseTemplate($templateStr, $placeholders='default'){
		
		if (!is_array($placeholders) && is_string($placeholders)){
			
			if (isset($this->placeholderLookup[$placeholders]))
				$placeholders = $this->placeholderLookup[$placeholders];
			else
				return false;
					
		}
		
		return strtr($templateStr, $placeholders);	
	}
	
	/**
	 * Set the content of this widget's tooltip.
	 *
	 * @param array $arr 	-  An associative array, with the key => value pairs
	 *					   	   that translate to placeholder => placeholderValue
	 * @param string $group - The group name to be used when retrieving these 
	 *						  placeholders
	 *
	 * @return void
	 */
	public function setPlaceholderLookup(array $arr, $group='default'){
		
		// Format the array
		$placeholders = array();
		
		foreach ($arr as $key => $value)
			$placeholders['{{' . $key . '}}'] = $value;
			
		$this->placeholderLookup[$group] = $placeholders;
		return $placeholders;
		
	}
	
	/**
	 * Set the content of this widget's tooltip.
	 *
	 * @return string $str - The html or plaintext that will be used
	 *						 in this widget's tooltip
	 */
	public function tooltip($str){
	
		$this->tooltipContent = $str;
	}
	
	/**
	 * Retrieve the content of the tooltip, typecasted as a string.
	 *
	 * @return string $tooltipContentStr - The tooltip's content
	 */
	public function tooltipStr(){
	
		return (string) $this->tooltipContent;
	}
	
	public function truncateStr($str, $limit){
		
		if (strlen($str) > $limit)
			$str = substr($str, 0, $limit) . '...';
			
		return $str;
	
	}

	
}

?>