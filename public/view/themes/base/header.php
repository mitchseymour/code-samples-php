	<div id="hidden-abs" class="absolute"></div>
	
	<!-- Menu -->
	<div class="menu-wrap">
		<nav class="menu">
			<div class="icon-list">
			    <a href="/about"><i class="fa fa-fw fa-user"></i><span>Instructor</span></a>
				<a href="/"><i class="fa fa-fw fa-star-o"></i><span>Overview</span></a>
				<a href="/html"><i class="fa fa-fw fa-code"></i><span>HTML</span></a>
				<a href="/css"><i class="fa fa-fw fa-paint-brush"></i><span>CSS</span></a>
				<a href="/js"><i class="fa fa-fw fa-terminal"></i><span>Javascript</span></a>
				<a href="/cordova"><i class="fa fa-fw fa-mobile"></i><span>Cordova</span></a>
			</div>
		</nav>
		<button id="close-button" class="close-button">Close Menu</button>
	</div>
	<button class="menu-button" id="open-button" title="Menu">Open Menu</button>
			
	<!-- Content -->
	<div id="main-content" class="container-fluid content-wrap">
	    
	    <div class="container">
    
            <div class="row">
	
		        <div class="col-md-12 page-header">
			
			        <!-- Intro -->
			        <div class="row">
		
		                <a href="/" class="plain">
		                    <h1 class="page-title">Mobile App Course &nbsp; <i class="icon fa fa-mobile"></i></h1>
		                    <h2 class="page-subtitle">Summer 2015</h2>
				        </a>
			        </div>
		            
		            <?php if ($this->description) { ?>
		            <p class="description-bubble">
		                <?=$this->description?>
		            </p>
		            <?php } ?>
		        </div>
	        </div>
	    </div>
	    <br>
