<?php
/**
 * Do not edit this page. This HTML is shared 
 * by all of the themes in this application.
 */
@header("X-UA-Compatible: IE=Edge,chrome=1 env=ie", false);
if ($this->preventCaching && !headers_sent()) {
	
	// try to prevent caching
	@header("Last-Modified: " . gmdate("D, d M Y H:i:s"));
	@header("Cache-Control: no-store, no-cache, must-revalidate");
	@header("Cache-Control: post-check=0, pre-check=0", false);
	@header("Pragma: no-cache");

}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?=$this->title;?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="author" content="<?=$this->author;?>">
    <meta name="description" content="<?=$this->description;?>">
    <meta name="keywords" content="<?=$this->keywords;?>">
	<!-- force IE to IE9 standards mode -->
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<?=$robots;?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php if ($this->preventCaching) { ?>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
	<?php } ?>
    <base href="<?=$this->webAddress;?>">
    <script>if (!window.console) console = {log: function() {}};</script>
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--css-->
    <?=$this->getCSS();?>
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="/www/img/apple-icon.png" />  
    <link rel="apple-touch-icon" sizes="72x72" href="/www/img/apple-ipad.png" />  
    <link rel="apple-touch-icon" sizes="114x114" href="/www/img/apple-icon-retina.png" /> 
	<?=$this->getAdditionalHeadContent();?>
  </head>
  <body>
    <?php

	if (!$this->skeletonMode)
  		$this->getThemeHeader();

 	echo $this->content;

 	?>
