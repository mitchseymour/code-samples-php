<?php


class details_Controller {
	
	
	public static function index(){
	    
	    if (!isset($_GET['course']))
	        return header('Location: /');
		
		$course = $_GET['course'];
		$day = $_GET['day'];
		$path = "course/{$course}";
		$file = "view/templates/{$path}.php";
		
		if (file_exists($file))
		    return View::setTemplate($path);
		    
		return header('Location: /');
		
	}
	

}

?>
