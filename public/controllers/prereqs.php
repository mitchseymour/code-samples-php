<?php


class prereqs_Controller {
	
	
	public static function get(){
	
		$course = $_GET['course'];
		
		// hardcoded for now. replace this with your db calls
		$details = self::getDetails($course);
		
		View::makeAvailable(array('course' => $course, 'details' => $details));
		View::setTemplate('docs/detail');
		
	}
	
	private static function getDetails($course){
	
		$details = array();
		
		// Options
		$details['html'] = array(
		                    'part' => 1, 
		                    'prereqs' => array(
		                            'The only requirement for this course is for the student
		                            to bring a laptop with Firefox installed. You can install
		                            the latest version of Firefox 
		                            <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank">here.</a>'
		                        )
		                    );
		                    
		$details['css'] = array(
		                    'part' => 2, 
		                    'prereqs' => array(
		                            'Students must complete the HTML course, or demonstrate a
		                            basic knowledge of HTML to the instructor, in order to attend
		                            this course'
		                        )
		                    );
		                    
		$details['javascript'] = array(
		                    'part' => 3, 
		                    'prereqs' => array(
		                            'Students must complete the HTML and CSS courses, or demonstrate
		                            an intermediate level of proficiency with these technologies,
		                            in order to attend the javascript course'
		                        )
		                    );
		                    
		$details['cordova'] = array(
		                    'part' => 4, 
		                    'prereqs' => array(
		                            'Students must complete the HTML, CSS, and Javascript courses.'
		                        )
		                    );

		
		$course = strtolower($course);
		
		if (isset($details[$course]))
			return $details[$course];
			
		
			
		return array(
		            'part' => 0, 
                    'prereqs' => array(':(')
		            );

	}
	

}

?>
