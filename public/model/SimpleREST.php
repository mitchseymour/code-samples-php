<?php
/**
 * Simple REST Client
 *
 * This class is a very simple and lightweight REST client that can
 * be used for making basic DELETE/GET/POST/PUT http requests. It has
 * generalized methods that can work with many APIs, including HBase,
 * Lync, etc.
 *
 * Usage:
 * ------ 
 * $rest = Config::getRESTClient('rasm');
 *
 * // Formats
 * $rest->binary();
 * $rest->html();
 * $rest->json();
 * $rest->plain();
 * $rest->protobuf();
 * $rest->xml();
 *
 * $rest->autoConvert(true); // Responses are converted into arrays, and base64_decode is called
 *
 * $requestData = array('name' => 'Mitch', 'age' => 28);
 * $rest->location('relative/path/to/url')->delete();
 * $rest->location('relative/path/to/url')->get($requestData);
 * $rest->location('relative/path/to/url')->post($requestData);
 * $rest->location('relative/path/to/url')->put();
 *
 * // Setting headers
 * $rest->headers(array('Referer' => 'https://google.com')); 
 *
 * // Get info about the last request
 * $infoArr = $rest->lastRequest();
 * $resCode = $rest->responseCode();
 *
 * HBase Examples (Using the Stargate API)
 * ---------------------------------------
 * $clusterStatus = $rest->location('status/cluster')->get();
 * $regions = $rest->location('MitchTest/regions')->get();
 * $schema = $rest->location('MitchTest/schema')->get();
 * $tables = $rest->location()->get();
 * $version = $rest->location('version')->get();
 * $versionCluster = $rest->location('version/cluster')->get();
 *
 * HBase CRUD Queries (Stargate)
 * -----------------------------
 * $rows = $rest->location('MitchTest/row1')->get();
 * $rows = $rest->location('MitchTest/*ASPECTJ*')->get(); // Filter row keys
 * $rows = $rest->location('MitchTest/*ASPECTJ*')->get(array('startrow' => '*', 'limit' => 30)); // Limit clause
 * // More coming soon...
 *
 * @author <mitchseymour@gmail.com>
 * @version 0.0.2
 */

require_once('DataStore.php');

class SimpleREST extends DataStore {

	/**
	 * @var boolean $autoConvert
	 * 
	 * Whether or not the responses should be automatically converted
	 * into arrays (JSON) or XML docs (XML)
	 */
	private $autoConvert;	
	
	/**
	 * @var string $acceptHeader
	 * 
	 * The expected encoding of responses (e.g. json, xml, binary, protobufs)
	 */
	private $acceptHeader;	
	
	/**
	 * @var string $contentType
	 * 
	 * The value of the Content-Type header
	 */
	private $contentType;	

	/**
	 * @var array $lastInfo
	 * 
	 * Information regarding the last request/transfer
	 */
	private $lastInfo = array();
	
	/**
	 * @var string $lastUrl
	 * 
	 * The last url that was set using the chainable 
	 * self::$location method
	 */
	private $lastUrl = '';
	
	/**
	 * @var string $lastHeaders
	 * 
	 * The last set of headers that was set using the chainable 
	 * self::$headers method
	 */
	private $lastHeaders = array();
	
	/**
	 * @var string $xmlVersion
	 * 
	 * The xml version to be used when auto-converting XML responses
	 * to DOMDocuments
	 */
	private $xmlVersion;
	
	/**
	 * Accepts
	 *
	 * Determines whether or not the provided content type is accepted by looking
	 * at the accept header.
	 *
	 * @param string $contentType - the content type to examine
	 * @return boolean $bool - true if the $contentType is accepted, otherwise false
	 */
	private function accepts($contentType){
	
		if ($this->acceptHeader && strpos(strtolower($this->acceptHeader), strtolower($contentType)) !== false)
			return true;
			
		return false;
			
	}
	
	/**
	 * Auto Convert
	 *
	 * @param boolean $bool - true if response should be automtically converted
	 * @return $this
	 */
	public function autoConvert($bool){
	
		$this->autoConvert = (boolean) $bool;
		return $this;
	}
		
	/**
	 * Before handle returned (overrides parent method)
	 *
	 * This method is called before any handle is returned.
	 * It allows us to configured the curl settings based on
	 * some previous methods that were called before the handle
	 * was retrieved
	 *
	 * @return string $clientType - The client type for this DataStore
	 */
	protected function beforeHandleReturned($c){
	
		if ($this->lastUrl){
			
			if (strpos($this->lastUrl, '://') !== FALSE)
				$c['address'] = $this->lastUrl;
			else
				$c['address'] = $c['address'] . '/'  . $this->lastUrl;
				
			$this->lastUrl = '';
			
		}
		if ($this->acceptHeader)
			$c['headers'][] = 'Accept: ' . $this->acceptHeader;
			
		if ($this->contentType)
			$c['headers'][] = 'Content-Type: ' . $this->contentType;
		
		if ($this->lastHeaders){
			
			$c['headers'] = array_merge($c['headers'], $this->lastHeaders);
		}
				
		curl_setopt($c['handle'], CURLOPT_URL, $c['address']);
		curl_setopt($c['handle'], CURLOPT_PUT, 0);
		curl_setopt($c['handle'], CURLOPT_HTTPHEADER, array_unique($c['headers']));
		
		return $c;
	}

	/**
	 * Client type (abstracted in parent class)
	 *
	 * @return string $clientType - The client type for this DataStore
	 */
	public static function clientType(){
	
		return 'REST Clients';
	}
	
	/**
	 * Configurable query types (abstracted in parent class)
	 *
	 * Different handles can be retrieved depending on the query type. To
	 * implement this functionality, you must create an "allow" parameter
	 * for this clientType in the settings.json file, that includes an
	 * array of query types (see below) that can be performed on the 
	 * selected handle
	 *
	 * @return array $queryTypes
	 */
	public static function configurableQueryTypes(){
	
		return array('delete', 'get', 'post', 'put');
	}
	
	/**
	 * Get handle from settings (abstracted in parent class)
	 *
	 * This method obtains a curl handle for performing HTTP requests
	 *
	 * @param array $params - The parameters configured for this handle type
	 * 						  in the settings.json file
	 *
	 * @return array $settings - An array of settings, which holds the both curl handle
	 *							 handle and additional parameters defined in settings.json
	 */
	public static function getHandleFromSettings($params){
	
		$ch = curl_init();
		
		// default settings
		$settings = array_merge(array('user' => false, 'pass' => false, 'userAgent' => false, 'headers' => array()), $params);
		
		// Append the credentials to the url if a user and pass were specified
		if ($settings['user'] && $settings['pass']){
		
			$address = $settings['address'];
			
			list($protocol, $addr) = explode('://', $address);
			$settings['address'] = "{$protocol}://{$settings['user']}:{$settings['pass']}@{$addr}";
		}
		
		if (!is_array($settings['headers']))
			$settings['headers'] = array_map('trim', explode(',', $settings['headers']));
			
		curl_setopt($ch, CURLOPT_URL, $settings['address']);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		
		if ($settings['userAgent'])
			curl_setopt($ch, CURLOPT_USERAGENT, $settings['userAgent']);

		$settings['handle'] = $ch;

		return $settings;
	}
	
	/**
	 * Location
	 *
	 * Sets the location for the next HTTP request. This method is chainable
	 *
	 * @return $this
	 */
	public function location($url=''){
		
		$this->lastUrl = $url;
		return $this;
	}
	
	/**
	 * Headers
	 *
	 * Sets the headers for the next HTTP request. This method is chainable
	 *
	 * @return $this
	 */
	public function headers(array $headers=array()){
		
		$h = array();
		
		foreach ($headers as $key => $val)
			$h[] = $key . ": " . $val;
		
		$this->lastHeaders = $h;
		return $this;
	}
	
	/**
	 * Response code
	 *
	 * Get the response code for the last request
	 *
	 * @return $this
	 */
	public function responseCode(){
		
		$request = $this->lastRequest();
		
		if (isset($request['http_code']))
			return $request['http_code'];
			
		return false;
	
	}
	
	/**
	 * Get info about the last request
	 *
	 * @return array $info
	 */
	public function lastRequest(){
	
		return $this->lastInfo;
	}
	
	/**
	 * DELETE request
	 *
	 * Sends a DELETE request to either the base url (configured in settings.json), or
	 * the extended url, which is configured using the self::location($url) method in 
	 * this class.
	 *
	 * @return mixed $response - The response receieved from the web server
	 */
	public function delete(){

		$c = $this->getHandle('delete');
		
		curl_setopt($c['handle'], CURLOPT_CUSTOMREQUEST, "DELETE");
		
		$content = curl_exec($c['handle']);
		
		if ($this->accepts('json') && $this->autoConvert)
			return json_decode($content, true);
			
		return $content;
	
	}
	
	/**
	 * GET request
	 *
	 * Sends a GET request to either the base url (configured in settings.json), or
	 * the extended url, which is configured using the self::location($url) method in 
	 * this class.
	 * 
	 * @param optional array $data - An optional array of data to be sent with the request
	 * @return mixed $response - The response receieved from the web server
	 */
	public function get(array $data=array()){
		
		$c = $this->getHandle('get');
		
		$data ? $url = $c['address'] . '?' . http_build_query($data) : $url = $c['address'];
		
		curl_setopt($c['handle'], CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($c['handle'], CURLOPT_URL, $url);
		
		$content = curl_exec($c['handle']);
		$this->lastInfo = curl_getinfo($c['handle']);

		if ($this->accepts('json') && $this->autoConvert){
			
			$content = json_decode($content, true);
			
			// HBase encodes the data in base64, so we need to loop through and decode these values
			if (isset($content['Row'])){
			
				foreach ($content['Row'] as $index => $row){
				
					// decode the row key
					$row['key'] = base64_decode($row['key']);
					
					array_walk($row['Cell'], function(&$r){
						
						// decode the column
						if (isset($r['column']))
							$r['column'] = base64_decode($r['column']);
						
						// HBase puts the cell data in the $ index when using JSON
						if (isset($r['$']))
							$r['$'] = base64_decode($r['$']);
							
					});
					
					$content['Row'][$index] = $row;
				}
			}
			
		} else 	if ($this->accepts('xml') && $this->autoConvert){
			
			$dom = new DOMDocument($this->xmlVersion);
			$dom->loadXML($content);
			$dom->saveXML();
			return $dom;
		}
		
		return $content;
	
	}
	
	/**
	 * POST request
	 *
	 * Sends a post request to either the base url (configured in settings.json), or
	 * the extended url, which is configured using the self::location($url) method in 
	 * this class.
	 *
	 * @param optional array $data - An optional array of data to be sent with the request
	 * @return mixed $response - The response receieved from the web server
	 */
	public function post(array $data=array()){
	
		$c = $this->getHandle('post');
		
		curl_setopt($c['handle'], CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($c['handle'], CURLOPT_POSTFIELDS, http_build_query($data));
		
		$content = curl_exec($c['handle']);
		
		if ($this->accepts('json') && $this->autoConvert)
			return json_decode($content, true);
			
		return $content;
	
	}
	
	/**
	 * PUT request
	 *
	 * Sends a PUT request to either the base url (configured in settings.json), or
	 * the extended url, which is configured using the self::location($url) method in 
	 * this class.
	 *
	 * @return mixed $response - The response receieved from the web server
	 */
	public function put(){
	
		$c = $this->getHandle('put');
		
		curl_setopt($c['handle'], CURLOPT_PUT, 1);
		
		$content = curl_exec($c['handle']);
		
		if ($this->accepts('json') && $this->autoConvert)
			return json_decode($content, true);
			
		return $content;
	
	}
	
	/**
	 * Binary
	 *
	 * Sets the appropriate header for retrieving binary responses
	 * from the server
	 *
	 * @return $this
	 */
	public function binary(){
		
		$this->acceptHeader = 'application/octet-stream';
		return $this;
	}
	
	/**
	 * HTML
	 *
	 * Sets the appropriate header for retrieving html text responses
	 * from the server
	 *
	 * @return $this
	 */
	public function html(){
		
		$this->acceptHeader = 'text/html';
		$this->contentType  = '';
		return $this;
	}
	
	/**
	 * JSON
	 *
	 * Sets the appropriate header for retrieving json responses
	 * from the server
	 *
	 * @return $this
	 */
	public function json(){
		
		$this->acceptHeader = 'application/json';
		$this->contentType  = '';
		return $this;
	}
	
	/**
	 * Plain text
	 *
	 * Sets the appropriate header for retrieving plain text responses
	 * from the server
	 *
	 * @return $this
	 */
	public function plain(){
		
		$this->acceptHeader = 'text/plain';
		$this->contentType  = '';
		return $this;
	}
	
	/**
	 * Protocol Buffer
	 *
	 * Sets the appropriate header for retrieving protobuf (Protocol Buffer) 
	 * responses from the server
	 *
	 * @return $this
	 */
	public function protobuf(){
		
		$this->acceptHeader  = 'application/x-protobuf';
		$this->contentType  = '';
		return $this;
	}

	/**
	 * XML
	 *
	 * Sets the appropriate header for retrieving xml responses
	 * from the server
	 *
	 * @return $this
	 */
	public function xml($version="1.0"){
		
		$this->acceptHeader = 'text/xml';
		$this->contentType  = 'text/xml';
		$this->xmlVersion   = $version;
		return $this;
	}

}

?>