<?php
/**
 * Interface for a Simple Data Store
 *
 * @author <mitchseymour@gmail.com>
 * @version 0.0.1
 */
 
interface SimpleDataStore {

	/**
	 * Client Type
	 *
	 * The type of data store that the child class is implementing. This is used
	 * for caching handles, and retrieving settings from the main configuration
	 * file (etc/settings.json)
	 *
	 * @return string $clientType
	 */
	static function clientType();
	
	/**
	 * Configurable Query Types
	 *
	 * This method should return an array of configurable query types.
	 *
	 * @return array $types
	 */
	static function configurableQueryTypes();
	
	/**
	 * Get handle from settings
	 *
	 * Since each client class (i.e. data store) will have different methods
	 * for generating a handle, child classes must implement this method
	 * to handle the process of generating a handle
	 *
	 * @return mixes $handle
	 */
	static function getHandleFromSettings($params);

}

/**
 * Abstract DataStore class
 *
 * @author <mitchseymour@gmail.com>
 * @version 0.0.2
 */
abstract class DataStore implements SimpleDataStore {
	
	/**
	 * @var bool static $debug
	 *
	 * Whether or not debug mode is activated.
	 */
	private static $debug;
	
	/**
	 * @var string $environment
	 * 
	 * The current environment that the client is running in. Note: this
	 * can be overridden using the self::simulateEnvironment method.
	 */
	protected $environment;
	
	/**
	 * @var array $hooks
	 * 
	 * Hooks that have been configured at the ** instance ** level for peforming
	 * client-defined actions during certain points in the data store's
	 * execution
	 */
	protected $hooks = array();
	
	/**
	 * @var array $_hooks
	 * 
	 * Hooks that have been configured at the ** class ** level for peforming
	 * client-defined actions during certain points in the data store's
	 * execution
	 */
	protected static $_hooks = array();
	
	/**
	 * @var string $name
	 *
	 * The client identifier
	 */
	protected $name;
	
	/**
	 * @var bool static $initialized
	 *
	 * Whether or not this client has been initialized. Since multiple clients
	 * can be invoked in the same process, we wrap all of the initialization
	 * code in a single, static constructor: self::init()
	 */
	private static $initialized;
	
	/**
	 * @var array static $handles
	 *
	 * An array of cached handles
	 */
	private static $handles;
	
	/**
	 * @var string $simulatedEnvironment
	 *
	 * The simulated environment. This is called via the self::simulateEnvironment()
	 * method.
	 */
	private static $simulatedEnvironment;
	
	/**
	 * @var static $settings $settings
	 *
	 * Holds the settings for each unique client type that is instantiated
	 */
	private static $settings;
		
	/**
	 * Constructor
	 *
	 * Note: initialization is delegated to a static method (self::init)
	 * to prevent multiple clients from performing the same basic initialization
	 * tasks
	 *
	 * @return void
	 */
	public function __construct($name){
		
		$this->name = $name;
		
		// Get the environment
		if(self::$simulatedEnvironment)
			$this->environment = self::$simulatedEnvironment;
		else
			$this->environment = Config::getEnvironment();
		
		// This initialization procedure will only be called once for each type of client
		if (!self::$initialized[static::clientType()])
			$this->init();
	}
	
	/**
	 * Before Handle Returned
	 *
	 * Child classes have the option of implementing this method, which gives
	 * them greater flexibility in managing the settings/handle parameters
	 * whenever a handle is retrieved.
	 *
	 * @return void
	 */
	protected function beforeHandleReturned($handle){
		
		return $handle;
	}
	
	/**
	 * Call hook
	 *
	 * This method can be used by child classes to insert hooks during certain points
	 * of the program's execution. For example, a Data Store that runs
	 * queries against a database may use: $this->callHook('beforeQuery', $sql)
	 * before a query is actually run, which allows clients to execute their own functions
	 *  by registering a hook with: $dataStore->hook('beforeQuery', function(){});
	 *
	 * @param mixed $params - Any number of parameters, with the first parameter always being the
							  hook name, and the rest of the parameters being passed directly
							  to the registered hook function
	 *
	 * @return mixed $results - The results of the client function if defined, otherwise false
	 */
	public function callHook(){
		
		$args = func_get_args();
		
		if (count($args) < 1)
			return false;
			
		$hook = array_shift($args);
		
		$clientType = static::clientType();
		
		if (isset($this->hooks[$clientType][$hook])){
			
			// a hook was cnofigured at the instance level
			self::__log("Calling instance-level hook: {$hook}");
			return call_user_func_array($this->hooks[$clientType][$hook], $args);
		
		} else if (isset(self::$_hooks[$clientType][$hook])){
			
			// a hook was cnofigured at the class level
			self::__log("Calling class-level hook: {$hook}");
			return call_user_func_array(self::$_hooks[$clientType][$hook], $args);
			
		}
		
		return false;
	
	}
	
	/**
	 * Hook
	 *
	 * Register a hook at the instance level
	 *
	 * @param string $name - The name of the hook to register
	 * @param callable $fn - The function to execute when the hook event occurs
	 * @return DataStore $this - The current object. This allows additional methods to be chained
	 */
	public function hook($name, $fn){
	
		$this->hooks[static::clientType()][$name] = $fn;
		return $this;
	}
	
	/**
	 * Global hook
	 *
	 * Register a hook at the class level
	 *
	 * @param string $name - The name of the hook to register
	 * @param callable $fn - The function to execute when the hook event occurs
	 * @return DataStore $this - The current object. This allows additional methods to be chained
	 */
	public static function globalHook($name, $fn){
		
		self::$_hooks[static::clientType()][$name] = $fn;
	
	}
	
	/**
	 * Get handle
	 *
	 * This method determines whether or not a handle has been cached for
	 * the current client + query type, and if not, delegates to the 
	 * child's `getHandleFromSettings` method for retrieving the handle/
	 *
	 * @param string $type   - The query type to be used on the handle	 
	 * @return mixed $handle - Usually, an array containing the handle.
	 *						   However, clients can override the 
	 *						   `beforeHandleReturned` method to return
	 *						   a different data type (e.g. a resource)
	 */
	public function getHandle($type){
	
		$settings   = $this->getSettings($this->name);
		$clientType = static::clientType();
		
		if (!$settings)
			return false;

		// If the client is executing a configurable query type, get a handle that allows it
		if (in_array($type, $this->configurableQueryTypes())){
			
			
			// See if a reference to this object already exists
			if (isset(self::$handles[$clientType][$this->name][$type])){
				
				return $this->beforeHandleReturned(self::$handles[$clientType][$this->name][$type]);
			}
			
			// No reference exists, so we need to create them
			foreach ($settings as $index => $params){
				
				if (isset($params['allow'][$type])){
					
					$handle = $this->getHandleFromSettings($params);
					
					$this->saveHandle($handle, array_flip($params['allow']));
					return $this->beforeHandleReturned($handle);
	
				}
			}
			
		} else {
			
			// See if a reference to this object already exists
			if (isset(self::$handles[$clientType][$this->name]['*'])){

				return $this->beforeHandleReturned(self::$handles[$clientType][$this->name]['*']);
			}
			
			// The client is not executing a configurable query type
			$params = $settings[0];
			$handle = $this->getHandleFromSettings($params);
			$this->saveHandle($handle, array_flip($params['allow']));
			return $this->beforeHandleReturned($handle);

		}

		
	}
	
	/**
	 * Save handle
	 *
	 * This method caches handles that can retrieved at later times
	 * during the execution of a script
	 *
	 * @param mixed $handle - The handle to be cached
	 * @param array $queryTypes - The types of queries for which the
	 *							  cached handle can be used
	 * @return void
	 */
	private function saveHandle($handle, array $queryTypes){
		
		$clientType = static::clientType();
		
		// add references to this object
		foreach ($queryTypes as $allowType)
			self::$handles[$clientType][$this->name][$allowType] = $handle;
	}
			
	/**
	 * Debug
	 *
	 * Turns debug mode on and off
	 *
	 * @params optional boolean - Whether or not the client wants debug output
	 *							  echoed to the  output device
	 * @return void
	 */
	public static function debug($bool=true){
	
		self::$debug = (bool) $bool;
	}
	
	/**
	 * Init
	 *
	 * Shared initialization tasks are handled by this method.
	 *
	 * @params string $type - The type of client. Usually the top level key
	 *						  for the client settings located in etc/settings.json
	 *						  (examples: "Databases", "REST Clients", etc)
	 * @return void
	 */
	private function init(){
		
		$environment = $this->environment;
		$settings    = array();
		$type        = static::clientType();
		
		// Get the configurable query types
		$configurableQueryTypes = $this->configurableQueryTypes();
		
		if (!is_array($configurableQueryTypes))
			$configurableQueryTypes = array_map('trim', explode(',', $configurableQueryTypes));
		
		self::__log("Initializing DataStore client ({$type}) for the following environment: {$environment}");
			
		$clients  = Config::getSettings($type);
		
		if (!$clients)
			return;
			
		// iterate over the client settings to get only those configured for the current environment
		foreach ($clients as $arr){
			
			if (!isset($arr['conn']))
				continue;
			
			$connectionParams = $this->reindex($arr['conn'], 'environment');
			
			if (isset($connectionParams[$this->environment])){
				
				// The environment is explicitly configured
				$connectionParams = $connectionParams[$this->environment][0];
									
				isset($connectionParams['allow']) ? $connectionParams['allow'] = array_flip($connectionParams['allow']) : $connectionParams['allow'] = array_flip($configurableQueryTypes);
				unset($arr['conn']);
				$settings[] = array_merge($connectionParams, $arr);
			
			} elseif (isset($connectionParams['*'])){
				
				// The environment is configured by a wild card
				$connectionParams = $connectionParams['*'][0];
				isset($connectionParams['allow']) ? $connectionParams['allow'] = array_flip($connectionParams['allow']) : $connectionParams['allow'] = array_flip($configurableQueryTypes);
				unset($arr['conn']);
				$settings[] = array_merge($connectionParams, $arr);
			}
		}
			
		self::$initialized = true;
		self::$settings[$type] = $this->reindex($settings, 'alias');
	
	}
	
	/**
	 * Log messages
	 *
	 * This method is used for logging messages. The format of the logs
	 * will depend on whether or not the process is being executed by
	 * an Apache web server
	 *
	 * @return void
	 */
	protected static function __log($message){
		
		if (self::$debug){
			
			if (Config::isApachePresent()){
			
				echo "<pre>" , $message , "</pre>";
			
			} else {
			
				echo $message , PHP_EOL;
				
			}
		}
	
	}
	
	/**
	 * Get settings
	 *
	 * Retrieves the settings for the current client + client name
	 *
	 * @return mixed $settings - An array of settings if found, otherwise false
	 */
	protected function getSettings(){
	
		if (isset(self::$settings[static::clientType()][$this->name]))
			return self::$settings[static::clientType()][$this->name];
			
		return false;
		
	}
	
	/**
	 * Re-index array
	 *
	 * This method will re-index arrays up to 2 nested values
	 *
	 * @params array $arr     - The array to be re-indexed
	 * @params string $column - The name of the key whose value is to be used
	 *							as the new index
	 
	 * @params optional boolean $unset - Whether or not the values that are being moved
	 *									 to the new indexes should be unset
	 *
	 * @return array $arr - The newly re-indexed array
	 */
	public function reindex($arr, $column, $unset=true){
		
		$reindexed = array();
	
		if (is_array($arr)){
			
			if (is_array($column) && count($column) == 2) {
			
				$k1 = $column[0];
				$k2 = $column[1];
				
				foreach ($arr as $row) {
					$key1 = $row[$k1];
					$key2 = $row[$k2];
					$reindexed[$key1][$key2] = $row;		
				}
			
			} else {		
			
				foreach ($arr as $row) {
					
					$key = $row[$column];
					
					if ($unset)
						unset($row[$column]);
						
					$reindexed[$key][] = $row;
					
				}	
			}	
		}
		
		return $reindexed;
	
	}
	
	/**
	 * Simulate Environment
	 *
	 * Allows clients to override the environment setting, which is initially
	 * determined by Config::getEnvironment()
	 *
	 * @params string $environmentName - The name of the environment to be simulated
	 * @return void
	 */
	public static function simulateEnvironment($environmentName){
	
		self::$simulatedEnvironment = $environmentName;
	}
}

?>