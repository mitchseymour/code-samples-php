<?php
/**
 * Simple DB
 *
 * Usage:
 * -------------------------------------------------------------------
 * $dbh = Config::getDatabase('dbname'); // returns SimpleDB object
 * 
 * Running a query
 * -------------------------------------------------------------------
 * $sql     = "select device_name, timestamp 
 * 			   from devices
 *			   where device_name = :device";
 *
 * $params  = array('device' => 'server1'); 
 * $results = $dbh->runQuery($sql, $params);
 *
 *
 * 	Fetching the results of a query into a class
 * -------------------------------------------------------------------
 * $object = $dbh->runQuery($sql, $params, $className);
 *
 * 
 * Custom exception handling: Method 1
 * -------------------------------------------------------------------
 * $dbh->exceptionHandler(function($e){
 *	
 *		$message = $e->getMessage(); 
 *		echo "Query failed with message: $message";		
 *	
 * });
 *
 *
 * Custom exception handling: Method 2
 * -------------------------------------------------------------------
 * $dbh->runQuery($sql, $params, function($e){ echo "Query failed!";});
 *
 *
 * Hooks
 * -------------------------------------------------------------------
 * You can register hooks for customizing SimpleDB's behavior even further
 *
 * SimpleDB::globalHook('beforeQuery', function(){echo "before query!";});
 * SimpleDB::globalHook('newDatabaseSeen', function($db){echo "new database seen! {$db['host']}";});
 *
 * Hook list
 * -------------------------------------------------------------------
 * beforeQuery
 * afterQuery
 * newDatabaseSeen
 *
 *
 * Dynamically bind placeholders from array
 * -------------------------------------------------------------------
 * $values = array('Server1', 'CentOS', '6');
 * $bind   = $dbh->bindFromArray($values);
 * 
 * // SQL statement becomes...
 * $sql = "insert into servers (serverName, OS, OSVersion)
 * 		   values {$bind['placeholders']}";
 *
 * // And you can bind the params with...
 * $dbh->runQuery($sql, $bind['bindParams']);
 *
 *
 * Calling a procedure
 * -------------------------------------------------------------------
 * $dbh->callProcedure('call doStuff()');
 *
 * // Note, when the procedure does not return any results (e.g. if it
 * // only creates a temporary table, you must prevent the fetch by passing
 * // true in as the second parameter
 * $dbh->callProcedure('call doStuff2()', true);
 *
 * @author <mitchseymour@gmail.com>
 * @version 2.2.0
 */
require_once('DataStore.php');

class SimpleDB extends DataStore {
	
	/**
	 * @var int $autonum
	 * 
	 * An autonumber that is incremented when creating queries
	 * with dynamic placeholders
	 */
	private $autonum;

	/**
	 * @var callable $exceptionHandler
	 * 
	 * A method to be used when an exception is encountered
	 * while executing queries
	 */
	private $exceptionHandler;
	
	/**
	 * @var string $fetchStyle
	 * 
	 * The fetch style to be used by PDO. Includes:
	 * PDO::FETCH_ASSOC, PDO::FETCH_BOTH, PDO::FETCH_BOUND,
	 * PDO::FETCH_CLASS, PDO::FETCH_INTO, PDO::FETCH_LAZY,
	 * PDO::FETCH_NAMED, PDO::FETCH_NUM, PDO::FETCH_OBJ
	 *
	 * @url http://php.net/manual/en/pdostatement.fetch.php
	 */
	private $fetchStyle;
	
	/**
	 * @var boolean $failSilently
	 * 
	 * Whether or not this class should fail silently
	 * when an exception is thrown by PDO
	 */
	private $failSilently = true;
	
	/**
	 * @var string $forceHandle
	 * 
	 * If a forceHandle is specified, then a handle will be retrieved
	 * for the specified query type (stored in the variable) when
	 * a query is run, regardless of the actual query type.
	 *
	 * Warning, this method removes the query type restrictions set
	 * forth in the /etc/settings.json file
	 */
	private $forceHandle;
	
	/**
	 * @var string $forceReturnQueryType
	 * 
	 * Results retrieved by the SimpleDB::runQuery method vary
	 * depending on the query type. If this property is specified,
	 * the results will be returned based off the query type assigned
	 * to this property, regardless of the actual query type
	 */
	private $forceReturnQueryType;
	
	/**
	 * @var array $dbsUsed
	 * 
	 * Contains information for all of the databases used in a 
	 * process.
	 */	
	public static $dbsUsed = array();
	
	/**
	 * Constructor
	 *
	 * Initiates the auto number (used for dynamically binding parameters)
	 * and calls the parent constructor
	 */
	public function __construct($name){
				
		parent::__construct($name);
				
		$this->autonum = 0;

	}
	
	/**
	 * Auto number
	 *
	 * This class keeps an autonum count for generating dynamic
	 * parameters to be bound in a SQL statement
	 *
	 * @return int $autonum - The incremented value of the autonumber property
	 */
	private function autonum(){
		
		$this->autonum += 1;
		return $this->autonum;
		
	}
	
	/**
	 * Client type (abstracted in parent class)
	 *
	 * @return string $clientType - The client type for this DataStore
	 */
	public static function clientType(){
	
		return 'Databases';
	}
	
	/**
	 * Configurable query types (abstracted in parent class)
	 *
	 * Different handles can be retrieved depending on the query type. To
	 * implement this functionality, you must create an "allow" parameter
	 * for this clientType in the settings.json file, that includes an
	 * array of query types (see below) that can be performed on the 
	 * selected handle
	 *
	 * @return array $queryTypes
	 */
	public static function configurableQueryTypes(){
	
		return array('select', 'update', 'insert', 'delete', 'call', 'show');
	}
	
	/**
	 * Count columns
	 *
	 * A shortcut method for counting columns in a table
	 *
	 * @param string $table - The name of the table whose columns should be counted
	 * @return int $count - The number of columns in the specified table
	 */
	public function countColumns($table){
	
		$columns = $this->showColumns($table);
		
		$columns ? $count = count($columns) : $count = 0;
		return $count;
	}
	
	/**
	 * Count records
	 *
	 * A shortcut method for counting the number of records in a table
	 *
	 * @param string $table - The name of the table whose records should be counted
	 * @param optional string $whereStmt - An optional condition for the rows being counted 
	 * @return int $count - The number of columns in the specified table
	 */
	public function countRecords($table, $whereStmt="") {
		
		$records = $this->runQuery("select count(*) as count from {$table} {$whereStmt}");
		if ($records)
			return (int) $records[0]['count'];
			
		return 0;
	}
	
	/**
	 * An alais for SimpleDB::countRecords
	 */
	public function countRows($table, $whereStmt=""){
		
		return $this->countRecords($table, $whereStmt);
	}
	
	/**
	 * Get handle from settings (abstracted in parent class)
	 *
	 * This method obtains a PDO handle for executing SQL statements
	 * against a DB
	 *
	 * @param array $params - The parameters configured for this handle type
	 * 						  in the settings.json file
	 *
	 * @return array $settings - An array of settings, which holds the both PDO handle
	 *							 handle and additional parameters defined in settings.json
	 */
	public static function getHandleFromSettings($params){
		
		// extract the relevant info from the parameters defined in our settings file
		$host   = $params['host'];
		$dbStr  = $params['db'];
		$user   = $params['user'];
		$pass   = $params['pass'];
		$port   = $params['port'];
		
		self::__log("Instantiating PDO instance for {$host}");

		$type = strtolower($params['type']);

		try {
			
			if ($type == 'mysql') {
				
				// Get the connection string for a MySQL database
				if (isset($params['redbean']) && $params['redbean']){
					
					
					$connectionString  = "mysql:host={$host};dbname={$dbStr}";
					
				} else {
				
					$connectionString  = "mysql:host={$host};port={$port};dbname={$dbStr}";
					
				}
				
			} else if ($type == "oracle"){
				
				// Get the connection string for an Oracle database
				isset($params['sid']) ? $sid = $params['sid'] : $sid = '';
				$connectionString  = "oci:dbname=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = {$host})(PORT = {$port}))) (CONNECT_DATA = (SID = {$sid})))";
			
			} else {
			
				throw new Exception("Database type {$type} is not supported by SimpleDB");
			
			}
			
			$dbh = new PDO($connectionString, $user, $pass);
			
			// Set the default properties for our PDO instance
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);		
			
			// If the auto_use parameter was set, call the initial `use dbname` query
			if (isset($params['auto_use']) && $params['auto_use'] == true)
				$this->runQuery("use {$dbStr}");
			
			return array('handle' => $dbh, 'name' => $dbStr, 'host' => $host, 'port' => $port, 'type' => $type, 'users' => $user);


		} catch (PDOException $e) {
			
			// If an exception hook was specified, invoke it
			if ($this->callHook("exception", $e))
				return false;
			
			// Otherwise, throw the exception
			throw new Exception($e->getMessage());
		
		}
		
	}
	
	/**
	 * Bind parameters from array
	 *
	 * @param array $arr - An array of values that will be bound to a set of
	 *					   dynamic placeholders
	 *
	 * @return array $bind - An array containing the placeholders and the
	 *						 dynamic parameter values to be used in a SQL statement
	 */	
	public function bindFromArray(array $arr){
	
		// Prepare these values for the query
		$placeholders = array();
		$bindParams   = array();
		
		foreach ($arr as $key => $value){
			
			$i = $this->autonum();
			
			// Dynamic placeholder
			$placeholder = 'p' . $i;
			$placeholders []= ':' . $placeholder;
			$bindParams[$placeholder] = $value;
		}			
		
		$placeholders = '(' . implode(',', $placeholders) . ')';
		
		$dupKey = array();
		
		foreach ($arr as $key => $val)
			$dupKey[] = "$key = VALUES($key)";
			
		$dupKey = implode(',', $dupKey);
		
		return array('placeholders'  => $placeholders, 
					 'bindParams'    => $bindParams, 
					 'columns'       => implode(',', array_keys($arr)),
					 'duplicate_key' => $dupKey);
	
	}	
	
	public function bindMultipleFromArray(array $arr) {
	
		$placeholders = array();
		$placeholdersStr="";
		$params       = array();
		$i=0;
	
		foreach ($arr as $index => $v1) {
		
			foreach($v1 as $param => $paramValue) {
			
				 $bindParam = ":" . $param . $i; 
					
				 $tempholders[] = $bindParam;
		
				 $bindParams[$bindParam] = $paramValue;
			}
			
			$i++;
			
			$tempholdersStr = '(' . implode(',', $tempholders) . ')';
			
			$tempholders = array();
			
			$placeholders[] = $tempholdersStr;
			
		}
		
		$placeholdersStr = implode(',', $placeholders) ;
		
		return array('placeholdersStr' => $placeholdersStr, 'bindParams' => $bindParams);
	}
	
	/**
	 * Call procedure
	 *
	 * In order to execute a procedure, we need to temporarily disable PDO's
	 * ATTR_EMULATE_PREPARES property, and re-enable it after the query
	 * is executed. This class handles that process, and returns the results
	 * of the query
	 *
	 * @param string $sql - The SQL statement that invokes the procedure
	 * @param optional $boolean $preventFetch - True if the procedure is not
	 *											expected to retrieve a result set
	 *
	 * @return mixed $results - The results of the executed query, or false if
	 *							a "call" handle could not be retrieved.
	 */	
	public function callProcedure($sql, $preventFetch=false){
		
		if ($db = $this->getHandle("call")){
	
			$dbh = $db['handle'];
			$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
			$results = $this->runQuery($sql, array(), null, $preventFetch, true);
			$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
			
			return $results;
		}
		
		return false;
	}
	
	/**
	 * Handle exceptions
	 *
	 * This method is called when an exception is encountered while querying
	 *
	 * @return mixed $results
	 */	
	public function doHandleException(PDOException $e, $sql="", $params=array()){
		
		if (is_callable($this->exceptionHandler)) {
		
			return call_user_func_array($this->exceptionHandler, array($e, $sql));
		
		} else if ($sql && !$this->failSilently){
			
			$e = new Exception("The following query failed" . PHP_EOL . $sql);
			
			if ($results = self::callHook('exception', $e))
				return $results;
				
			throw $e;
				
		}
		
		self::callHook('exception', $e);
		return false;
		
	}
	
	/**
	 * Exception handler for queries
	 *
	 * Sets an exception handler to be invoked when errors are encountered
	 * while querying
	 *
	 * @param callable $c - A closure to be called when query exceptions are encountered
	 */
	public function exceptionHandler(callable $c){
		
		$this->exceptionHandler = $c;
	}
	
	/**
	 * Fail Silently
	 *
	 * This method tells SimpleDB to not throw exceptions when a problem
	 * is encountered while querying
	 *
	 * @param boolean $yes - true if SimpleDB should fail silently, otherwise false
	 * @return SimpleDB $this - The current SimpleDB object. This allows us to chain methods
	 */	
	public function failSilently($yes=true){
	
		$this->failSilently = (boolean) $yes;
		return $this;
	}
	
	/**
	 * Force handle
	 *
	 * This method tells SimpleDB to always retrieve a handle for
	 * the provided query type, regardless of the actual query type
	 * that is determined by the self::getQueryType() method
	 *
	 * @param string $queryType - The query type to be used for fetching db handles
	 * @return SimpleDB $this - The current SimpleDB object. This allows us to chain methods
	 */	
	public function forceHandle($queryType){
	
		$this->forceHandle = $queryType;
		return $this;
	}
	
	/**
	 * Force return query type
	 *
	 * This method tells SimpleDB to always retrieve a handle for
	 * the provided query type, regardless of the actual query type
	 * that is determined by the self::getQueryType() method
	 *
	 * @param string $queryType - The query type to be used for fetching db handles
	 * @return SimpleDB $this - The current SimpleDB object. This allows us to chain methods
	 */	
	public function forceReturnQueryType($queryType){
	
		$this->forceReturnQueryType = $queryType;
		return $this;
	}
	
	/**
	 * Query type
	 *
	 * Extracts the query type from a SQL statement
	 *
	 * @param string $sql - The SQL statement to examine
	 * @return string $queryType
	 */	
	public static function getQueryType($sql){
		
		list($firstWord, $restOfQuery) = explode(' ', $sql);
		return trim(strtolower(ltrim($firstWord, '(')));

	}
	
	/**
	 * Run Multiple Queries
	 *
	 * Executes multiple SQL statements
	 *
	 * @param string $sql - The SQL statement to execute
	 * @param array optional $params - An associative array of parameters to bind
	 * @param optional callable $c - A callback function to be executed if the query fails
	 * @param optional boolean $preventFetch - Whether or not PDO should attempt to fetch a result set
	 * @param optional boolean $proc - Whether or not the provided SQL statement invokes a procedure
	 *
	 * @return mixed $results
	 */	
	public function runQueries($sql, $params=array(), $c=null, $preventFetch=false, $proc=false){
		
		if ($proc){
		
			// If the current SQL statement invokes a procedure, then we force the query type to "call"
			$queryType = "call";
		
		} else {
				
			// extract the query type
			$queryType = self::getQueryType($sql);
		
		}
		
		if ($dbh = $this->getHandle($queryType)){
		
			$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
			$results = $this->runQuery($sql, $params, $c, $preventFetch);
			$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
			
			return $results;
		}
		
		return false;
	}
	
	/**
	 * Get the primary keys for a table
	 *
	 * MySQL only
	 *
	 * @param string $table - The name of the table whose primary keys should be fetched
	 * @return array $keys - An array of primary keys
	 */	
	public function primaryKey($table){
		
		$keys = array();
		
		if ($this->type == 'mysql') {
			
			if ($results = $this->runQuery("SHOW KEYS FROM {$table} WHERE Key_name = 'PRIMARY'"))
				return array_keys($this->reindex($results, 'Column_name'));
		
		}
			
		return $keys;
	}
	
	
	/**
	 * An alais for SimpleDB::primaryKey
	 */
	public function primaryKeys($table){
		return $this->primaryKey($table);
	}

	/**
	 * Run Query
	 *
	 * Executes a single SQL statement
	 *
	 * @param string $sql - The SQL statement to execute
	 * @param array optional $params - An associative array of parameters to bind
	 * @param optional callable $c - A callback function to be executed if the query fails
	 * @param optional boolean $preventFetch - Whether or not PDO should attempt to fetch a result set
	 * @param optional boolean $proc - Whether or not the provided SQL statement invokes a procedure
	 *
	 * @return mixed $results
	 */	
	public function runQuery($sql, $params = array(), $c=null, $preventFetch=false, $proc=false) {
		
		$this->callHook("beforeQuery", $this, $sql, $params);
		
		if ($this->forceHandle){
			
			// The client has specified a query type to be used for all handles
			$queryType = $this->forceHandle;
		
		} else if ($proc) {
			
			// If the query invokes a procedure, we force the type to "call"
			$queryType = "call";
		
		} else {
			
			// Extract the query type automatically
			$queryType = self::getQueryType($sql);
		
		}
		
		$stmt = false;
		
		$db = $this->getHandle($queryType);
		$dbh = $db['handle'];
		
		// Keep track of the databases we are querying. This helps with debugging and reporting
		if (!isset(self::$dbsUsed[$db['host']])){
			
			$this->callHook("newDatabaseSeen", $db);
			self::$dbsUsed[$db['host']] = true;
		}
		
		// If the client is not binding params, they can specify a callback function as the second argument
		// We check for that condition here
		if (is_callable($params) && !$c) {
			
			// Client specified a callback as the second argument
			$c = $params;
			$bindParams = false;
		
		} else {
			
			count($params) > 0 ? $bindParams = true : $bindParams = false;
		}	
		
		self::__log("Running {$queryType} query against {$db['name']} => {$db['host']}");
				
		// Check to see if the query results should be loaded into a class
		is_string($c) && class_exists($c) ? $class = $c : $class = false;

		if ($bindParams) {
				
			// Parameters are being bound!
			try {
			
				$stmt = $dbh->prepare($sql);
				
				// If the query fails, handle the error
				if (!$results = $stmt->execute($params))
					return $this->doHandleException($stmt->errorInfo(), $sql, $params);
				
				if ($queryType == 'insert' && (!$this->forceReturnQueryType || trim(strtolower($this->forceReturnQueryType)) == 'insert')) {
					
					// If an insert query is successful, return the auto incremented id of the last inserted row
					if (!$id = $dbh->lastInsertId())
						$id = true;
		
					$this->callHook("afterQuery", $this, $sql, $params);
					
					return $id;
					

				} else if ($queryType !== 'select' && !$this->forceReturnQueryType) {
					
					// If an update or delete query was successful, return true
					$this->callHook("afterQuery", $this, $sql, $params);
					return true;
				
				}

			} catch (PDOException $e) {
				
				// Query failed
				if (is_callable($c))
					return call_user_func_array($c, array($e));
					
				return $this->doHandleException($e, $sql, $params);
				
			}

		} else {
			
			// Parameters are not being bound!
			try {

				if (!$stmt = $dbh->query($sql)){
					
					$this->callHook("afterQuery", $this, $sql, array());
					return false;
				}

			} catch (PDOException $e) {
				
				if (is_callable($c))
					return call_user_func_array($c, array($e));
					
				return $this->doHandleException($e, $sql, $params);
			
			}
			
		}
		
		$rows = array();
		
		// See if the client wants us to use a certain query type for returning results
		if ($this->forceReturnQueryType)
			$queryType = $this->forceReturnQueryType;

		// Figure out how to return the results
		if ($preventFetch && $stmt){
			
			// Prevent fetch is true, so we don't call PDO::fetchAll()
			$rows = $stmt;
		
		} else if ($class && $queryType == 'select') {
			
			// The client specified a class name, so load the results into a class
			$rows = $stmt->fetchAll(PDO::FETCH_CLASS, $class);
		
		} else if ($queryType == 'select' || $queryType == 'show' || $queryType == 'call') {
			
			// Fetch all of the results
			$rows = $stmt->fetchAll($this->fetchStyle);
		
		} else if ($queryType == 'set'){
			
			// The client set a variable before executing the query, so return the next row set
			$stmt->nextRowset();
			$rows = $stmt->fetchAll($this->fetchStyle);
		
		} else {
			
			// The query was successful, but there isn't a result set to return
			$rows = true;
		}
		
		$this->callHook("afterQuery", $this, $sql, $params);

		return $rows;
	}
	
	/**
	 * Show Columns
	 *
	 * MySQL and Oracle only - Shows the column names for a table
	 *
	 * @param string $table - The name of the table whose column names should be fetched
	 * @return array $columns - An array of column names
	 */	
	public function showColumns($table) {

		$table = strtolower($table);
		$columns = array();

		if ($dbh = $this->getHandle()) {

			switch ($this->type) {

				case "oracle" :	$sql = "select column_name
										from user_tab_cols
										where lower(table_name) = '$table'
										order by column_name asc";
						
								$col = "COLUMN_NAME";
								break;

				case "mysql" :  $sql = "show columns
										from $table"; // notice how we omit the single quotes around the table name
								$col = "Field";
								break;

			}
			
			
			$results = $this->runQuery($sql);

			if (!$results)
				return false;

			foreach ($results as $row) {

				$columns[] = $row[$col];

			}
			
			return $columns;
			
		}

		return false;
	}
	
	/**
	 * Set the database type
	 *
	 * @param string $type - The type of database that the current instance should interact with
	 * @return SimpleDB $this - The current SimpleDB object. This allows us to chain methods
	 */
	public function setType($type) {
		$this->type = $type;
		return $this;
	}
	
	/**
	 * Get the database type
	 *
	 * @return string $type- The type of database that the current instance is interacting with
	 */
	public function type(){
		return $this->type;
	}
}


?>
