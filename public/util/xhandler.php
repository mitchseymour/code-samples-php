<?php

class xhandler {

	private static $displayed     = array();
	private static $errorSettings = array();
	private static $initialized   = false;
	private static $on;
	private static $path;
		
	public static function getErrorSettings(){
	
		if (!self::$initialized) {
		
			$settings = Config::getCurrentHostSettings();
			
			self::$errorSettings = array('echo' => false, 'file' => false, 'dateFormat' => 'M j, Y g:ia');
			self::$path = dirname(dirname(__FILE__));
			
			if (isset($settings['errors']) && is_array($settings['errors']))
				self::$errorSettings = array_merge(self::$errorSettings, $settings['errors']);
			
			
		}
		
		return self::$errorSettings;
	}

	public static function bottleneck($type, $comparisonString, $placeholders, $values){
		
		// First, see if this error has already been displayed/written
		if (isset(self::$displayed[$comparisonString]))
			return;
		
		self::$displayed[$comparisonString] = true;
		
		// This will tell us whether to output html or text
		$apache = Config::isApachePresent();
		
		$settings = self::getErrorSettings();

		if ($settings['echo'] == true){
		
			$apache ? $message = "<pre>{$comparisonString}</pre>" : $message = $comparisonString;
			
			echo $message;
		
		}
		
		if ($settings['file'] && is_file($settings['file'])){
			
			$date = date($settings['dateFormat']);
			file_put_contents($settings['file'], $date . "\t" . $comparisonString . PHP_EOL, FILE_APPEND);
		
		}
		
	}

	public static function handleError($number, $string, $file, $line, $context){
	
		// default message
		$comparisonString = "An error occurred. File: {$file}, Line: {$line}, String: {$string}";
		
		$search  = array(':number', ':string', ':message', ':file', ':line', ':context');
		$replace = array($number, $string, $string, $file, $line, $context);
		
		return self::bottleneck('error', $comparisonString, $search, $replace);
	
	}
	

	public static function handleException($e){

		$code    = $e->getCode();
		$file    = $e->getFile();
		$line    = $e->getLine();
		$message = $e->getMessage();
		
		// default message
		$comparisonString = "An exception occurred. File: {$file}, Line: {$line}, Code: {$code}, String: {$message}";
		
		$search  = array(':code', ':string', ':message', ':file', ':line');
		$replace = array($code, $message, $message, $file, $line);
		
		return self::bottleneck('exception', $comparisonString, $search, $replace);
	}

	public static function handleFatal() {

		$code = 'unknown';
		$file = "unknown file";
		$string  = "shutdown";
		$doe   = E_CORE_ERROR;
		$line = 0;

		$error = error_get_last();

		if( $error !== NULL) {

			$code        = $error["type"];
			$file        = $error["file"];
			$line        = $error["line"];
			$lineContent = "";
			$string     = $error["message"];
	
			if (is_file($file)) {
				$lines = file($file);
				//$lineContent = trim($lines[$line-1]);
			}
			
		}
		
		if ($string == 'shutdown')
			return false;
			
		// default message
		$comparisonString = "An error occurred. File: $file, Line: $line, String: $string";
		
		$search  = array(':code', ':string', ':message', ':file', ':line');
		$replace = array($code, $string, $string, $file, $line);
		
		return self::bottleneck('exception', $comparisonString, $search, $replace);
			
	}
}

?>
