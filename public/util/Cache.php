<?php
/**
 * Cache
 *
 * A class for abstracting cache-related functions. This was built
 * in anticipation of better caching options in PHP 5.5+. Instead of
 * changing the apc functions everywhere in the dashboard, we can
 * just update this class.
 *
 * This is not meant to replace more robust options like phpfastcache, 
 * although this class may end up being a wrapper for one of these
 * libraries in the future.
 *
 * Usage:
 * -------
 * $item = Cache::get("item"); // Get something
 * $item = Cache::set("item", "value"); // Store something
 *
 * @author Mitch Seymour <mitchseymour@gmail.com>
 * @version 0.1
 */
class Cache {
	
	/**
	 * @var boolean $preventFetch
	 * 
	 * If set to true, then the self::get() method
	 * will always return false
	 */
	private static $preventFetch = false;
	
	/**
	 * @var boolean $debug
	 * 
	 * If set to true, then debug text will be echoed to
	 * the client
	 */
	private static $debug = false;
	
	/**
	 * debug
	 * 
	 * @param optional boolean $bool - Whether or not debug mode
	 *								   should be activated
	 *
	 * @return void
	 */
	public static function debug($bool=true){
	
		self::$debug = (boolean) $bool;
	}
	
	/**
	 * getter
	 * 
	 * @param string $key - The key to be used for retrieving the cached 
	 *						value
	 *
	 * @return mixed $val - The value that was cached for the provided key,
	 *						or false if either apc is not loaded, or the value
	 *						does not exist
	 */
	public static function get($key){
		
		// All fetches were prevented
		if (self::$preventFetch)
			return false;
			
		if (function_exists('apc_fetch') && $val = apc_fetch($key)){
			
			if (self::$debug)
				echo "<pre>getting $key from cache</pre>";
				
			// Cached
			return $val;
		
		}
		
		// Not cached
		return false;
	}
	
	/**
	 * setter
	 * 
	 * @param string $key - The key to be used for retrieving the cached 
	 *						value
	 *
	 * @param string $key - The value to be cached
	 *
	 * @param optional int $ttl - The time-to-live, in seconds, of the
	 *							  cached value
	 *
	 * @return mixed $val - The value that was cached
	 */
	public static function set($key, $val, $ttl=120){
		
		if (function_exists('apc_store')) {
	
			apc_store($key, $val, $ttl);
			
		}
		
		return $val;

	}
	
	private static function log($message){
	
		if (self::$debug)
			echo "<pre>{$message}</pre>";
	}
	
	public static function preventFetch($bool=true){
	
		$bool = (boolean) $bool;
		self::$preventFetch = $bool;
	}
	
	public static function removeExpired(){
		
		$dateFormat = 'm/d/Y h:i:sa';
		
		if (!function_exists('apc_cache_info'))
			return false;
		
		 $cache = apc_cache_info('user');
	 
		if (!isset($cache['cache_list']) || !is_array($cache['cache_list']))
			return false;
		
		foreach ($cache['cache_list'] as $arr) {
	
			if ($arr['creation_time'] + $arr['ttl'] < time()) {
				
				self::log($arr['info'] . ' expires at ' . date($dateFormat, $arr['creation_time'] + $arr['ttl']) . ' (created at ' . date($dateFormat, $arr['creation_time']) . ' with a ttl of ' . $arr['ttl'] . ')');
				apc_delete($arr['info']);
			
			} else {
				
				self::log("Okay: " . $arr['info']);
			
			}
			
		}

	}

}

?>