<?php
/**
 * Secure encryption class
 *
 * Makes encryption a little easier. 
 *
 *  Sample Usage:
 *
 *  $e = Secure::encryptme("Mitch Seymour");
 *  $d = Secure::decryptme($e);
 *
 * @author Mitch Seymour <me@mitchseymour.com>
 * @copyright Copyright (c) 2013, Mitch Seymour
 */

class Secure {

	private static $base64   = true;
	private static $cipher   = MCRYPT_RIJNDAEL_128;
	private static $key_loc  = 'secure/key';
	private static $salt_loc = 'secure/salt';	
	private static $mode     = MCRYPT_MODE_ECB;	

	public static function get_key(){

		$key = file_get_contents(self::$key_loc);
		return $key;
	
	}

	public static function get_salt(){

		$salt = file_get_contents(self::$salt_loc);
		return $salt;
	}

	/**
	 *  blowfish
	 *
	 *  A hash that is not reversible. Should be used for passwords,
	 *  or other types of data whose values only need to be verified, and
	 *  not reconstrcuted or decrypted. For the latter, use the encryptme and
	 *  decryptme methods in this class.
	 *
	 *  @param $string - the string or data to be hashed
	 */

	public static function blowfish($string){

		$salt = strtr(base64_encode(openssl_random_pseudo_bytes(18)), '+', '.');
		$hash = crypt($string, sprintf('$2y$%02d$%s', 13, $salt));

		return $hash;

	}

	public static function encryptme($string){

		$key  = self::get_key();
		$enc  = mcrypt_encrypt(self::$cipher, $key, $string, self::$mode);
		
		if (self::$base64)
			$enc = base64_encode($enc);
	
		return $enc;
	}

	public static function decryptme($string){

		$key  = self::get_key();
                
		if (self::$base64)
            $string = base64_decode($string);

		$decr = mcrypt_decrypt(self::$cipher, $key, $string, self::$mode);
		
		return $decr;

	}

	public static function hash($string){

		return self::blowfish($string);

	}
}

?>
