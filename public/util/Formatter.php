<?php

class Formatter {

	private function __construct(){}

	public static function Date($value){

		$date = date('D m/d/Y', strtotime($value));
		return $date;	
	}

	public static function Float($value){

                $decimalPlaces = 2;

                $value = round($value, $decimalPlaces);

                if (strlen($value) < $decimalPlaces + 2) {
                        $value .= '0';
                }
                return $value;
        }

	public static function Int($value){
		
		$value = (int) $value;
		$number = number_format($value);
		return $number;
	}
}

?>
