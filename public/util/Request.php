<?php
/**
 *  Request class
 *
 *  @version 1.2
 *  @author <mitch.seymour@lexisnexis.com
 */

class Request {

    public static function removeProtocol($url){

	// feel free to add more protocols
	$protocols = array('file', 'ftp', 'http', 'https', 'ldap', 'ldaps');

	foreach ($protocols as $index => $protocol) {

	    $protocols[$index] = $protocol . '://';
	}

	$url = str_replace($protocols, '', $url);
	return $url;

    }

}
