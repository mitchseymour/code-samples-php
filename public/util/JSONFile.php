<?php

class JSONFile {

	public static function toArray($file){

    	$settings = file_get_contents($file, true);
        $settings = str_replace("\n","", $settings);
        $settings = json_decode($settings, true);

		if (is_array($settings))
			return $settings;

		return false;
	}
	
	public static function toD3($file){
	
		$settings = self::toArray($file);
		
		if (is_array($settings)) {
			
			toD3Array($settings);
						
		}
		
		
		
	}

}

function toD3Array(array $arr, $indent='', $defaultKey = false) {
    if ($arr) {
        foreach ($arr as $index => $value) {
            if (is_array($value)) {
                //
                toD3Array($value, $indent . '--', $index);
                
            } else {
                //  Output
                
                if ($defaultKey)
                	$index = $defaultKey;
                echo "$indent $value ($index)<br/>";
            }
        }
    }
}

?>
