<?php

require_once('etc/parsers/ConfParser.php');

class ThemeConfig extends ConfParser {

	public static $settingsFile = '../themes.json';
	public static $theme;
	public static $themeSettings = array();

	public static function getSettingsFile() {
	
		isset(self::$settingsFile) ? $file = __dir__ . DIRECTORY_SEPARATOR . self::$settingsFile : $file = false;
		return $file;
	}
	
	public static function getThemeSettings() {
	
		if (!isset(self::$theme))
			throw new Exception("You must first call ThemeConfig::setTheme before accessing a theme's settings");
	
		$name = self::$theme;
	
		if (isset($themeSettings[$name]) && is_array($themeSettings[$name]))
			return $themeSettings[$name];

		$themes = self::getSettings("Themes");
		
		if (is_array($themes)) {
		
			foreach ($themes as $theme) {
			
				if (isset($theme['name']) && $theme['name'] == $name) {
					self::$themeSettings[$name] = $theme;
					return $theme;
				}
				
			}
		}
		
		throw new Exception("Tried to access theme that has not been configured: $name");
	
	}
	
	public static function setTheme($theme) {
	
		self::$theme = $theme;
	}
	
	public static function getCSS($group="defaults") {
		
		$settings = self::getThemeSettings();
		
		if (is_array($settings) && isset($settings[$group]["stylesheets"])) {
		
			return $settings[$group]["stylesheets"];
		}
	
	}
	
	public static function getJS($group="defaults") {
		
		$settings = self::getThemeSettings();
		
		if (is_array($settings) && isset($settings[$group]["javascripts"])) {
		
			return $settings[$group]["javascripts"];
		}
	
	}
	
	
	public static function getPlugins($group="defaults") {
	
		$settings = self::getThemeSettings();
		
		if (is_array($settings) && isset($settings[$group]["plugins"])) {
			return $settings[$group]["plugins"];
		}
	
	}
	
	

}

?>