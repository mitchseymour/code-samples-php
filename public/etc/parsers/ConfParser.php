<?php

require_once('util/JSONFile.php');

abstract class ConfParser {

	protected static $decodedSettings = array();
	
	public static function decodeSettingsFile() {
	
		$file = static::getSettingsFile();
	
		// check to see if we have already read this settings file
		if (isset(self::$decodedSettings[$file])) {
			
			return self::$decodedSettings[$file];
			
		} else if (file_exists($file)) {
		
			return JSONFile::toArray($file);
			
		} else {
		
			return array();
		}
	}
	
	public static function getSettings($key) {

        $settings = static::decodeSettingsFile();
        
        if ($key == '*') return $settings;

        if (is_array($settings) && isset($settings[$key]))
        	return $settings[$key];

        return false;
    }

}

?>