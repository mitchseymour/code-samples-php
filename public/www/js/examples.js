map = new MoonMap('#demo1', {
	moonSelector: '.sSelector'	
});

map.moonEvent('click', function(moon, map){
	
	console.log('Moon: ');
	console.log(moon);
	console.log('Map: ');
	console.log(map);
	
	moon.className = moon.className + ' active';

});

map.startCarousel(300);


map2 = new MoonMap('#demo2', {

	active: function(map){
			
		var phase = map.current() + 1;
		
		document.querySelector('#demo2 .center-title').innerHTML = '<span style="font-size:13px">Phase ' + phase + '</span>';
	
	},
	moonSelector: '.sSelector2'
});

map2.startCarousel(1200);

map3 =  new MoonMap('#demo3', {
	
	active: function(map){
	
	},
	content: function(i){
		
		return '';
		//return i + 1;
	},
	n:4, 
	radius: 100
	
});

map3.startCarousel(1100);

setInterval(function(){

	map3.rotateMoons(map3.lastRotation + 90);
	
}, 1100);
