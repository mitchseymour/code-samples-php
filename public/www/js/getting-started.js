var icons = ['edit', 'star', 'spinner', 'music', 'photo', 'support', 'paint-brush', 'rss'];

map = new MoonMap('#demo1', {
	n: 8,
	content: function(index, moon){
	    return '<span><i class="fa fa-' + icons[index-1] + '"></i></span>';
	
	}
});

map.startCarousel(700);