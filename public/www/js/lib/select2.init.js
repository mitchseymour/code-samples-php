(function($) {
    $.fn.getAttributes = function() {
        var attributes = {};

        if( this.length ) {
            $.each( this[0].attributes, function( index, attr ) {
                attributes[ attr.name ] = attr.value;
            } );
        }

        return attributes;
    };
})(jQuery);

$(function(){

	var attributes, count, custom, data, el, input, inputClass, multiple, oID, options, oText, seleced, tagName;

	count = 0;
	selected = [];

	$('.select2').each(function(){

	    attributes = $(this).getAttributes();
	    el = $(this);
	    inputClass = 'multiConverted' + count;
	    if ($(this).hasClass('multiple')) multiple = true;
	    if ($(this).hasClass('custom')) custom = true;

	    tagName    = $(this).prop('tagName').toLowerCase();
	    count++;

	    // default options
	    options = {};

	    if (multiple && tagName == 'select'){

		// get all of the options for this select
		data = {};
		data.results = [];

		$(this).children('option').each(function(){

		    oID   = $(this).val();
		    oText = $(this).text();

		    if ($(this).attr('selected', 'selected')){

			selected.push(oID);

		    }

		    data.results.push({id: oID, text: oText});

		});

		$(this).replaceWith('<input type="hidden" name="' + attributes['name'] + '" class="span3 ' + inputClass + '" multiple>');

		el = $('.' + inputClass);
		$.each(attributes, function(key, value){

		    if (key !== 'name')
			el.attr(key, value);

		});

		options['query'] =  function(query){
					query.callback(data);
				    }
		options['multiple'] = true;


	    }

	    if (custom){

		options['createSearchChoice'] = function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} };

	    }

	    // set the default values if there are any
	    if (selected.length > 0){

		selected = selected.join(',');
		el.val(selected);
	    }

	    // this will only be called if there are selected values
	    options['initSelection'] = function(element, callback) {

		var data = [];

		$(element.val().split(",")).each(function () {
		    data.push({id: this, text: this});
		});

		callback(data);

	    }

	    el.select2(options);
	});
});
