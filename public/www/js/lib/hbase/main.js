$(window).load(function(){
    $("#example-basic").treetable({ expandable: true });

    /*$("#example-basic").on("onNodeCollapse", function() {
        alert( "A branch has collapsed" );
    });*/
});
$(document).ready(function(){
	

	$('.chart').each(function () {
		id = $(this).attr('id');
		var container = document.getElementById(id);
		var arr = id.split("_");
		var startTime = arr[1];
		var endTime = arr[2];
		var duration = arr[3];
		var minTime = arr[4];
		var maxTime = arr[5];

		var d2 = [
			[startTime,1.5,endTime," "]
	    ];

		var data = [{label:"",data:d2,color:"#B2A7C8",gantt:{connectSteps:{show:true,color:"rgb(0,255,0)" } } }];

		var options = {
			series:{editMode: 'v',editable:true,
					gantt: {active:true,show:true,barHeight:1.0 }
					},
			xaxis:{
				min:minTime,max:maxTime,mode:"time"
			},
			yaxis: {min:1.0,max:2.0,ticks:[[1,""]]},
			grid:   { hoverable: true, clickable: true, editable: true, borderWidth: 1}
		};
		var p = $.plot($("#"+id), data, options);
	});
	//$("#example-basic").treetable({ expandable: true });
});

