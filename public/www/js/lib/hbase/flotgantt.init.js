function makeFlottGanttChart(selector, data, userOptions, callback){
	
	// may need to divide these by 1000?
	var minTime = data.minTime;
	var maxTime = data.maxTime;
	var startTime = data.startTime;
	var endTime = data.endTime;
	
	console.log('min time: ' + new Date(minTime/1000));
	console.log('max time: ' + new Date(maxTime/1000));
	console.log('start time: ' + new Date(startTime/1000));
	console.log('end time: ' + new Date(endTime/1000));
	console.log('-----------------------');
	
	var d = [[startTime,1.5,endTime," "]];

	var data = [{label:"",data:d,color:"#B2A7C8",gantt:{connectSteps:{show:true,color:"rgb(0,255,0)" } } }];

	var options = {
		series:{editMode: 'v',editable:true,
			gantt: {active:true,show:true,barHeight:1.0 }
		      },
		xaxis:{min:minTime,max:maxTime,mode:"time"},
		yaxis:{min:1.0,max:2.0,ticks:[[1,""]]},
		grid :{hoverable: true, clickable: true, editable: true, borderWidth: 1}
	};
	
	var p = $.plot($(selector), data, options);
	$(selector).on('click', function(){
	
		alert('Show something');
	});
}
