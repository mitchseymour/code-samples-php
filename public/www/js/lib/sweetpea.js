/*
A simple jQuery plugin for building sweet progress bars

About License:
Copyright (C) 2013 Mitchell Seymour
You may use sweetpea plugin under the terms of the MIT Licese.
*/
(function($){	

	$.fn.sweetpea = function(initVal){
		
		var a = $("<div>").addClass('sweetpea-bar'); // bar
		var b = $("<div>").addClass("sweetpea-value"); // value
		var c = $("<i>").addClass("icon-white sweetpea-icon"); // icon
		var d = c.wrap("<div></div>").parent().addClass('sweetpea-icon-circle'); // icon container
		var e = $("<div>").addClass("sweet-pea-container").append(a).append(b).append(d);
		
		e.append($("<input type='hidden' class='sweetpea'>"));
		
		return this.each(function(){
		
			var icon = $(this).attr('data-icon');
			var oldClasses = $(this).attr("class");
			var pea = e.clone();
			var val = 0;
			
			if (initVal)
				val = initVal;
			else
				val = $(this).val();
				
			if (val > 100) 
				val = 100;
			else if (val < 1)
				val = 0;
				
			pea.find(".sweetpea-value").css("width", val + "%");
			pea.find(".sweetpea-icon").addClass("icon-" + icon);
			pea.addClass(oldClasses);
			$(this).replaceWith(pea);
			
		});
		
		}
		
}(jQuery));