
$(document).ready(function() {

	$("#submit").click(function(e){

		$('#successful').modal('show');
		$('#loadShow').show();
		$('#loadHide').hide();
		e.preventDefault();
		/* 
		var valid='';
		var required=' is required';
		var statusArr=$('form #statusArr').val();
		var dateRangeSingle=$('form #dateRangeSingle').val();
		var endTime=$('form #endTime').val();
		var businessUnitArr=$('form #businessUnitArr').val();
		var envArr=$('form #envArr').val();
		var divArr=$('form #divArr').val();
		var otherDiv=$('form #otherDiv').val();
		var subdivArr=$('form #subdivArr').val();
		var otherSubDiv=$('form #otherSubDiv').val();
		var compArr=$('form #compArr').val();
		var otherComp=$('form #otherComp').val();
		var stateArr=$('form #stateArr').val();
		var catErrorArr=$('form #catErrorArr').val();
		var otherErr=$('form #otherErr').val();
		var emailR=$('form #emailR').val();
		var Sticket=$('form #Sticket').val();
		var CAUSEDBYTKT=$('form #CAUSEDBYTKT').val();
		var incidentsMin=$('form #incidentsMin').val();
		var description=$('form #description').val();
		var email=$('form #email').val();
		var emailSubject=$('form #emailSubject').val();
		var startDate=$('form #startDate').val();
		var inciPart=$('form #inciPart').val();
		var sTicket=$('form #sTicket').val();
		var cTicket=$('form #cTicket').val();
		var currStatus=$('form #currStatus').val();
		var actionTaken=$('form #actionTaken').val();
		var nextUpdate=$('form #nextUpdate').val();
		var inciDesc=$('form #inciDesc').val();
		var businessImpact=$('form #businessImpact').val();
		var statusSummary=$('form #statusSummary').val();
		 */
		 
		var formData=$('#perfectDaysForm').serialize();
		submitForm(formData);
		console.log(formData);

	});
/***********************What element to hide**********************************/
	
	//Incident Report Tab
	$("#Application").hide();
	$("#Network").hide();
	$("#3rd-Party").hide();
	$("#Data").hide();
	$("#businessUnitArr").hide();

	$("#otherDiv").hide();
	$("#otherSubDiv").hide();
	$("#otherComp").hide();
	$("#otherErr").hide();

	$("#endTime").hide();
	$("#restore").hide();
	$("#restore0").hide();
	$("#restore1").hide();
	$("#restore2").hide();
	$("#restore3").hide();
	$("#restore4").hide();
	$("#restore5").hide();
	$("#dateTime").hide();
	
	//Operational Measurements Tab
	$("#mcomChart").hide();
	$("#appChart").hide();
	$("#thirdPartyChart").hide();
	$("#ibmChart").hide();
		
/************************What element to show*************************************/		
	//Incident Report Tab
	$("#Infrastructure").show();

	//Operational Measurements Tab
	$("#Infrastructure").show();
	$("#ifsChart").show();
	$("#dateSingle").show();
		
/************************Incident Report Tab*************************************/		
	$("#statusArr").change(function(){
		var id3=document.getElementById("statusArr").value;
		switch (id3){
		case "INITIAL":
		case "UPDATE":
		//hide
		$("#restore").hide();
		$("#restore0").hide();
		$("#restore1").hide();
		$("#restore2").hide();
		$("#restore3").hide();
		$("#restore4").hide();
		$("#restore5").hide();
		$("#dateTime").hide();
		$("#businessUnitArr").hide();
		//show
		$("#initial").show();
		$("#dateSingle").show();
		break;
		
		case "SERVICE RESTORED":
		case "INITIAL/SERVICE RESTORED":
		//hide
		$("#initial").hide();
		$("#dateSingle").hide();

		//show
		$("#restore").show();
		$("#restore0").show();
		$("#restore1").show();
		$("#restore2").show();
		$("#restore3").show();
		$("#restore4").show();	
		$("#restore5").show();
		$("#dateTime").show();
		$("#endTime").show();
		$("#businessUnitArr").show();
		break;
		}	
	});	
	$("#divArr").change(function(){
		var id=document.getElementById("divArr").value;
		switch(id){
		case "Infrastructure":
		$("#Infrastructure").show();
		$("#Application").hide();
		$("#Network").hide();
		$("#Data").hide();
		$("#3rd-Party").hide();
		$("#otherDiv").hide();
		$("#otherSubDiv").hide();
		$("#otherComp").hide();
		break;
		
		case "Application":
		$("#Infrastructure").hide();
		$("#Application").show();
		$("#Network").hide();
		$("#Data").hide();
		$("#3rd-Party").hide();
		$("#otherDiv").hide();
		$("#otherSubDiv").hide();
		$("#otherComp").hide();
		break;
		
		case "Network":
		$("#Infrastructure").hide();
		$("#Application").hide();
		$("#Network").show();
		$("#Data").hide();
		$("#3rd-Party").hide();
		$("#otherDiv").hide();
		$("#otherSubDiv").hide();
		$("#otherComp").hide();
		break;
		
		case "Data":
		$("#Infrastructure").hide();
		$("#Application").hide();
		$("#Network").hide();
		$("#Data").show();
		$("#3rd-Party").hide();
		$("#otherDiv").hide();
		$("#otherSubDiv").hide();
		$("#otherComp").hide();
		break;
		
		case "3rd-Party":
		$("#Infrastructure").hide();
		$("#Application").hide();
		$("#Network").hide();
		$("#Data").hide();
		$("#3rd-Party").show();
		$("#otherDiv").hide();
		$("#otherSubDiv").hide();
		$("#otherComp").hide();
		break;
		
		case "Other":
		$("#Infrastructure").hide();
		$("#Application").hide();
		$("#Network").hide();
		$("#Data").hide();
		$("#3rd-Party").hide();
		$("#otherDiv").show();
		$("#otherSubDiv").show();
		$("#otherComp").hide();
		break;
		}
	});
	$("#Infrastructure").change(function(){
	var id1=document.getElementById("Infrastructure").value;
		if (id1=="Other"){
		$("#otherSubDiv").show();
		}else{
		$("#otherSubDiv").hide();
		}
	});
	$("#Application").change(function(){
	var id1=document.getElementById("Application").value;
		if (id1=="Other"){
		$("#otherSubDiv").show();
		}else{
		$("#otherSubDiv").hide();
		}
	});
	$("#Network").change(function(){
	var id1=document.getElementById("Network").value;
		if (id1=="Other"){
		$("#otherSubDiv").show();
		}else{
		$("#otherSubDiv").hide();
		}
	});
	$("#Data").change(function(){
	var id1=document.getElementById("Data").value;
		if (id1=="Other"){
		$("#otherSubDiv").show();
		}else{
		$("#otherSubDiv").hide();
		}
	});
	$("#3rd-Party").change(function(){
	var id1=document.getElementById("3rd-Party").value;
		if (id1=="Other"){
		$("#otherSubDiv").show();
		}else{
		$("#otherSubDiv").hide();
		}
	});
	$("#compArr").change(function(){
	var id1=document.getElementById("compArr").value;
		if (id1=="Other"){
		$("#otherComp").show();
		}else{
		$("#otherComp").hide();
		}
	});
	$("#catErrorArr1").change(function(){
	var id2=document.getElementById("catErrorArr1").value;
		if (id2=="Other"){
		$("#otherErr").show();
		}else{
		$("#otherErr").hide();
		}
	});
	function submitForm(formData){
		$.ajax({
			type:'POST',
			url:'/siteMetrics/insertData.php',
			data: formData,
			dataType:'json',
			cache: false,
			timeout: 7000,
			success: function(){

					},
			error: function(){
			
			},
			complete: function(){
				$('#loadShow').hide();
				$('#loadHide').show();
				}
		});

	};
});
