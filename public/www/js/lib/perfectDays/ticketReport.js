//total changes per year
var svg3 = dimple.newSvg("#chartContainer3", "100%", 400);
var dataX = page.get('totalChange');
var chart3 = new dimple.chart(svg3, dataX);
chart3.setBounds(50, 25, "90%", 340);
var x3 = chart3.addCategoryAxis("x",["type","year"]);
x3.addOrderRule(["2013","2014"],true);
chart3.addMeasureAxis("y", "Tickets Closed");
chart3.addSeries("year", dimple.plot.bar);
var myLegend3 = chart3.addLegend(40, 0, "100%", "100%", "left");
chart3.draw();

// This is a critical step.  By doing this we orphan the legend. This
// means it will not respond to graph updates.  Without this the legend
// will redraw when the chart refreshes removing the unchecked item and
// also dropping the events we define below.
chart3.legends = [];
// Get a unique list of Owner values to use when filtering
var filterValues = dimple.getUniqueValues(dataX, "year");
// Get all the rectangles from our now orphaned legend
myLegend3.shapes.selectAll("rect")
  // Add a click event to each rectangle
  .on("click", function (e) {
		// This indicates whether the item is already visible or not
		var hide = false;
		var newFilters = [];
		// If the filters contain the clicked shape hide it
		filterValues.forEach(function (f) {
			
		  if (f === e.aggField.slice(-1)[0]) {
			hide = true;

		  } else {
			newFilters.push(f);

		  }
		});
		// Hide the shape or show it
		if (hide) {
		  d3.select(this).style("opacity", 0.2);
		} else {
		  newFilters.push(e.aggField.slice(-1)[0]);
		  d3.select(this).style("opacity", 0.8);
		}	
		// Update the filters
		filterValues = newFilters;
		// Filter the data
		chart3.data = dimple.filterData(dataX, "year", newFilters);
		// Passing a duration parameter makes the chart animate. Without
		// it there is no transition
		chart3.draw(800);
	});

 
//changes by month for the year
var svg4 = dimple.newSvg("#chartContainer4", "100%", 400);
var data4 = page.get('changeData');
var chart4 = new dimple.chart(svg4, data4);
chart4.setBounds(40, 25, "90%", 340);

var x4 = chart4.addCategoryAxis("x", ["month","year"]);
x4.addOrderRule(["February","March","April","May","June","July",
							"August","September","October","November","December","January"]
				);
chart4.addMeasureAxis("y", "Tickets Closed");
chart4.addSeries("year", dimple.plot.bar);
var myLegend4=chart4.addLegend(40, 0, "100%", "100%", "left");
chart4.draw();
// This is a critical step.  By doing this we orphan the legend. This
// means it will not respond to graph updates.  Without this the legend
// will redraw when the chart refreshes removing the unchecked item and
// also dropping the events we define below.
chart4.legends = [];
// Get a unique list of Owner values to use when filtering
var filterValues4 = dimple.getUniqueValues(data4, "year");
// Get all the rectangles from our now orphaned legend
myLegend4.shapes.selectAll("rect").on("click", function (e) {
	var hide = false;
	var newFilters = [];
	// If the filters contain the clicked shape hide it
	filterValues4.forEach(function (f) {	
	  if (f === e.aggField.slice(-1)[0]) {
		hide = true;

	  } else {
		newFilters.push(f);

	  }
	});
	// Hide the shape or show it
	if (hide) {
	  d3.select(this).style("opacity", 0.2);
	} else {
	  newFilters.push(e.aggField.slice(-1)[0]);
	  d3.select(this).style("opacity", 0.8);
	}	
	// Update the filters
	filterValues4 = newFilters;
	// Filter the data
	chart4.data = dimple.filterData(data4, "year", newFilters);
	// Passing a duration parameter makes the chart animate. Without
	// it there is no transition
	chart4.draw(800);
});



//changes by owner by year
var svg2 = dimple.newSvg("#chartContainer2", "100%", 400);
var data2 = page.get('changesByOwner');
var chart2 = new dimple.chart(svg2, data2);
chart2.setBounds(40, 25, "90%", 300);

var x2 = chart2.addCategoryAxis("x", ["name","year"]);
x2.addOrderRule("year");
chart2.addMeasureAxis("y", "Tickets Closed");
chart2.addSeries("year", dimple.plot.bar);
var myLegend2=chart2.addLegend(40, 0, "100%", "100%", "left");
chart2.draw();

// This is a critical step.  By doing this we orphan the legend. This
// means it will not respond to graph updates.  Without this the legend
// will redraw when the chart refreshes removing the unchecked item and
// also dropping the events we define below.
chart2.legends = [];
// Get a unique list of Owner values to use when filtering
var filterValues2 = dimple.getUniqueValues(data2, "year");
// Get all the rectangles from our now orphaned legend
myLegend2.shapes.selectAll("rect").on("click", function (e) {
	var hide = false;
	var newFilters = [];
	// If the filters contain the clicked shape hide it
	filterValues2.forEach(function (f) {	
	  if (f === e.aggField.slice(-1)[0]) {
		hide = true;

	  } else {
		newFilters.push(f);

	  }
	});
	// Hide the shape or show it
	if (hide) {
	  d3.select(this).style("opacity", 0.2);
	} else {
	  newFilters.push(e.aggField.slice(-1)[0]);
	  d3.select(this).style("opacity", 0.8);
	}	
	// Update the filters
	filterValues2 = newFilters;
	// Filter the data
	chart2.data = dimple.filterData(data2, "year", newFilters);
	// Passing a duration parameter makes the chart animate. Without
	// it there is no transition
	chart2.draw(800);
});

//changes by status by priority by year
var svg1 = dimple.newSvg("#chartContainer1", "100%", 400);
var data1 = page.get('changesByStatus');
var chart1 = new dimple.chart(svg1, data1);
chart1.setBounds(40, 25, "90%", 340);
var x1 = chart1.addCategoryAxis("x", ["status","priority"]);
//x1.addOrderRule("year");
chart1.addMeasureAxis("y", "Tickets Closed");
chart1.addSeries("priority", dimple.plot.bar);
var myLegend1=chart1.addLegend(40, 0, "100%", "100%", "left");
chart1.draw();

// This is a critical step.  By doing this we orphan the legend. This
// means it will not respond to graph updates.  Without this the legend
// will redraw when the chart refreshes removing the unchecked item and
// also dropping the events we define below.
chart1.legends = [];
// Get a unique list of Owner values to use when filtering
var filterValues1 = dimple.getUniqueValues(data1, "priority");
// Get all the rectangles from our now orphaned legend
myLegend1.shapes.selectAll("rect").on("click", function (e) {
	var hide = false;
	var newFilters = [];
	// If the filters contain the clicked shape hide it
	filterValues1.forEach(function (f) {	
	  if (f === e.aggField.slice(-1)[0]) {
		hide = true;

	  } else {
		newFilters.push(f);

	  }
	});
	// Hide the shape or show it
	if (hide) {
	  d3.select(this).style("opacity", 0.2);
	} else {
	  newFilters.push(e.aggField.slice(-1)[0]);
	  d3.select(this).style("opacity", 0.8);
	}	
	// Update the filters
	filterValues1 = newFilters;
	// Filter the data
	chart1.data = dimple.filterData(data1, "priority", newFilters);
	// Passing a duration parameter makes the chart animate. Without
	// it there is no transition
	chart1.draw(800);
});

//changes by status by priority by year 2013
var svg0 = dimple.newSvg("#chartContainer0", "100%", 400);
var data0 = page.get('changesByStatus2013');
var chart0 = new dimple.chart(svg0, data0);
chart0.setBounds(40, 25, "90%", 340);

chart0.addCategoryAxis("x", ["status","priority"]);
//x0.addOrderRule("year");
chart0.addMeasureAxis("y", "Tickets Closed");
chart0.addSeries("priority", dimple.plot.bar);
var myLegend0=chart0.addLegend(40, 0, "100%", "100%", "left");
chart0.draw();

// This is a critical step.  By doing this we orphan the legend. This
// means it will not respond to graph updates.  Without this the legend
// will redraw when the chart refreshes removing the unchecked item and
// also dropping the events we define below.
chart0.legends = [];
// Get a unique list of Owner values to use when filtering
var filterValues0 = dimple.getUniqueValues(data0, "priority");
// Get all the rectangles from our now orphaned legend
myLegend0.shapes.selectAll("rect").on("click", function (e) {
	var hide = false;
	var newFilters = [];
	// If the filters contain the clicked shape hide it
	filterValues0.forEach(function (f) {	
	  if (f === e.aggField.slice(-1)[0]) {
		hide = true;

	  } else {
		newFilters.push(f);

	  }
	});
	// Hide the shape or show it
	if (hide) {
	  d3.select(this).style("opacity", 0.2);
	} else {
	  newFilters.push(e.aggField.slice(-1)[0]);
	  d3.select(this).style("opacity", 0.8);
	}	
	// Update the filters
	filterValues0 = newFilters;
	// Filter the data
	chart0.data = dimple.filterData(data0, "priority", newFilters);
	// Passing a duration parameter makes the chart animate. Without
	// it there is no transition
	chart0.draw(800);
});

//changes by status by owner by year 2014
var svg11 = dimple.newSvg("#chartContainer11", "100%", 400);
var data11 = page.get('taskByOwnerByStatus');
var chart11 = new dimple.chart(svg11, data11);
chart11.setBounds(40, 25, "90%", 300);

var x11 = chart11.addCategoryAxis("x", ["name","status"]);
//x11.addOrderRule("year");
chart11.addMeasureAxis("y", "Tickets Closed");
chart11.addSeries("status", dimple.plot.bar);

var myLegend11=chart11.addLegend(40, 0, "100%", "100%", "left");
chart11.draw();

// This is a critical step.  By doing this we orphan the legend. This
// means it will not respond to graph updates.  Without this the legend
// will redraw when the chart refreshes removing the unchecked item and
// also dropping the events we define below.
chart11.legends = [];
// Get a unique list of Owner values to use when filtering
var filterValues11 = dimple.getUniqueValues(data11, "status");
// Get all the rectangles from our now orphaned legend
myLegend11.shapes.selectAll("rect").on("click", function (e) {
	var hide = false;
	var newFilters = [];
	// If the filters contain the clicked shape hide it
	filterValues11.forEach(function (f) {	
	  if (f === e.aggField.slice(-1)[0]) {
		hide = true;

	  } else {
		newFilters.push(f);

	  }
	});
	// Hide the shape or show it
	if (hide) {
	  d3.select(this).style("opacity", 0.2);
	} else {
	  newFilters.push(e.aggField.slice(-1)[0]);
	  d3.select(this).style("opacity", 0.8);
	}	
	// Update the filters
	filterValues11 = newFilters;
	// Filter the data
	chart11.data = dimple.filterData(data11, "status", newFilters);
	// Passing a duration parameter makes the chart animate. Without
	// it there is no transition
	chart11.draw(800);
});

 
//changes by status by owner by year 2013
var svg12 = dimple.newSvg("#chartContainer12", "100%", 400);
var data12 = page.get('taskByOwnerByStatus2013');
var chart12 = new dimple.chart(svg12, data12);
chart12.setBounds(40, 25, "90%", 300);

var x12 = chart12.addCategoryAxis("x", ["name","status"]);
//x12.addOrderRule("year");
chart12.addMeasureAxis("y", "Tickets Closed");
chart12.addSeries("status", dimple.plot.bar);
var myLegend12 =chart12.addLegend(40, 0, "100%", "100%", "left");
chart12.draw();
// This is a critical step.  By doing this we orphan the legend. This
// means it will not respond to graph updates.  Without this the legend
// will redraw when the chart refreshes removing the unchecked item and
// also dropping the events we define below.
chart12.legends = [];
// Get a unique list of Owner values to use when filtering
var filterValues12 = dimple.getUniqueValues(data12, "status");
// Get all the rectangles from our now orphaned legend
myLegend12.shapes.selectAll("rect").on("click", function (e) {
	var hide = false;
	var newFilters = [];
	// If the filters contain the clicked shape hide it
	filterValues12.forEach(function (f) {	
	  if (f === e.aggField.slice(-1)[0]) {
		hide = true;

	  } else {
		newFilters.push(f);

	  }
	});
	// Hide the shape or show it
	if (hide) {
	  d3.select(this).style("opacity", 0.2);
	} else {
	  newFilters.push(e.aggField.slice(-1)[0]);
	  d3.select(this).style("opacity", 0.8);
	}	
	// Update the filters
	filterValues12 = newFilters;
	// Filter the data
	chart12.data = dimple.filterData(data12, "status", newFilters);
	// Passing a duration parameter makes the chart animate. Without
	// it there is no transition
	chart12.draw(800);
});
 // Add a method to draw the chart on resize of the window
$(window).resize(function () {
    // As of 1.1.0 the second parameter here allows you to draw
    // without reprocessing data.  This saves a lot on performance
    // when you know the data won't have changed.
    chart0.draw(0, true);
	chart1.draw(0, true);
	chart2.draw(0, true);
	chart3.draw(0, true);
	chart4.draw(0, true);
	chart11.draw(0, true);
	chart12.draw(0, true);
});
 


$(document).ready(function() {

});

