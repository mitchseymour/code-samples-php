
$(document).ready(function() {
/************************Operational Measurements tab*************************************/		
		$('#stabilityTableSEV-1').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-2').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-3').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-4').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableOther').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-11').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-21').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-31').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-41').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableOther1').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-111').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-211').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-311').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-411').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableOther11').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTable').dataTable({
		"iDisplayLength": 10
		});
		$('#caseTable').dataTable({
		"iDisplayLength": 10
		});
		$('#changeTable').dataTable({
		"iDisplayLength": 10
		});
		$('#nightlyChange').dataTable({
		"iDisplayLength": 50
		});
		$('#nightlyChangeTableModalSEV-1').dataTable({
		"iDisplayLength": 10
		});
		$('#nightlyChangeTableModalSEV-2').dataTable({
		"iDisplayLength": 10
		});
		$('#nightlyChangeTableModalSEV-3').dataTable({
		"iDisplayLength": 10
		});
		$('#nightlyChangeTableModalSEV-4').dataTable({
		"iDisplayLength": 10
		});
		$('#nightlyChangeTableModalOther').dataTable({
		"iDisplayLength": 10
		});
		$('#taskModalTableInProgress').dataTable({
		"iDisplayLength": 10
		});
		$('#taskModalTablePendingAction').dataTable({
		"iDisplayLength": 10
		});
		$('#taskModalTableRejected').dataTable({
		"iDisplayLength": 10
		});
		$('#taskTable').dataTable({
		"iDisplayLength": 10
		});
});

