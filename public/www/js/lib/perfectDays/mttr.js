
$(document).ready(function() {
/************************Operational Measurements tab*************************************/		
		$('#mttrModalTableMCOM-SITESiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-SITESiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-SITEUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-SASSiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-SASSiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-SASUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-MEWSiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-MEWSiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-MEWUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-MOBILESiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-MOBILESiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableMCOM-MOBILEUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});

		
		$('#mttrModalTableBCOM-SITESiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-SITESiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-SITEUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-SASSiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-SASSiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-SASUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-MEWSiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-MEWSiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-MEWUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-MOBILESiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-MOBILESiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTableBCOM-MOBILEUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		
		$('#mttrModalTable2013MCOM-SITESiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-SITESiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-SITEUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-SASSiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-SASSiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-SASUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-MEWSiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-MEWSiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-MEWUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-MOBILESiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-MOBILESiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013MCOM-MOBILEUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});

		
		$('#mttrModalTable2013BCOM-SITESiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-SITESiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-SITEUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-SASSiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-SASSiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-SASUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-MEWSiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-MEWSiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-MEWUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-MOBILESiteDegraded-Minor2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-MOBILESiteDegraded-Major2').dataTable({
		"iDisplayLength": 10
		});
		$('#mttrModalTable2013BCOM-MOBILEUnavailable-Unplanned2').dataTable({
		"iDisplayLength": 10
		});
		
		$('#stabilityTableSEV-2').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-3').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-4').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableOther').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-11').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-21').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-31').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-41').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableOther1').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-111').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-211').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-311').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableSEV-411').dataTable({
		"iDisplayLength": 10
		});
		$('#stabilityTableOther11').dataTable({
		"iDisplayLength": 10
		});
		
});

