/**
 *  ajaxDataTable, Dependencies: jQuery, dataTables
 *
 *  This plugin simplifies the process of rendering
 *  datatables from ajax data. The important part is
 *  that these datatables can be reinitialised, which
 *  is not supported by default in the datatables plugin.
 *
 *  Sample Usage:
 *  -----------
 *  $('#myTable').ajaxTable('getData.php');
 *
 *  Note: the data coming from getData.php should be 
 *        json encoded nest arrays
 *		  <?php 
 *		      $rows = array(); 
 *			  $rows[] = array('one', 'two'); 
 *			  $rows[] = array('three', 'four'); 
 *			  echo json_encode($rows); 
 *		   ?>
 *
 *  @author Mitch Seymour, mitchseymour@gmail.com, October 24, 2013
 *  @version 0.1
 */
(function($){	

	$.fn.ajaxTable = function(url, ajaxOpts, dataTableOpts){

		if (!window.console) console = {log: function() {}};
		var fadeSpeed = 600;
		
		/*! initialize empty objects if no options were specified */
		if (!ajaxOpts) ajaxOpts = {};
		if (!dataTableOpts) dataTableOpts = {};	

	   /*!
		*  These are the default datatable options, that will be
	 	*  merged with any custom options that the client code
	 	*  specifies in the fourth argument to this function.
	 	*/

		var dataTableDefaults = {
								"aaSorting"      : [], // this is important. it turns off the default sorting when the table is initialized!
								"bPaginate"      : false,
								"bFilter"        : true,
								"bInfo"          : false,
								"oLanguage"      : {
													"sSearch": "Filter: &nbsp; &nbsp; "
												   }
							}

		/* merge the custom options and the defaults */
		var dataTableOptions = $.extend({}, dataTableDefaults, dataTableOpts);	

		return this.each(function(){

			var ajt = $(this);
			var makeFirstRowHeader = false;

			if (ajt.children("thead").length <= 0){
				makeFirstRowHeader = true;
			}

			/*!
         	 *  Some default ajax options are provided, but users can specify
         	 *  there own ajax options in the third parameter of this function.
         	 *  Below, we set the default options and then merge the custom
         	 *  options that the user providers (optional)
         	 */

        	var ajaxDefaults = {url: url, type: 'GET', dataType: 'json'};
        	ajaxDefaults.success = function(data){

        		ajt.find('tbody:first').html('');

        		var rows  = '';
				var hRows = '';
        		var count = 0;

        		if (!data) {

            		var tdCount = ajt.find('tbody:first').children('td').length;
            		rows += '<tr><td colspan="' + tdCount + '">:(</td></tr>';

            		ajt.find('tbody:first').html(rows);
            		ajt.fadeIn(fadeSpeed);
            		return false;

        		}

        		$.each(data, function(index, row){

            		count++;
					if (count==1 && makeFirstRowHeader){
						
						hRows += '<tr>';

                    	$.each(row, function(key, val){
                        	hRows += '<th>' + val + '</th>';
                    	});

                    	hRows += '</tr>';
							
					} else {

            			rows += '<tr>';

            			$.each(row, function(key, val){
                			rows += '<td>' + val + '</td>';
            			});

            			rows += '</tr>';

					}

            	});

				if (makeFirstRowHeader){
					ajt.html('');
					ajt.append($("<thead>"));
					ajt.append($("<tbody>"));
					ajt.find('thead').html(hRows);
				}
            	ajt.find('tbody:first').html(rows);
            	var oTable = ajt.dataTable(dataTableOptions);
            	ajt.fadeIn(fadeSpeed);
			} // end success

        	ajaxDefaults.error = function(xhr){ console.log(xhr.responseText); }

        	var ajaxOptions = $.extend({}, ajaxDefaults, ajaxOpts);	

			/*!
			 * If this is the first time the table is initialized,
			 * assign a unique id so we can store the table's skeleton
			 * for reinitialization
			 */

			if (!$(this).attr('data-dtInitID')){
				console.log('assigning skeleton id');
				var initID = new Date().getTime();
				$(this).attr('data-dtInitID', initID)
			} else {
				console.log('retrieving skeleton id');
				var initID = $(this).attr('data-dtInitID');
			}

			/*!
			 * Check to see if the datatables skeleton object
			 * has been initialised
			 */
			if (typeof window.dtSkel === 'undefined')
				window.dtSkel = {};

			if (!window.dtSkel[initID]){

				// Not initialised? Clone the element and save it to the skeletons array
				console.log("storing skeleton");
				var skel = $(this).wrap("<div id='" + initID + "'/>").clone(); // create the element's skeleton so we can reinitialise the datatable
				window.dtSkel[initID] = skel;
			
			} else {

				// Initialised? Retrieve the skeleton
				console.log("retrieving skeleton");
				var skel = window.dtSkel[initID];
			}

			/* Assign a class selector that we can use for the datatables plugin */
			var dtClass = 'dt-' + new Date().getTime();
			skel.addClass(dtClass);

			/* Get the outer html of the skeleton by appending a temporary element */
			var html = $('<div>').append(skel).html();

			/* reset the skeleton's html */
			$('#' + initID).html(html);

			/*!
			 * This part is extremely important. Resetting the html above detaches
			 * the table (stored in the ajt variable) from the DOM. You MUST reassign
			 * the table to the ajt variable in order for the dataTables plugin
			 * to work.
			 */
			ajt = $('#' + initID).find('table:first'); 

			/* Hide the table until the rows have been added */
			var el = $('.' + dtClass);
			el.hide();

			/* Finally, we call the url and build the datatable */
			$.ajax(ajaxOptions);
	
		});
		
	}
		
}(jQuery));
