/**
 * Fixed queue
 *
 * @link http://www.bennadel.com/blog/2308-creating-a-fixed-length-queue-in-javascript-using-arrays.htm
 * @example  
 * var friends = FixedQueue( 3, [ "Sarah", "Kit" ] );
 * friends.unshift( "Tricia", "Anna" ); // Add 2 items to the head
 * friends.push( "Kim", "Joanna" );     // Add 2 items to the tail
 *
 */
 function FixedQueue(e,t){t=t||[];var n=Array.apply(null,t);n.fixedSize=e;n.push=FixedQueue.push;n.splice=FixedQueue.splice;n.unshift=FixedQueue.unshift;FixedQueue.trimTail.call(n);return n}FixedQueue.trimHead=function(){if(this.length<=this.fixedSize){return}Array.prototype.splice.call(this,0,this.length-this.fixedSize)};FixedQueue.trimTail=function(){if(this.length<=this.fixedSize){return}Array.prototype.splice.call(this,this.fixedSize,this.length-this.fixedSize)};FixedQueue.wrapMethod=function(e,t){var n=function(){var n=Array.prototype[e];var r=n.apply(this,arguments);t.call(this);return r};return n};FixedQueue.push=FixedQueue.wrapMethod("push",FixedQueue.trimHead);FixedQueue.splice=FixedQueue.wrapMethod("splice",FixedQueue.trimTail);FixedQueue.unshift=FixedQueue.wrapMethod("unshift",FixedQueue.trimTail)

/**
 * Colorbox variables and methods
*/
$.m_cboxHistory 	   = FixedQueue(10, []); // This will hold our request history
$.m_cboxBackSelector   = '.colorbox-back, .back-colorbox';
$.m_cboxLinkSelector   = '.popup-link, .colorbox-link';
$.m_cboxReloadSelector = '.colorbox-reload, .reload-colorbox';

$.m_showColorboxContent = function(msg, width){

	
	var width = $('body').width() * .7;
	
	if (width > 850)
		width = 850;
		
	var html = '<div style="width:' + width + 'px">' + msg + '</div>';

	$.colorbox({	
		html: html, 
		maxHeight: "70%",
		onComplete: function(){
		
			$.colorbox.resize();
			$.m_canRefresh = function(){
				return false;
			}
			
		},
		onClosed: function(){
			$.m_canRefresh = function(){
				return true;
			}
		}
	});
}

$.m_initColorbox = function(url, data){
	
	if (typeof data !== 'object')
		var data = {};
	
	/** Add this request to the request history so we can refresh, go back, etc **/
	$.m_cboxHistory.push({url: url, data: data});
	
	/** Show a loading image while the content is being loaded **/
	var html = $('#loading-content').html();
	
	/** Get the snapshot if one was specified **/
	var snapshot = getUrlParameter('snapshot');
	if (snapshot)
		data.snapshot = snapshot;
	
	$.colorbox({
		html: html,
		closeButton: false,
		onComplete: function(){
			$.colorbox.resize();
			$('#cboxLoadingGraphic').hide();
		}
	});
	
	$.ajax({
		url: url,
		type: 'GET',
		data: data,
		timeout: 8000,
		success: function(response){
			$.m_showColorboxContent(response);
		},
		error: function(xhr, status){
		
			if (status == 'timeout'){
			
				var message = "<p>Request timed out. Please try again in a few seconds. Click <span style='text-decoration:underline' class='colorbox-reload'>here</span> to try again</p>";
			
			} else {
			
				console.log(xhr);
				var message = "<p>" + url + " could not be loaded. <b>XHR object</b> logged to console</p>";
			}
			
			$.growl.error({
				title: "Error", 
				message: message,
				duration: 8000
			});
			$.colorbox.close();
		}
	});
}

$(function(){
	
	$('#cboxLoadingGraphic').css('display', 'none !important');
	
	// Register the 'go back' listener and event handlers
	$($.m_cboxBackSelector).live('click', function(e){
		
		e.preventDefault();
		
		if ($.m_cboxHistory.length) {
			
			// Remove the current request from the history array
			$.m_cboxHistory.pop();
			
			// Get the previous request
			var previousRequest = $.m_cboxHistory.pop();
			$.m_initColorbox(previousRequest.url, previousRequest.data);
		
		}
		
	});
	
	// Register the 'load colorbox' listener and event handlers
	$($.m_cboxLinkSelector).live('click', function(e){
		
		e.preventDefault();
		
		var url = $(this).attr('href');
		if (url)
			$.m_initColorbox(url);
	});
	
	// Register the 'reload colorbox' listener and event handlers
	$($.m_cboxReloadSelector).live('click', function(){
	
		if ($.m_cboxHistory.length) {
			
			var previousRequest = $.m_cboxHistory.pop();
			$.m_initColorbox(previousRequest.url, previousRequest.data);
		
		}
	});
});
