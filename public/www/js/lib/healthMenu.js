function hideConnectionViews(){

	$('.hideConnect').fadeIn(600);
	$('.showConnect').hide();
}


function showConnectionViews(){

	$('.hideConnect').hide();
	$('.showConnect').fadeIn(600);
}

function hideChartViews(){

	$('.hideCharts').fadeIn(600);
	$('.showCharts').hide();
}

function showChartViews(){

	$('.hideCharts').hide();
	$('.showCharts').fadeIn(600);
}


function resetPlumbListeners(){
	
	$('#connect').unbind('toggle');
	
	$('#connect').toggle(function(){
	
		clearConnections();
		showConnectionViews();
		
		$(this).hide().fadeIn(800);
		
		var plumb = getPlumbInstance();
		var w_count = 0;
		var c_count = 0;
		
		$('.warning_source').each(function(){
			
			plumb.connect({
							source: $('.warning_target').eq(w_count), 
							target: $(this), 
							anchors:["Left", "Right"],
							paintStyle:{ 
								lineWidth:1,
								strokeStyle:"#EFBF00"
							}
							});
			w_count++;
		
		});
		
		
		$('.critical_source').each(function(){
			
			plumb.connect({
							source: $('.critical_target').eq(c_count), 
							target: $(this), 
							anchors:["Left", "Right"],
							paintStyle:{ 
								lineWidth:1,
								strokeStyle:"#E31A2C",
								dashstyle:"2 2"
							}
			}); // end connect
				
			c_count++;
			
		});
		
	}, function(){
	
		clearConnections();
		hideConnectionViews();
	
	});

}

function updateDials(){
	
	console.log('Updating dials!');
	
	$('.healthDial').each(function () {

		var dial = $(this);
		var newVal = 100 - parseInt($(this).attr('data-weight'));

		$(this).knob({"readOnly": true});

		if (newVal < 30)
			color = '#E31A2C';
		else if (newVal < 70)
			color = '#EFBF00';
		else
			color = '#64C607';

	   $({
		   value: 0
	   }).animate({

		   value: newVal
	   }, {
		   duration: 1000,
		   easing: 'swing',
		   step: function () {
		
				dial.val(this.value).trigger('change').trigger('configure', {'fgColor': color});
				dial.css('color', color);
		   }
	   })
	   
	   if (newVal <= 0){
	  
			dial.trigger('configure', {'bgColor': color});
			/********************************
			// health is at 0, start blinking
			dial.parent().fadeOut(500).fadeIn(1600);
			
			setInterval(function(){
				dial.parent().fadeOut(300).fadeIn(1600);
			}, 2800);
			********************************/
	   
	   } else if (newVal == 100) {
	   
			$('#connect').hide();
			
	   }

   });
   
}

$(function(){

	$('#healthMenuContainer').waitUntilExists(function(){
		
		updateDials();
		resetPlumbListeners();
		
		$('#charts').toggle(function(){
			
			$('.fa:not(#charts)').hide();
			$('.hideCharts').hide();
			$('#chartContainer').fadeIn(800);
			$.getJSON('/support/charts/line', function(json){
				
				var myChart = new FusionCharts("RealTimeLine", "", "100%", "100%", "0", "1");
				myChart.setJSONData(json); 
				myChart.render('chartContainer');
			
			});
		
		}, function(){
			
			$('.fa').fadeIn();
			$('#chartContainer').hide();
			$('.hideCharts').show();
		
		});
	
	});

});