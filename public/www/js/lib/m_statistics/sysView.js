$(function(){
	
	var selector = '.sysViewHealthBox';
	
	$.m_addSelector(selector);
	
	$(selector).on('updateHealth', function(){
	
		var health = $(this).attr('data-updatedHealth');
		var color  = $(this).attr('data-updatedColor');
		$(this).text(health).css('background', color);
		
	});
	
	$(selector).on('updateFailed', function(){
		
		$(this).text('?').css('background', '#e2e2e2');
	
	});
	
	$('.sysViewContainer a').on('click', function(e){
	
		e.preventDefault();
		
		var displayList  = $(this).hasClass('toggleOn');
		var list	     = $(this).parent().parent().find('ul');
		var toggleOn     = $(this).parent().find('.toggleOn');
		var toggleOff    = $(this).parent().find('.toggleOff');
		
		if (displayList){
			
			toggleOn.hide();
			toggleOff.fadeIn();
			list.fadeIn();
		
		} else {

			toggleOff.hide();
			toggleOn.fadeIn();
			list.fadeOut();
		
		}
	});

});