function initHealthBoxes(el){
	
	el.each(function(){
		
		if ($(this).hasClass('checkbox')){
			
			var checkedHtml = '<i class="fa fa-check check"></i>';
			
			$(this).off('click').on('click', function(){
				
				if ($(this).attr('checked') == 'checked') {
					
					$(this).attr('checked', false).html('').trigger('change');
					
				} else {
					
					$('.healthBox.checkbox').html('').attr('checked', false);
					$(this).attr('checked', true).html(checkedHtml).trigger('change');
				
				}
				
				
			});
		}
	
	});

}