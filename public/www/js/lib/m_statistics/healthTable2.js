function buildHealthTable2(el, sparklineSelector){
	
	var executed = false;
	var sparklines = $(sparklineSelector);

	sparklines.waitUntilExists(function(){
		
		if (executed == true)
			return true;
		
		executed = true;
		
		var timer = setInterval(function(){
			
			if (sparklines.first().is(':visible')){
				clearInterval(timer);
				
			sparklines.each(function(){
				
				var len     = $(this).text().split(',').length;
				var upper   = parseFloat($(this).attr('data-upperBound'));
				var lower   = parseFloat($(this).attr('data-lowerBound'));
				var history = $.trim($(this).attr('data-historyStr')).split(',');
				var times   = $(this).attr('data-time');

				// Convert the times to an object
				times = times.split(',');
						timesObj = {};
						
				$.each(times, function(i, k){
				
					timesObj[i] = k;
				});
				
				// colors
				var boundColor   = $(this).attr('data-boundColor');
				var historyColor = $(this).attr('data-historyColor');
				
				
				if (!len)
					return;
							
				var upperArr = new Array();
				var lowerArr = new Array();
				
				for (var i = 0; i < len; i++){
					
					upperArr.push(upper);
					lowerArr.push(lower);
					
				}
				
				var rangeMap = {};
				rangeMap[':' + lower] = '<i style="color:white;" class="fa fa-times"></i> &nbsp; Not in Range: ';
				rangeMap[upper + ':'] = '<i style="color:white;" class="fa fa-times"></i> &nbsp; Not in Range: ';
				rangeMap[lower + ':' + upper] = '<i style="color:' + historyColor + ';" class="fa fa-check"></i> &nbsp; In Range: ';
			
				$(this).sparkline(upperArr, {

					chartRangeMax: upper,
					chartRangeMin: lower,
					lineColor: 'rgba(0,0,0,0)',
					fillColor: 'rgba(0,0,0,0)',
					width: '140px',
					height: '27px',
					spotRadius: 0,
					tooltipClassname: 'jqstooltip inverse no-padding',
					tooltipFormat: "<p style='border-bottom:1px solid #555; width:140px;padding:5px; text-align:center; border-radius:4px; font-size:9px; margin-bottom:10px;'>{{x:times}}</p><p class='tooltip-bound upper'> \
										<i style='color:{{color}};' class='fa fa-arrow-up'></i> \
										Upper Limit: &nbsp; {{y}} \
									</p>",
					tooltipValueLookups: {
						times: timesObj
					}
				});
				
				$(this).sparkline(history, {
					
					chartRangeMax: upper,
					chartRangeMin: lower,
					fillColor: 'rgba(0,0,0,0)',
					lineColor: historyColor,
					lineWidth: '1.5',
					composite:true,
					spotRadius: 0,
					tooltipFormat: "<p class='tooltip-middle-val'> \
										{{y:normalLevels}} &nbsp; {{y}} \
									</p>",
					tooltipValueLookups: {
						normalLevels: $.range_map(rangeMap)
					}
				
				});
				
				$(this).sparkline(lowerArr, {
					chartRangeMax: upper,
					chartRangeMin: lower,
					lineColor: 'rgba(0,0,0,0)',
					fillColor: 'rgba(0,0,0,0)',
					composite:true,
					spotRadius: 0,
					tooltipFormat: "<p class='tooltip-bound lower'> \
										<i style='color:{{color}};' class='fa fa-arrow-down'></i> \
										Lower Limit: &nbsp; {{y}} \
									</p>"
				});
				
			});
			
			}
			
		}, 5);

	
	}, 1);
}
