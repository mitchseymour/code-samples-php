function makeMorrisChart(selector, data, userOptions, callback){
	
	var options = {
		element: selector,
		data: data,
		xkey: userOptions.xkey,
		ykeys: userOptions.ykeys,
		labels: userOptions.labels,
		pointSize: userOptions.points ? '2px' : '0px'
	};
	
	switch (userOptions.type) {
	
		case 'line' : if (typeof userOptions.colors !== 'undefined')
						options.lineColors = userOptions.colors; 
						
					  Morris.Line(options); break;
					  
		case 'area' : if (typeof userOptions.colors !== 'undefined')
						options.lineColors = userOptions.colors;
						
					  Morris.Area(options); break;
					  
		case 'stackedbar' : if (typeof userOptions.colors !== 'undefined')
							options.barColors = userOptions.colors;
						
						options.stacked = true;
						Morris.Bar(options); break;
						
		case 'scatter' : if (typeof userOptions.colors !== 'undefined')
							options.lineColors = userOptions.colors;
						
						options.lineWidth = '0px';
						options.pointSize = '5px';
						Morris.Line(options); break;
					  
		case 'bar'     : if (typeof userOptions.colors !== 'undefined')
							options.barColors = userOptions.colors;
						
						Morris.Bar(options); break;
					  
		case 'donut'     : if (typeof userOptions.colors !== 'undefined')
							options.colors = userOptions.colors;
						
						Morris.Donut(options); break;
					  
		default     : if (typeof userOptions.colors !== 'undefined')
						options.lineColors = userOptions.colors; 
						
					  Morris.Line(options); break;
	}


}