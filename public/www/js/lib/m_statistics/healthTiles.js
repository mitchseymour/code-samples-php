function flipAllTiles(triggerElement){
	
	var flipped = triggerElement.hasClass('flipped');
		 
	if (!flipped) {
	
		/** 
		 * This will be true the first time
		 * the trigger element is clicked, 
		 * and every 2 clicks afterwards.
		 */
	
		// Hide the front, show the back
		$('.flipper .front').hide();
		$('.flipper .back').show();
		
		// change the icon to point in the opposite direction
		triggerElement.hide().removeClass('fa-rotate-right').addClass('flipped fa-rotate-left').fadeIn();
		
		// Rotate the container so the back is visible
		$('.flipper').addClass('rotated').css('transform', 'rotateY(180deg)');
		
	} else {

		// Hide the back, show the front
		$('.flipper .back').hide();
		$('.flipper .front').show();
		
		// change the icon to point in the opposite direction
		triggerElement.hide().removeClass('fa-rotate-left').removeClass('flipped').addClass('fa-rotate-right').fadeIn();
		
		// Rotate the container so the front is visible
		$('.flipper').removeClass('rotated').css('transform', 'rotateY(0deg)');
	
	}

}

function addMetricAlert(el){

	var el = $(el);
	var container = $(el).parent();
	
	var refID  = el.attr('data-refID');
	var metric = el.attr('data-metric');
	var params = el.attr('data-params');
	var data   = {refID: refID, metric: metric, params: params};
	
	$.m_initColorbox('/formLoader/subscribe-to-metric', data);
	
	
}

function addThreshold(el){
	
	var el = $(el);
	var container = $(el).parent();
	
	var refID  = el.attr('data-refID');
	var metric = el.attr('data-metric');
	var params = el.attr('data-params');
	var data   = {refID: refID, metric: metric, params: params};
	
	$.m_initColorbox('/formLoader/threshold-form', data);

}

function playWhatIf(el){

	var el = $(el);
	var container = $(el).parent();
	
	var refID  = el.attr('data-refID');
	var metric = el.attr('data-metric');
	var params = el.attr('data-params');
	var data   = {refID: refID, metric: metric, params: params};
	
	$.m_initColorbox('/formLoader/what-if-form', data);
}

function editMultipliers(el){
	
	var el = $(el);
	var container = $(el).parent();
	
	var refID  = el.attr('data-refID');
	var metric = el.attr('data-metric');
	var params = el.attr('data-params');
	var data   = {refID: refID, metric: metric, params: params};
	
	$.m_initColorbox('/formLoader/multipliers-form', data);

}

$(function(){
	
	var executed = false;
	
	$('.trendLine').waitUntilExists(function(){
		
		if (executed == true)
			return true;
			
		executed = true;

		var admin = getUrlParameter('admin');
		
		if (admin) {
		
			$('.healthTiles').sortable({
			
				stop: function(){
					
					var arr = $(this).sortable('toArray');
					
					$.ajax({
					
						url: '/formProcessor/saveMetricOrder',
						data: {metricOrder: arr},
						type: 'POST',
						error: function(xhr){
						
							console.log(xhr.responseText);
						}
					
					});
				}
			});
		
		}
		//$('.healthBoxContainer').draggable();
		$('.trendLine').each(function(){
		
			var history = $(this).attr('data-history');
			var times   = $(this).attr('data-time');
			var lower   = $(this).attr('data-normalLow');
			var upper   = $(this).attr('data-normalHigh');
			var tooltip = $(this).parents('.healthBoxContainer')
								 .find('.sparklineTooltipContainer')
								 .eq(0)
								 .wrap('<div>')
								 .html();
			
			// Remove the commas so the rangeMap works correctly
			lower = parseFloat(lower.replace(/,/g, ''));
			upper = parseFloat(upper.replace(/,/g, ''));
			
			if (history) {
				
				times = times.split(',');
				timesObj = {};
				
				$.each(times, function(i, k){
				
					timesObj[i] = k;
				});
				
				var rangeMap = {};
				rangeMap[':' + lower] = $(this).parents('.healthBoxContainer').find('.ifLower').html() || 'Lower :(';
				rangeMap[upper + ':'] = $(this).parents('.healthBoxContainer').find('.ifHigher').html() || 'Higher :(';
				rangeMap[lower + ':' + upper] = $(this).parents('.healthBoxContainer').find('.ifInBounds').html() || 'In bounds';

				$(this).sparkline(history.split(','), {
					/**
					 * This is important since, by default, sparklines appends the 
					 * tooltip to the body, making it not visible from our colorbox
					 * container.
					 */
					//tooltipContainer: $(this).parents('.healthBoxContainer').eq(0),
					tooltipFormat: tooltip,
					tooltipClassname: 'jqstooltip inverse',
					tooltipValueLookups: {
						times: timesObj,
						normalLevels: $.range_map(rangeMap)
					},
					height:'15px',
					width:'40px'
				});
			}
		});
	
	}, true);
	
	// When a Health Tile is clicked, rotate the container and show the content on the back
	$('.flipContainer').die('click').live('click', function(){
	
		
		var back    = $(this).find('.back');
		var front   = $(this).find('.front');
		var flipper = $(this).find('.flipper');
		var rotated = flipper.hasClass('rotated');
		
		if (rotated) {
			
			back.hide();
			front.show();
			
			flipper.removeClass('rotated').css('transform', 'rotateY(0deg)');
		
		} else {
			
			back.show();
			front.hide();
			flipper.addClass('rotated').css('transform', 'rotateY(180deg)');
		}
		
	});
	
	// When the flip-panes button is clicked, flip all of the tiles at once
	$('#flip-panes').die('click').live('click', function(){
			
			flipAllTiles($(this));
	});
	
	$('.healthBox').tooltip({
		content: function(){
			return $(this).prop('title');
		},
		position: {my: 'left center', at: 'right+11 center'}
	});

});