function makeD3Chart(selector, data, userOptions, callback){

	if (typeof userOptions.colors !== 'undefined')
		colors = userOptions.colors;
	else
		colors = d3.scale.category10().range();
		
	switch (userOptions.type) {
  
  		case 'zoomableLine' :
  	
  			nv.addGraph(function() {
    			var chart = nv.models.lineWithFocusChart()
                  	.x(function(d) { return d[0] })
                  	.y(function(d) { return d[1] })
                  	.color(colors);

     			chart.xAxis
        			.tickValues([1078030800000,1122782400000,1167541200000,1251691200000])
        			.tickFormat(function(d) {
            			return d3.time.format('%x, %X')(new Date(d))
          			});
          			

    			chart.yAxis
        			.tickFormat(d3.format('.02f'));
        			
        	    chart.x2Axis
          			.tickValues([1078030800000,1122782400000,1167541200000,1251691200000])
        			.tickFormat(function(d) {
            			return d3.time.format('%x, %X')(new Date(d))
          		});

    			d3.select(selector)
        			.datum(data)
        			.call(chart);

    			nv.utils.windowResize(chart.update);

  			});
		
			break;  
			
		case 'area' :
  	
  			nv.addGraph(function() {
    			var chart = nv.models.stackedAreaChart()
                  	.x(function(d) { return d[0] })
                  	.y(function(d) { return d[1] })
                  	.color(colors)
                  	.transitionDuration(500)
                  	.showControls(true) 
                  	.useInteractiveGuideline(true)
                  	.clipEdge(true);

     			chart.xAxis
        			.tickValues([1078030800000,1122782400000,1167541200000,1251691200000])
        			.tickFormat(function(d) {
            			return d3.time.format('%x, %X')(new Date(d))
          			});

    			chart.yAxis
        			.tickFormat(d3.format('.02f'));

    			d3.select(selector)
        			.datum(data)
        			.call(chart);

    			nv.utils.windowResize(chart.update);

  			});
		
			break; 
		
		case 'scatter' :

			nv.addGraph(function() {
  				var chart = nv.models.scatterChart()
               		.showDistX(true)
                	.showDistY(true)
                	.transitionDuration(350)
                	.color(colors)
                	.size(1)
                	.sizeRange([60,60]);
                	
                chart.scatter.onlyCircles(false);

  				chart.xAxis
  					.tickValues([1078030800000,1122782400000,1167541200000,1251691200000])
  					.tickFormat(function(d) {
            			return d3.time.format('%x, %X')(new Date(d))
          			});
  					
  				chart.yAxis
  					.tickFormat(d3.format('.02f'));
  				
  				/**
  				  chart.tooltipContent(function(){
  	
  						return "E";
  				});
  				**/

  				d3.select(selector)
      				.datum(data)
    			.transition().duration(500)
      				.call(chart);

  				nv.utils.windowResize(chart.update);

			});
			
			break;
			
		case 'bar' : 
			
			nv.addGraph(function() {
    			var chart = nv.models.multiBarChart()
      				.transitionDuration(350)
      				.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      				.rotateLabels(0)      //Angle to rotate x-axis labels.
      				.showControls(true)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      				.groupSpacing(0.1)    //Distance between each group of bars.
      				.color(colors);

    			chart.xAxis
        			.tickFormat(function(d) {
            			return d3.time.format('%x, %X')(new Date(d))
          			});

    			chart.yAxis
        			.tickFormat(d3.format('.02f'));

    			d3.select(selector)
        			.datum(data)
        			.call(chart);

    			nv.utils.windowResize(chart.update);

		});
		
		break;
		
		case 'donut' :
			
			nv.addGraph(function() {
  				var chart = nv.models.pieChart()
      				.x(function(d, i) { return d["key"] })
      				.y(function(d) { return d["values"] })
      				.showLabels(true)
      				.color(colors)
      				.donut(true)
      				.donutRatio(0.35);


			d3.select(selector)
        		.datum(data)
        		.transition().duration(350)
        		.call(chart);

			});
			
			break;
			
		case 'pie' :
			
			nv.addGraph(function() {
  				var chart = nv.models.pieChart()
      				.x(function(d, i) { return d["key"] })
      				.y(function(d) { return d["values"] })
      				.showLabels(true)
      				.color(colors)
      				.donut(false);


			d3.select(selector)
        		.datum(data)
        		.transition().duration(350)
        		.call(chart);

			});
			
			break;
		
		// default = line chart
		default : 
		
		  			nv.addGraph(function() {
    			var chart = nv.models.lineChart()
                  	.x(function(d) { return d[0] })
                  	.y(function(d) { return d[1] })
                  	.useInteractiveGuideline(true)
                  	.color(colors);

     			chart.xAxis
        			.tickValues([1078030800000,1122782400000,1167541200000,1251691200000])
        			.tickFormat(function(d) {
            			return d3.time.format('%x, %X')(new Date(d))
          			});

    			chart.yAxis
        			.tickFormat(d3.format('.02f'));

    			d3.select(selector)
        			.datum(data)
        			.call(chart);

    			nv.utils.windowResize(chart.update);

  			});
		
			break; 
	
  	}
  	
  	//data = exampleData();
  	//console.log(data);
  	

}

/* Inspired by Lee Byron's test data generator. */
function stream_layers(n, m, o) {
  if (arguments.length < 3) o = 0;
  function bump(a) {
    var x = 1 / (.1 + Math.random()),
        y = 2 * Math.random() - .5,
        z = 10 / (.1 + Math.random());
    for (var i = 0; i < m; i++) {
      var w = (i / m - y) * z;
      a[i] += x * Math.exp(-w * w);
    }
  }
  return d3.range(n).map(function() {
      var a = [], i;
      for (i = 0; i < m; i++) a[i] = o + o * Math.random();
      for (i = 0; i < 5; i++) bump(a);
      return a.map(stream_index);
    });
}

/* Another layer generator using gamma distributions. */
function stream_waves(n, m) {
  return d3.range(n).map(function(i) {
    return d3.range(m).map(function(j) {
        var x = 20 * j / m - i / 3;
        return 2 * x * Math.exp(-.5 * x);
      }).map(stream_index);
    });
}

function stream_index(d, i) {
  return {x: i, y: Math.max(0, d)};
}



function exampleData() {
  return stream_layers(3,10+Math.random()*100,.1).map(function(data, i) {
    return {
      key: 'Stream #' + i,
      values: data
    };
  });
}