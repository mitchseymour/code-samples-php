
function initPies(selector){
		
	var sparklineOptions = {
		type: "bar",
		chartRangeMin: 0,
		chartRangeMax: 100
	}

	if (typeof selector == 'undefined')
		selector = '.m_easypie';
		
	
	var timer = setInterval(function(){
	
		if ($(selector).is(':visible'))
			clearInterval(timer);
		
	}, 5);
	
	$(selector).each(function(){
		
		var color         = $(this).attr('data-barColor');
		var colorMap      = $.parseJSON($(this).attr('data-colorMap'));
		var historyLimit  = $(this).attr('data-historyLimit');
		var lineWidth     = $(this).attr('data-lineWidth');
		var size	      = $(this).attr('data-size');
		
		if (!historyLimit || historyLimit < 0)
			historyLimit = -1;
		
		if (typeof $(this).data('easyPieChart') == 'undefined') {
					
			// Add new pie chart
			$(this).easyPieChart({
				barColor: color,
				lineWidth: lineWidth,
				chartRangeMin: -1,
				chartRangeMax: 100,
				scaleColor: false,
				size: size
			});
			
							
			$(this).on('update', function(event, data){
				
				var val   = data['val'];
				var color = data['color'];
				var pie   = $(this).data('easyPieChart');
				var y     = false;
				
				if (typeof data['y'] !== 'undefined')
					y = data['y'];
				
				if (!color)
					color = pie.options['barColor'];
				else
					pie.options['barColor'] = color;
				
				$(this).find('.easypie_val').eq(0).text(val).css('color', color);
				pie.update(val);
				
				var sparkline = $(this).parent().find('.easypie_history').eq(0);
				
				if (typeof sparkline !== 'undefined') {
				
					sparkline.trigger('update', {val: val, y: y});
					
				}
					
			
			});
			
		}		
		
		var chart       = $(this).parent().find('.easypie_history').eq(0);
		var isSparkline = chart.attr('data-sparkline');
		var onClick     = chart.attr('data-onClick');
		var tooltip     = chart.parents('.easypie_container')
								 .find('.sparklineTooltipContainer')
								 .eq(0)
								 .wrap('<div>')
								 .html();
		
		if (colorMap)
			sparklineOptions['colorMap'] = $.range_map(colorMap);
		
		if (tooltip)
			sparklineOptions['tooltipFormat'] = tooltip;
		
		if (!isSparkline) {
		
			
			var y = chart.attr('data-historyindex');
			var history = $.trim(chart.attr('data-historyStr')).split(',');
			
			yArr = new Array();
			
			if (y) { yArr = y.split(','); }
			
			chart.sparkline(history, sparklineOptions);
			chart.data('history', history);
			chart.data('yArr', yArr);
			chart.attr('data-sparkline', true);
			chart.bind('sparklineClick', function(ev){

				var sparkline = ev.sparklines[0],
					region    = sparkline.getCurrentRegionFields();
				
				region = region[0];
				region['y'] = $(this).data('yArr')[region.offset];
				
				executeFunctionByName(onClick, window, $(this), region);
				
			});
			
			chart.bind('update', function(event, data){
				
				var history = $(this).data('history');
				var yArr    = $(this).data('yArr');
				
				history.push(data.val);
				yArr.push(data.y);

				if (historyLimit > 0){
				
					if (history.length > historyLimit) {
					
						history = history.slice(history.length - historyLimit, historyLimit);
						yArr    = yArr.slice(yArr.length - historyLimit, historyLimit);
					}
				}
				
				// rebind the data
				$(this).data('history', history);
				$(this).data('yArr', yArr);

				if (colorMap)
					sparklineOptions['colorMap'] = $.range_map(colorMap);
					
					$(this).sparkline(history, sparklineOptions);
			
			});
		}
		
		var canvas = chart.find('canvas');

	});

}