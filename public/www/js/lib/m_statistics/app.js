/**
 * =================================
 *		      M_Statistics
 * =================================
 * @author <mitchseymour@gmail.com>
 * @uses d3
 * @uses jquery
 * @version 2.4
 * =================================
 *
 * Usage:
 * ---------------------------------
 * 	$.m_addSelector('.sysView');
 * 		
 *  // The update event is triggered when an object's health is updated
 *	$('.sysView').on('updateHealth', function(){
 *		
 *		// Updated health data is saved to data-updatedHealth attribute
 *		var health = $(this).attr('data-updatedHealth');
 *
 *		// Updated color information is saved to the data-updatedColor attribute
 *		var color  = $(this).attr('data-updatedColor');
 *
 *		// Do whatever you want with this information
 *		$(this).text(health).css('background', color);
 *
 *	});
 *
 * // The updateFailed event is triggered when an object's health could NOT be updated
 * $('.sysView').on('updateFailed', function(){
 *
 *		$(this).text('Unknown').css('background', '#e2e2e2');
 *
 * }); 
 *
 * // The updatesPaused event is triggered when the ajax calls are manually
 * // suspended via $.m_pause(); It is triggered on the document object
 * $(document).on('updatesPaused', function(){
 *
 *		$('#updateStatus').text('Paused').css('background', '#e2e2e2');
 *
 * }); 
 *
 * // The updatesResumed event is triggered when the ajax calls are manually
 * // resumed via $.m_resume(); It is triggered on the document object
 * $(document).on('updatesResumed', function(){
 *
 *		$('#updateStatus').text('Paused').css('background', '#e2e2e2');
 *
 * }); 
 *
 *
 * // version 2.3 includes support for SVG elements
 * $.m_addSVGSelector('circle', {
 * 		
 * 		stroke: function(healthData, index){
 * 			return healthData['color'];
 * 		}
 * 		
 * 	});
 *
 */

// Do not cache ajax responses
$.ajaxSetup ({
	cache: true
});

// If debug is set to true, information will be logged to the console
$.m_debug = true;

$.m_emptyHealth = "NA";

// The url that provides the updated health information
$.m_endPoint = '/supportHealth/update-ui';
$.m_formEndPoint = '/formProcessor/';
$.m_searchURL = '/smartSearch';

// How often the page should be reloaded to prevent potential memory leaks
$.m_refreshInterval = false;

$.m_loadTime = new Date().getTime() / 1000;

// A callback that determines whether or not the page can be refreshed. Clients
// can override this to prevent a page refresh while certain events are detected
$.m_canRefresh = function(){
	return true;
}

$.m_lastHealth = '';

// This is called whenever $.m_interval runs 
$.m_needsRefresh = function(){
	
	if (!$.m_refreshInterval)
		return false;
		
	if ($.m_isPaused)
		return false;
			
	var elapsed = ((new Date().getTime() / 1000) - $.m_loadTime) * 1000;

	if(elapsed > $.m_refreshInterval) {
	
		if (typeof $.m_canRefresh == 'function' && !$.m_canRefresh()){
		
			console.log('Refresh prevented by client');
			return false;
		
		}
		
		location.reload();
	}
	
}

// When this is true, we do not check for updates
$.m_isPaused = false;

// DOM elements with these selectors will receive update events
$.m_objectSelectors = [];

// Add selectors to be updated
$.m_addSelector = function(selector){
	
	if (!selector)
		return false;
	
	if ($.inArray(selector, $.m_objectSelectors) !== -1)
		return false;
		
	$.m_objectSelectors.push(selector);
	return true;
}

// SVG elements with these selectors will also receive update events
$.m_svgSelectors = {};

$.m_addSVGSelector = function(selector, styleObj){
		
	if (!selector)
		return false;
		
	if (!styleObj || typeof styleObj !== 'object')
		styleObj = {};
	
	if ($.inArray(selector, $.m_svgSelectors) !== -1)
		return false;
	
	$.m_svgSelectors[selector] = styleObj;
	return true;
}

$.m_d3ColorOverride = {};

// default health selector
$.m_addSelector('.healthObject');

// Pause updates
$.m_pause = function(){
	
	$.m_isPaused = true;
	$(document).trigger('updatesPaused');
}

// Resume updates
$.m_resume = function(){

	$.m_isPaused = false;
	$(document).trigger('updatesResumed');
}

// These can be set if system reports are desired
$.m_rootGroup  = false;
$.m_rootID     = false;
$.m_sysReports = false;

// This will be set when the health is updated. Clients should
// access this object using the $(document).on('updatedHealthTrends') 
// event
$.m_healthTrends = {};

$.m_submitFormViaAjax = function(form){
	
	if (!$.m_formEndPoint) {
		
		if ($.m_debug)
			console.log('Form end point has not been configured');
		return;
		
	}
	
	var formName = form.attr('name');
	var data     = form.serialize() + '&formName=' + formName;
	var type     = (form.attr('method')) || 'get';
	
	var submitBtns = form.find('[type=submit]');
	var whenSubmitting = form.find('.when-submitting, .whenSubmitting');
	
	submitBtns.attr('disabled', 'disabled');
	whenSubmitting.removeClass('hidden').show();
	
	$.ajax({
	
		url: $.m_formEndPoint,
		type: type,
		dataType: 'json',
		data: data,
		complete: function(){
		
			submitBtns.removeAttr('disabled');
			whenSubmitting.hide();
			
		},
		success: function(resp){
			
			if (!resp || typeof resp !== 'object') {
			
				resp = {};
				resp['status'] = 0;
				resp['message'] = 'Failed to submit. Please verify endpoint';
			}
			
			if (typeof resp['status'] !== 'undefined') {
				
				if (resp['status'] == 1) {
				
					if (resp['message']) {
						
						var message = form.find('.respMessage');
						message.hide().html('<span class="success">' + resp['message'] + '</span>').fadeIn(600);

					}
						
					form.trigger('ajaxSubmitSuccess');
					
				} else {
					
					if (resp['message'])
						form.find('.respMessage').html('<span class="error">' + resp['message'] + '</span>');
						
					form.trigger('ajaxSubmitError');
				}
				
				if (typeof resp['reloadPage'] !== 'undefined' && resp['reloadPage'] == true) {
					
					location.reload();
					
				}
				
				// See if there are any callbacks
				if (typeof resp["callback"] == "function"){
					var fn = resp["callback"];
					
					if (typeof resp["data"] == undefined)
						resp["data"] = {};
						
					executeFunctionByName(fn, window, resp["data"]);
				}
			
			}
			
		},
		error: function(xhr){
			
			if ($.m_debug)
				console.log(xhr.responseText);
				
			form.trigger('ajaxSubmitXHRError');
		}
	});


}

/**
 * This function updates the following attributes with the appropriate information
 * for the given object (as determined by the data-oid and data-ogroup) properties
 *
 * data-updatedColor, data-updatedHealth, data-updatedTime, data-updatedTrend
 */
 
$.m_updateUI = function(healthObj, debug){
	
	// Make sure healthObj is an object
	if (typeof healthObj !== 'object' || healthObj === 'undefined') {	
		
		if ($.m_debug)
			console.log('No health object was provided to $.m_updateUI');
			
		return false;
	}
	
	// Make sure at least one selector was provided so we know which DOM/SVG elements to update
	if (typeof $.m_objectSelectors == 'undefined' || 
		typeof $.m_svgSelectors == 'undefined'){
		
		if ($.m_debug)
			console.log('No selectors were specified');
		
		// Pause updates
		$.m_pause();
			
		return false;
	}
	
	// Compare the last health time to the new health time. If it's the same, don't update the UI
	var healthTime  = healthObj['lastCalculated'];
	var snapshot    = healthObj['snapshot'];
	var forceUpdate = healthObj['forceUpdate'];

	if ($.m_lastHealth && $.m_lastHealth == healthTime && !forceUpdate) {
		
		if ($.m_debug)
			console.log('Health has not been updated');
		
		return false;
	}
	
	if ($.m_debug)
		console.log('Health has been updated!');
	
	$.m_lastHealth = healthTime;
	
	var d = new Date(healthTime);
	
	/**
	 * Note
	 * -----------------------------------------------------
	 * 'return true' in the $.each method below is the same
	 * as calling 'continue' in a traditional loop.
	 *
	 */
	 
	$.each($.m_objectSelectors, function(index, selector){
	 
		$(selector).each(function(){
				
			var el = $(this);
					
			/**
			 * Make sure the update-able health object has the
			 * required attributes.
			 */
			if (!el.attr('data-ogroup') || !el.attr('data-oid')){
				
				if ($.m_debug)
					console.log(selector + ' could not be updated, because it is missing the required attributes (data-ogroup and data-oid)');
				
				el.trigger('updateFailed');
				return true;
			}
					
			/**
			 * Make sure the health information for this object's
			 * group and id was provided in the health object
			 */
			var group = el.attr('data-ogroup');
			var id    = el.attr('data-oid');
				
			if (typeof healthObj.health[group] == 'undefined' || typeof healthObj.health[group][id] == 'undefined'){
				
				el.trigger('updateFailed');
				return true;
			}
				
			var obj    = healthObj['health'][group][id];
			var health = obj['current_health'];
				
			if (health == '-1')
				health = $.m_emptyHealth;
					
			el.attr('data-updatedColor', obj['color']);
			el.attr('data-updatedHealth', health);
			el.attr('data-updatedTime', d);
			el.attr('data-updatedTimeUnformatted', healthTime);
			el.attr('data-updatedSnapshot', snapshot);
			el.attr('data-updatedTrend', obj['trending']);
			
			el.trigger('updateHealth');
			
		});
	});
	
	$.each($.m_svgSelectors, function(selector, styleObj){
		
		var el = d3.selectAll(selector);
		
		$.each(styleObj, function(styleProperty, val){
			
			el.style(styleProperty, function(d, i){
				
				if (val && typeof val == 'function') {
				
					if (d.group && d.name) {
					
						var group  = d.group;
						var id     = d.name;
						
					} else if (d.target.group && d.target.name) {
					
						var group  = d.target.group;
						var id     = d.target.name;
					
					} else {
						
						if ($.m_debug)
							console.log(selector + ' does not have the required attributes for updating');
						
						return true;
						
					}
					
					if (typeof healthObj['health'][group] !== 'undefined' && healthObj['health'][group][id]) {
						
						var health = healthObj['health'][group][id];	
						var v = val(health, i);
						
						if (selector == 'path.link' && $.m_d3ColorOverride[v])
							v = $.m_d3ColorOverride[v];
						
						return v;
					
					}
					
					return;
				}
				
				return val;
								
			});
		});
		
	});
	
	
	$.m_healthObj = healthObj;
	
	$(document).trigger('updatedHealth');
	
	if (typeof healthObj.trends === 'object') {
	
		$.m_healthTrends = healthObj.trends;
		$(document).trigger('updatedHealthTrends');	
	}
};

$.m_getHealth = function(group, id){
	
	if (typeof $.m_healthObj !== 'object')
		$.m_healthObj = {};
	
	try {
	
		var health = $.m_healthObj['health'][group][id]['current_health']
		
	} catch (e) {
		
		var health = 100;
	}
		
	return health;
	
}

// Update the UI elements for the health objects at the specified interval
$.m_updateAtInterval = function(interval, runImmediately){
	

	if (!interval) {
		return false;
	} else if (!$.m_endPoint) {
	
		if ($.m_debug)
			console.log('In order to use $.m_updateAtInterval, you must specify the location of the server-side script that retrieves the object health using $.m_endPoint = "[url]"');
		
		return false;
	} else if (typeof $.m_objectSelectors == 'undefined' || $.m_objectSelectors.length == 0) {
		
		if ($.m_debug)
			console.log('No m_objectSelectors were specified. Use the $.m_addSelector function to register a selector before calling $.m_updateAtInterval');
		
		return false;
	}
		
	$.m_interval = setInterval(function(){
	
		$.m_needsRefresh();
		$.m_doUpdate();
		
	}, interval);
	
	if (runImmediately){
		$.m_doUpdate();
	}
	
}

$.m_doUpdate = function(){

	if ($.m_isPaused == true)
			return;
		
	var inventory = {};
	
	$.each($.m_objectSelectors, function(index, selector){
		
		// Gather an inventory of all of the health objects on the page
		$(selector).each(function(){

			var group = $(this).attr('data-ogroup');
			var id    = $(this).attr('data-oid');
						
			if (!inventory[group])
				inventory[group] = new Array();
				
			inventory[group].push(id)
			
		});
		
	});
	
	$.each($.m_svgSelectors, function(selector, styleObj){
		
		d3.selectAll(selector).each(function(data){
			
			if (data.group && data.name) {
				
				var group = data.group;
				var id    = data.name;
			
			} else if (data.target.group && data.target.name) {
				
				var group = data.target.group;
				var id    = data.target.name;
				
				
			
			}
			if (!inventory[group])
				inventory[group] = new Array();
				
			inventory[group].push(id)
			
		});		
	});
	
	if ($.isEmptyObject(inventory))
		return;
	
	// If the last ajax request is still processing, don't submit another one
	if ($.m_stillProcessing) {
		
		console.log('Still processing');
		return;
	}
		
	$.m_stillProcessing = true;
	
	$.ajax({

		url: $.m_endPoint,
		type: 'POST',
		data: {inventory: inventory, lastSnapshot: $.m_lastHealth, sysReports: $.m_sysReports, rootGroup: $.m_rootGroup, rootID: $.m_rootID},
		dataType: 'json',
		success: function(response){
			
			if (!response || typeof response !== 'object' || !response.hasOwnProperty('status')){
				
				if ($.m_debug)
					console.log('No response at ' + $.m_endPoint);
					
				return false;
			}
			
			if (response['status'] == 0){
			
				if ($.m_debug)
					console.log(response['message']);
				
				return false;
			}
		
			$.m_updateUI(response);
			
		},
		error: function(xhr){
		
			console.log(xhr.responseText);
		},
		complete: function(){
		
			$.m_stillProcessing = false;
			
		}

	});

}

$.root_ = $('body');
 
/**
* Detect the whether or not the device is mobile or desktop
*/
$.device = null;


var ismobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

if (!ismobile) {
// Desktop
$.root_.addClass("desktop-detected");
$.device = "desktop";
} else {
// Mobile
$.root_.addClass("mobile-detected");
$.device = "mobile";
}

$(function(){
	
	$('.load-dots').hide();
	
	/**
	 * Initiate all tooltips
	 */
	$(document).tooltip({
		position: {my: 'left center', at: 'right+11 center'},
		tooltipClass: function(){
			
			if ($(this).attr('tooltip-class')) {
				console.log($(this));
				return $(this).attr('data-tooltipClass');
			}
			
			return 'inverse';
		},
		content: function(){
			return $(this).prop('title')
		}
	});
	
	$('.ajax-form').live('submit', function(e){
		
		e.preventDefault();
		$.m_submitFormViaAjax($(this));
	
	});
	
	/**
	 * Initiate any object autocompletes
	 */
	var autocomplete = $('.object-autocomplete').live("focus", function(){
	
		$(this).autocomplete({
			
			search: function(){
				
				$('.load-dots').show();
			},
			select: function(event, ui){
				
				$('.load-dots').hide();
				
				var callback = $(event.target).attr('data-callback') || false;
				var item     = ui.item;
				
				/*
				if (callback) {
					
					executeFunctionByName(callback, window, {group: item.group, id: item.object_id});
				
				} else if (item.type == 'ws') {
					
					$('.health-circle').each(function(){
						
						var c = $(this).clone();
						$('body').append(c);
						
					});
					
				
				} else if (item.type == 'page' || item.type == 'chart'){
					
					window.open(item.link, '_blank');
				
				}
				**/
				
				if (typeof item['link'] !== 'undefined') {
					
					if (!item['modal'])
						window.open(item.link, '_blank');
					else
						$.m_initColorbox(item.link);
				}
			},
			source: function(request, response){
				
				var previouslyPaused = false;
				
				if ($.m_isPaused)
					previouslyPaused = true;
					
				$.m_pause();
				
				var snapshot = getUrlParameter('snapshot') || '';
				var term     = request.term;
				
				$.ajax({
					url: $.m_searchURL,
					dataType: 'json',
					data: {term: term, snapshot: snapshot},
					minLength: 2,
					complete: function(){
						
						$('.load-dots').hide();
						
						if (!previouslyPaused)
							$.m_resume();
						
					},
					success: function(data){
						
						if (!data)
							return false;
							
		
						response($.map(data, function(item){
							
							if (typeof item['modal'] == 'undefined')
								item['modal'] = false;
								
							return {label: item.html, value: term, link: item.link, modal: item['modal']};
						}));
						
					}
					
				});
			}
		});
		if (autocomplete.length){
		
			autocomplete.data("ui-autocomplete")._renderItem = function(ul, item){
		
				return $("<li class='object-autocomplete-item'></li>")
					 .data("item.autocomplete", item)
					 .append("<a>" + item.label + "</a>")
					 .appendTo(ul);
			}
		}
	});
	

});

/**
 * Launch fullscreen mode
 *
 * @example $('#fullscreen').click(function(){launchFullscreen();});
 */
function launchFullscreen(){e=document.documentElement;if(!$.root_.hasClass("full-screen")){$.root_.addClass("full-screen");if(e.requestFullscreen){e.requestFullscreen()}else if(e.mozRequestFullScreen){e.mozRequestFullScreen()}else if(e.webkitRequestFullscreen){e.webkitRequestFullscreen()}else if(e.msRequestFullscreen){e.msRequestFullscreen()}}else{$.root_.removeClass("full-screen");if(document.exitFullscreen){document.exitFullscreen()}else if(document.mozCancelFullScreen){document.mozCancelFullScreen()}else if(document.webkitExitFullscreen){document.webkitExitFullscreen()}}}

/**
 * Get url parameters
 *
 * @example var lastname = getURLParameter('lastname');
 */
 function getURLParameter(name) { return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;}
 /** Alias **/
 function getUrlParameter(name) { return getURLParameter(name);}
 
/**
 * Execute function by name
 *
 * @example var lastname = getURLParameter('lastname');
 */
 function executeFunctionByName(functionName, context /*, args */) {

  // first, see if the functionName includes the arguments as well, e.g. functionName = 'alert(7)'
  var match = functionName.match(/\((.+?)\)/);
  if (match) {
  
	var args = match[1].split(',');
	var functionName = functionName.replace(match[0], '');
  
  } else {
	
	
	var args = [].slice.call(arguments).splice(2);
  
  }
  
  var namespaces = functionName.split(".");
  var func = namespaces.pop();
  for(var i = 0; i < namespaces.length; i++) {
    context = context[namespaces[i]];
  }
  
  if (typeof context[func] !== 'undefined')
	return context[func].apply(this, args);
}