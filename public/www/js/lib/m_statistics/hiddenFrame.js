function _initHiddenFrames(selector){
	
	$('a.hfPageLink' + selector).waitUntilExists(function(){

		// show the first frame by default
		var frames = $('.frame' + selector);
		frames.eq(0).fadeIn();
		_initHiddenSparklines();
		
		$('a.hfPageLink' + selector).eq(0).addClass('active');
		
		$(this).on('click', function(e){
		
			e.preventDefault();
			
			// Add the 'active' class to the clicked link
			$('a.hfPageLink' + selector).removeClass('active');
			$(this).addClass('active');
			
			var index = $(this).attr('data-frameIndex');
			
			frames.hide();
			frames.eq(index).fadeIn();
			_initHiddenSparklines();
						
		});
	
	}, true);
	
	$('.hfNextPage').die('click').live('click', function(e){
		
		
		var frames = $('.frame' + selector);
		var link = $('a.hfPageLink.active').parent('li').next('li').find('a.hfPageLink');
		
		if (!link.length)
			link = $('a.hfPageLink.active').parent('li').parent('ul').first('li').find('a.hfPageLink');
			
		var index = link.attr('data-frameIndex');

		
		$('a.hfPageLink' + selector).removeClass('active');
		link.addClass('active');
		frames.hide();
		frames.eq(index).fadeIn();
		_initHiddenSparklines();
		
						
	});
}

function _initHiddenSparklines(){

	/**
	 * Pages that use the sparkline plugin will not have their
	 * sparklines rendered unless we call this
	 */
	if (typeof $.sparkline_display_visible == 'function')
		$.sparkline_display_visible();

}