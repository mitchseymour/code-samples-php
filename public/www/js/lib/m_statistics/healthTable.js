function buildHealthTable(el){

	el.find('.inBoundSparkline').each(function(){
		
		var len     = $(this).text().split(',').length;
		var upper   = parseFloat($(this).attr('data-upperBound'));
		var lower   = parseFloat($(this).attr('data-lowerBound'));
		var history = $.trim($(this).text()).split(',');

		// colors
		var boundColor   = $(this).attr('data-boundColor');
		var historyColor = $(this).attr('data-historyColor');
		
		
		if (!len)
			return;
					
		var upperArr = new Array();
		var lowerArr = new Array();
		
		for (var i = 0; i < len; i++){
			
			upperArr.push(upper);
			lowerArr.push(lower);
			
		}
		
		var rangeMap = {};
		rangeMap[':' + lower] = '<i style="color:white;" class="fa fa-times"></i>';
		rangeMap[upper + ':'] = '<i style="color:white;" class="fa fa-times"></i>';
		rangeMap[lower + ':' + upper] = '<i style="color:' + historyColor + ';" class="fa fa-check"></i>';
		
		console.log(historyColor);
		
		$(this).sparkline(upperArr, {

			chartRangeMax: upper + 20,
			chartRangeMin: lower - 20,
			lineColor: boundColor,
			fillColor: 'rgba(0,0,0,0)',
			width: '120px',
			height: '32px',
			spotRadius: 0,
			tooltipClassname: 'jqstooltip inverse no-padding',
			tooltipFormat: "<p class='tooltip-bound upper'> \
								<i style='color:{{color}};' class='fa fa-arrow-up'></i> \
								Upper Limit: &nbsp; {{y}} \
							</p>"
		});
		
		$(this).sparkline(history, {
			
			chartRangeMax: upper + 20,
			chartRangeMin: lower - 20,
			fillColor: 'rgba(0,0,0,0)',
			lineColor: historyColor,
			composite:true,
			spotRadius: 0,
			tooltipFormat: "<p class='tooltip-middle-val'> \
								{{y:normalLevels}} &nbsp; {{y}} \
							</p>",
			tooltipValueLookups: {
				normalLevels: $.range_map(rangeMap)
			}
		
		});
		
		$(this).sparkline(lowerArr, {
			chartRangeMax: upper + 20,
			chartRangeMin: lower - 20,
			lineColor: boundColor,
			fillColor: 'rgba(0,0,0,0)',
			composite:true,
			spotRadius: 0,
			tooltipFormat: "<p class='tooltip-bound lower'> \
								<i style='color:{{color}};' class='fa fa-arrow-down'></i> \
								Lower Limit: &nbsp; {{y}} \
							</p>"
		});
		

	});
}
