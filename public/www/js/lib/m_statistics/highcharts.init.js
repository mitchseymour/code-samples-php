function makeHighChart(selector, data, userOptions, callback){
	
	var options = {
            title: {
                text: typeof userOptions.title !== 'undefined' ? userOptions.title : '',
                x: -20 //center
            },
            subtitle: {
                text: typeof userOptions.subTitle !== 'undefined' ? userOptions.subTitle : '',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: ''
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: data,
			chart: {
				type: userOptions.type == 'undefined' ? 'line' : userOptions.type
			},
			plotOptions: {
				animation: false
			}
        };
		
	if (typeof userOptions.colors !== 'undefined')
		options.colors = userOptions.colors;
		
	//var options = $.extend({}, options, userOptions);
		
	$(selector).highcharts(options);
}