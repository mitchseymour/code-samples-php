$(function(){
	
	$(".eq-slider").waitUntilExists(function(){
		
		// Initialize the multiplier EQ sliders
		$( ".eq-slider" ).each(function(){
			
			
			var val = 1;
			
			if ($(this).attr('data-value'))
				val = $(this).attr('data-value');
				
			$(this).parent().find('input').val(val);
			
			$(this).slider({
			
				animate: true,
				orientation: "vertical",
				range: "min",
				min: 0,
				max: 10,
				step: 0.1,
				value: val,
				slide: function(event, ui){
					
					var input = '.' + $(this).attr('data-inputRef');
					$(input).val(ui.value);
					
				}
			});
		});
	
	
	});



});