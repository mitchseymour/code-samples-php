function buildDependencyTable(el){
	
	var aoColumns = [];
	var columns   = el.find('thead th');
	var visible   = el.find('th.visibleHealthColumn').eq(0).index();
	var hidden    = el.find('th.hiddenHealthColumn').eq(0).index();
	
	$.each(columns, function(i, obj){
	
		if (i == visible)
			aoColumns.push({ "iDataSort": hidden}); /** Sort by the hidden health column **/
		else if (i == hidden)
			aoColumns.push({ "bVisible" : 0}); /** Hide the hidden health column **/
		else
			aoColumns.push(null);
		
	});
	
	el.dataTable({
			"bFilter" : false,
			"fnDrawCallback": function(oSettings) {
			
				// Only show the pagination info when there are enough records to require pagination
                if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                        $('.dataTables_paginate, .dataTables_info').css("display", "block");
                } else {
                        $('.dataTables_paginate, .dataTables_info').css("display", "none");
                }
				
				var sortColumn = oSettings.aaSorting[0][0];
				
				/**
				if (sortColumn == 2) {
					
					// The user sorted the health column, so we briefly show the health
					$('span.hiddenHealthInfo').fadeIn();
					setTimeout(function(){
						
						$('span.hiddenHealthInfo').fadeOut();
					
					}, 6000);
				}
				**/

            },
			"bSortClasses" : true,
			"fnInitComplete": function(){
			
				el.hide().css('visibility', 'visible').fadeIn(500);
			},
			"aoColumns": aoColumns,
			"aaSorting": [[ 3, "asc" ]], // sort the health column descending by default
		});
		
		el.find('tr:nth-child(even)').css('background', 'F4F7F7 !important');
		el.fadeIn(100);
}