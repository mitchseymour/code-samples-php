function m_select2(selector, userOptions){

	var el = $(selector);
	var defaults = {};
	
	if (typeof userOptions !== 'object')
		userOptions = {};
		
	var options = $.extend({}, defaults, userOptions);
	
	el.select2(options);
	
	console.log(options);
}