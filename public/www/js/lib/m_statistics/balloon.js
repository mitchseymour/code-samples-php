function makeBalloon(selector, jsonStr, jsonOpts) {
	
	var json = JSON.parse(jsonStr);
	var userOpts = JSON.parse(jsonOpts);
	
	var defaults = {
		height: 800,
		width: 900,
		top: 0,
		right: 0,
		bottom: 0,
		left: 0
	};
	
	var options = $.extend({}, defaults, userOpts);

	var margin = {top: options.top, right: options.right, bottom: options.bottom, left: options.left},
		width  = options.width, 
		height = options.height, 
		diameter = Math.min(width, height),
		radius = diameter / 2;

	var tree = d3.layout.balloon()
	  .size([width, height])
	  .value(function(d) { return d.size; })
	  .gap(function(d) { return d.externalLinkCount * 50; });

	var bundle = d3.layout.bundle();

	var vis = d3.select(selector).append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
	  .append("g")
		.attr("transform", "translate(" + (margin.left + radius) + "," + (margin.top + radius) + ")");

  var nodes = tree.nodes(packages.root(json)),
      links = bundle(packages.imports(nodes)); 

  var line = d3.svg.line()
      .interpolate("bundle")
      .tension(.4)
      .x(function(d) { return d.lx; })
      .y(function(d) { return d.ly; });

  var link = vis.selectAll("path.link")
      .data(links)
    .enter().append("path")
      .attr("class", "link")
      .attr("d", line)
	  .style("stroke", function(d, i){
			
			return  '#aaa';;
	  });

  var node = vis.selectAll("g.node")
      .data(nodes)
    .enter().append("g")
      .attr("class", "node");

  node.append("circle")
      .attr("r", function(d) { return d.r; })
      .attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; })
	  .style("stroke", function(d, i){
		
		// Make the outer circle border white
		if (i == 0)
			return '#fff';
		
	  })
	  .attr("class", function(d, i){
	  
		if (typeof d.nodeColor == 'undefined')
			return 'outer-circle';
		
		return 'inner-circle';
	  })
	  .transition()
	  .duration(2000)
	  .style("fill", function(d, i){
		
		if (typeof d.nodeColor !== 'undefined')
			return d.nodeColor;
	  })
	  .style("fill-opacity", function(d, i){
		
		if (typeof d.nodeColor !== 'undefined')
			return .3;
			
		return 0;
	  })
	  .attr("r", function(d, i){
		
		// to do: make circle's proportionate to their impact on the root object
		return d.r;
	  });
	  
	 node.append("text")
	  .attr('x', function(d){ return d.x})
	  .attr('y', function(d){ return d.y})
	  .style("font-size", function(d, i){
		
			if (i == 1)
				return "30px";
			
			return "10px";
	  })
	  .style("fill", function(d, i){
	  
		if (i == 1)
			return "#555";
			
		return "#aaa";
	  })
	  .text(function(d, i) { 
	  
		if (i % 5 == 0) 
			return d.name.substring(2); 
		else if ( i == 1)
			return d.name.substring(2); 
	});
	
	/**
	var outerCircles = vis.selectAll('.outer-circle');
	outerCircles.each(function(){
	
		var x = d3.select(this).attr("cx");
		var y = d3.select(this).attr("cy");
		
		
	});
	**/

}
