var _fch_options;

function renderFusion(selector, userOptions){
	
	if (typeof selector == 'undefined')
		return;
		
	if (!userOptions || typeof userOptions !== 'object')
		userOptions = {};
		
	var defaults = {type:"area2d", drawAnchors:0,height:"350",dataFormat:"json",dataSource:{chart:{manageresize:"1",bgcolor:"FFFFFF",bgalpha:"0",showborder:"0",upperlimit:"100",lowerlimit:"0",numbersuffix:"",valueabovepointer:"1",pointerontop:"1",pointerradius:"6",charttopmargin:"48",chartbottommargin:"20",chartleftmargin:"20",chartrightmargin:"20",outCnvBaseFontColor:"555",canvasBorderColor:"FFFFFF",alternateHGridColor:"FFFFFF",canvasPadding:"70",showLabels:"1",labelDisplay:"Rotate",labelSlant:"1",labelPadding:"4",slantLabels:"1",showYAxisValues:"1",interactiveLegend:"1",showValues:"0", anchorRadius:"0",yAxisMinValue:"0",yAxisMaxValue:"100","plotFillColor ":"cccccc",plotGradientColor:" "},styles:{definition:[{name:"CaptionFont",type:"font",size:"12"}, {name:"SubCaptionFont",type:"font",size:"10"}],application:[{toobject:"CAPTION",styles:"CaptionFont"},{toobject:"SUBCAPTION",styles:"SubCaptionFont"}]}}}
	var options  = $.extend({}, defaults, userOptions);
	
	$(selector).each(function(){

		var seriesType  = $(this).attr('data-fusionSeriesType');
		var series 		= $.parseJSON($(this).attr('data-fusionSeries'));
		var title       = $(this).attr('data-fusionCaption');
		var subTitle    = $(this).attr('data-fusionSubCaption');
		
		if (title)
			options.dataSource.chart.caption = title;
			
		if (subTitle)
			options.dataSource.chart.subcaption = subTitle;
			
		if (!series)
			series = new Array();
		
		if (seriesType == 'default') {
		
			options['dataSource']['data'] = series[0]['data'];
		
		} else if (seriesType == 'categorical') {
			
			var count = 0;
			var categories = [];
			var dataSet= [];
			
			$.each(series, function(index, objects){
				
				count++;
				var data = [];
				var seriesName = objects['name'];
				
				$.each(objects['data'], function(i, object){
					
					if (count == 1)
						categories.push({"label" : object['label']});
						
					data.push({"value" : object["value"]});
					
				});
				
				dataSet.push({"seriesname" : seriesName, "data": data});
			
			});
			
			options['dataSource']['categories'] = [{"category": categories}];
			options['dataSource']['dataset'] = dataSet;
			//console.log(options);
		
		}
			
		if (options.colors && options.colors !== '')
			options['dataSource']['paletteColors'] = options.colors;
			
		$(this).insertFusionCharts(options);
			
		window._fch_options = options;
	});

}