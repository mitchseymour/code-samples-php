$.m_healthDropsActive = true;

$.m_hideHealthDrops = function(){
	$('#growls .growl:not(.broadcast)').remove();
}

$.m_showHealthDrops = function(){
	
	$.m_hideHealthDrops();
	
	var count = 0;
	

	$.each($.m_healthTrends, function(group, groupStatus){
		
		if (groupStatus.down) {
			
			count+=1;
			var len = groupStatus.down.length;
			if (len == 1)
				len = len + ' item';
			else
				len = len + ' items';
			var message = "<p>" + len + " in the <b>" + group + "</b> group experienced a drop in health:<ul style='list-style:none;margin:0;padding:0;'>";
			
			$.each(groupStatus.down, function(key, o){
			
				message += '<li style="list-style:none"><a class="popup-link" style="color:#fff !important" href="support/detail&popup=true&group=' + o.group + '&id=' + o.id + '">' + o.id + '</a></li>';
		
			});
			
			$.growl.error({
		
				title: group + ' Health Drop',
				message: message,
				duration: 30000
			});
			
		}
	});
	
	/*
	if (count==0) {
	
		$.growl.notice({
			title: 'Success',
			message: '<p>No health drops were found.</p>',
			duration: 5000
			});
		return;
	}
	//*/
}

$(function(){
	
	$(document).on('updatedHealthTrends', function(){
	
	if ($.m_healthDropsActive)
		$.m_showHealthDrops();	
		
	});
});