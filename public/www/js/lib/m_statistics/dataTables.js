function buildDynamicDatatable(selector, userOptions){

	if (!userOptions || typeof userOptions !== 'object')
		userOptions = {};
	
	var el = $(selector);
	var defaults = {
		"aaSorting"		 : [[ 1, "asc" ]], // default sorting column
		"bFilter"        : true,
		"bInfo"          : true,
		"bPaginate"      : true,
		"bSortClasses"   : true,
		"iDisplayLength" : 25, // Number of records per page, when pagination is enabled
		"fnDrawCallback": function(oSettings) {
			
			/**
			 * Smart pagination
			 *
			 * Only show the pagination info when there are enough records 
			 * to require pagination
			 */
			if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)
				$('.dataTables_paginate, .dataTables_info').css("display", "block");
			else
				$('.dataTables_paginate, .dataTables_info').css("display", "none");
			
				

		},
		"fnInitComplete" : function(){
			
		},
		"renderTo": {}
	}
	
	var options = $.extend({}, defaults, userOptions);
	console.log(options);
	el.hide().dataTable(options);
	
	var elementSelectorLookup = {
		
		"bFilter": ".dataTables_filter"
	
	}
	
	$.each(options.renderTo, function(dtElement, selector){
	
		if (elementSelectorLookup[dtElement]){
		
			var dtEl = $(elementSelectorLookup[dtElement]);
			
			if (dtEl[0]) {	
			
				var html = dtEl[0].outerHTML;
			
				$(selector).append(html);
				dtEl.remove();
				
			}
		}
	});
	
	el.fadeIn(600);
}

$(function(){

});