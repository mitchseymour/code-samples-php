var m_radialTree = {};

function canDrillDown(d){

	if (typeof d['parent'] == 'undefined')
		return false;
					
	if (typeof d['children'] == 'undefined' || ! d['children'])
		return false;
		
	return true;
}

m_radialTree.nodeEnter = function(node){

	var options = m_radialTree.getOptions();
	
	node.attr("class", function(d){
		
		var nodeClass = "node popup-link ";
		
		(d.health < 100) ? nodeClass += 'unhealthy' : nodeClass += 'healthy';
			
		return nodeClass;
	  
	  })
	  .attr("data-ogroup", function(d){return d.group})
	  .attr("data-oid", function(d){return d.name})
	  .attr("data-defaultEventsEnabled", "no")
	  .attr("data-health", function(d){return d.health})
	  .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })
	  .on("click", function(d){

		return;
		// hide children
		if (d.children) {
			d._children = d.children;
			d.children = null;
		} else {
			d.children = d._children;
			d._children = null;
		}
		
		// update the tree
		m_radialTree.update(d);

	  });
	
	node.append("circle")
	  .attr("r", options.circleRadius)
	  .attr("class", "popup-link")
	  .attr("data-ogroup", function(d){return d.group})
	  .attr("data-oid", function(d){return d.name})
	  .attr("data-defaultEventsEnabled", "no")
	  .attr("href", function(d){
	  
		return "/support/detail&popup=true&group=" + d.group + "&id=" + d.name;

	  })
      .style("stroke", function(d){
      
		  if (d.nodeColor)
			  return d.nodeColor;
          
      })
      .attr("title", function(d){
      
		  return "Click here to view the " + d.name + " => " + d.group + " report";
          
      });

	node.append("text")
	  .attr("dy", ".31em")
	  .attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
	  .attr("transform", function(d, i) { 
			
			var rotate = 180;
			
			if (i == 0)
				rotate = -90;
			
			return d.x < 180 ? "translate(8)" : "rotate(" + rotate + ")translate(-8)"; 
			
	  })
	  .attr('class', function(d){
		
		if (!canDrillDown(d))
			return "popup-link";
	  
	  })
	  .attr('href', function(d){
		
		if (!canDrillDown(d))
			return "/support/detail&popup=true&group=" + d.group + "&id=" + d.name;
			
		
	  })
	  .text(function(d) { return d.name; })
	  .style("font-size", function(d, i){
	  
		  if (i == 0)
			  return "14px";
	  })
      .style("stroke-width", function(d, i){
      
		  if (i == 0)
			  return "5px";
      })
	  .on('click', function(d){
	  
			var id = d.name;
			var group = d.group;
			
			if (!canDrillDown(d))
				return;
				
			window.open('support/tree?id=' + id + '&group=' + group, '_blank');  
			
	  });
}

m_radialTree.linkEnter = function (link){
	
	var options = m_radialTree.getOptions();
	var svg = options.svg;
	
	var diagonal = d3.svg.diagonal.radial()
		.projection(function(d) { return [d.y, d.x / 180 * Math.PI]; });
	
	link.append("path")
		.attr("class", "link popup-link")
		.attr("href", function(d){
			return "/support/detail&popup=true&group=" + d.target.group + "&id=" + d.target.name;
		})
		.attr("data-ogroup", function(d){return d.target.group})
		.attr("data-oid", function(d){return d.target.name})
		.attr("data-d3ColorOverride", function(d){

			if(d.target.linkColor){
			  
				if(options.d3ColorOverride[d.target.linkColor])
					return options.d3ColorOverride[d.target.linkColor];
					
				return d.target.linkColor;
			 }

		})
		.attr("d", diagonal)
		.style("stroke", function(d){
		 
		if(d.target.linkColor){
		  
			if(options.d3ColorOverride[d.target.linkColor])
				return options.d3ColorOverride[d.target.linkColor];
				
			return d.target.linkColor;
		 }
		});

}

m_radialTree.setOptions = function(options){

	m_radialTree.options = options;
};

m_radialTree.getOptions = function(){

	if (typeof m_radialTree.options !== 'undefined')
		return m_radialTree.options;
		
	return {};
};

m_radialTree.update = function (d){
	
	var options = m_radialTree.getOptions();
	
	var svg  = options.svg;
	var root = options.root;
	var tree = options.tree;

	var i = 0;
	var duration = 1000;
	var source = d;
	
	var diagonal = d3.svg.diagonal.radial()
		.projection(function(d) { return [d.y, d.x / 180 * Math.PI]; });
	
	// Compute the new tree layout.
	var nodes = tree.nodes(root),
		links = tree.links(nodes);

	// Update the nodes…
	var node = svg.selectAll("g.node")
		.data(nodes, function(d) { return d.id || (d.id = ++i); });

	// Enter any new nodes at the parent's previous position.
	var nodeEnter = node.enter().append("g");
	m_radialTree.nodeEnter(nodeEnter, options);

	// Transition nodes to their new position.
	var nodeUpdate = node.transition()
		.duration(duration)
		.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })

	nodeUpdate.select("circle")
		.attr("r", 4.5)
		.style("fill", function(d) { return d._children ? d.nodeColor : "#fff"; });

	nodeUpdate.select("text")
      .style("fill-opacity", 1);
		  
	var nodeExit = node.exit().transition()
		.duration(duration)
		.remove();

	nodeExit.select("circle")
		.attr("r", 1e-6);

	nodeExit.select("text")
		.style("fill-opacity", 1e-6);

	// Update the links…
	var link = svg.selectAll("path.link")
			.data(links, function(d) { return d.target.id; });
		
	// Enter any new links at the parent's previous position.
	var links = link.enter().insert("path", "g")
	  .attr("class", "link")
	  .attr("d", function(d) {
		var o = {x: source.x0, y: source.y0};
		return diagonal({source: o, target: o});
		
	  }).style("stroke", function(d){
		 
		if(d.target.linkColor){
		  
			if(options.d3ColorOverride[d.target.linkColor])
				return options.d3ColorOverride[d.target.linkColor];
				
			return d.target.linkColor;
		 }
		});

	// Transition links to their new position.
	link.transition()
	  .duration(duration)
	  .attr("d", diagonal);

	// Transition exiting nodes to the parent's new position.
	link.exit().transition()
	  .duration(duration)
	  .attr("d", function(d) {
		var o = {x: source.x, y: source.y};
		return diagonal({source: o, target: o});
	  })
	  .remove();

	// Stash the old positions for transition.
	nodes.forEach(function(d) {
		d.x0 = d.x;
		d.y0 = d.y;
	});

	
}

function drawRadialTree(selector, jsonStr, userOpts){
	
	// Default options
	var defaults = {
		"angle" 		  : 360,
		"diameter" 		  : 960,
		"d3ColorOverride" : {},
		"circleRadius"	  : 4.5
	};
	
	if (!userOpts) 
		userOpts = {};
	
	// Merge the user options
    var options = $.extend({}, defaults, JSON.parse(userOpts));	
	
	if (options.d3ColorOverride)
		$.m_d3ColorOverride = options.d3ColorOverride;
	else
		$.m_d3ColorOverride = {};
		
	var diameter = options.diameter;

	var tree = d3.layout.tree()
		.size([options['angle'], diameter / 2 - 120])
		.separation(function(a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });

	var svg = d3.select(selector).append("svg")
		.attr("width", diameter)
		.attr("height", diameter - 150)
	  .append("g")
		.attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

	var root  = JSON.parse(jsonStr);
	var nodes = tree.nodes(root),
	    links = tree.links(nodes);
		
	options.svg  = svg;
	options.tree = tree;
	options.root = root;
	m_radialTree.setOptions(options);

	var links = svg.selectAll(".link")
		.data(links)
		.enter();
		
	m_radialTree.linkEnter(links, options);

	var node = svg.selectAll(".node")
	  .data(nodes)
	.enter().append("g");
	
	
	m_radialTree.nodeEnter(node, options);
	
	d3.select(self.frameElement).style("height", diameter - 150 + "px");

}
