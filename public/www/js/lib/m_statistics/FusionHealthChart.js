var _fch_options;

$(function(){

	$('.fusionHealthLine').waitUntilExists(function(){
		
		$('.datepicker').datepicker();
		
		var options={type:"area2d", height:"350",dataFormat:"json",dataSource:{chart:{manageresize:"1",bgcolor:"FFFFFF",bgalpha:"0",showborder:"1",upperlimit:"100",lowerlimit:"0",numbersuffix:"",valueabovepointer:"1",pointerontop:"1",pointerradius:"6",charttopmargin:"48",chartbottommargin:"20",chartleftmargin:"20",chartrightmargin:"20",outCnvBaseFontColor:"555",canvasBorderColor:"FFFFFF",alternateHGridColor:"FFFFFF",canvasPadding:"70",showLabels:"1",labelDisplay:"Rotate",labelSlant:"1",labelPadding:"4",slantLabels:"1",showYAxisValues:"1",interactiveLegend:"1",showValues:"1",yAxisMinValue:"0",yAxisMaxValue:"100","plotFillColor ":"cccccc",plotGradientColor:" "},styles:{definition:[{name:"CaptionFont",type:"font",size:"12"}],application:[{toobject:"CAPTION",styles:"CaptionFont"},{toobject:"SUBCAPTION",styles:"CaptionFont"}]}}}
		
		$('.fusionHealthLine').each(function(){
			
			// Dimensions
			var w = $(this).attr('data-fusionWidth');
			var h = $(this).attr('data-fusionHeight');
			
			var chartType   = $(this).attr('data-fusionChartType');
			var colors 		= $(this).attr('data-fusionColors');
			var seriesType  = $(this).attr('data-fusionSeriesType');
			var series 		= $.parseJSON($(this).attr('data-fusionSeries'));
			
			if (!series)
				series = new Array();
			
			if (seriesType == 'default') {
			
				options['dataSource']['data'] = series[0]['data'];
			
			} else if (seriesType == 'categorical') {
				
				var count = 0;
				var categories = [];
				var dataSet= [];
				
				$.each(series, function(index, objects){
					
					count++;
					var data = [];
					var seriesName = objects['name'];
					
					$.each(objects['data'], function(i, object){
						
						if (count == 1)
							categories.push({"label" : object['label']});
							
						data.push({"value" : object["value"]});
						
					});
					
					dataSet.push({"seriesname" : seriesName, "data": data});
				
				});
				
				options['dataSource']['categories'] = [{"category": categories}];
				options['dataSource']['dataset'] = dataSet;
				//console.log(options);
			
			}
			
			if (w && w !== '')
				options['width'] = $(this).attr('data-fusionWidth');
				
			
			if (h && h !== '')
				options['height'] = $(this).attr('data-fusionHeight');
				
			if (chartType && chartType !== '')
				options['type'] = chartType;
				
			if (colors && colors !== '')
				options['dataSource']['paletteColors'] = colors;
				
			$(this).insertFusionCharts(options);
				
			window._fch_options = options;
		});
		
	
	});
	
		$('#changeFusion').unbind('change').live('change', function(){
		
			var val = $(this).val();
			var options = window._fch_options;
			
			if (val){
			
				options['type'] = val;					
				
				var fusionChart = $(this).parent().find('.fusionHealthLine');
				fusionChart.insertFusionCharts(options);
			
			}
		});

});