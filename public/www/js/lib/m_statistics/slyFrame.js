var _slyCount = 0;

function handleActive(index, $frame){

	// After the frame slides, update the buttons if necessary
	var length = $frame.find('.frameContainer').length;
	var nextIndex = index + 1;
	var prevIndex = index - 1;
	
	$('.activeSlyLink').removeClass('activeSlyLink');
	$('.slyPageLink[data-pageindex=' + index + ']').addClass('activeSlyLink');
	
	if (index + 1 == length) {
		
		// we are on the last frame
		$('.next').hide();
		nextIndex = 0;
	
	} else if (index == 0) {
		
		// we are on the first frame
		prevIndex = length - 1;
		$('.prev').hide();
	
	} else {
	
		$('.prev, .next').show();
	}
	//console.log('previous index: ' + prevIndex + ', next index: ' + nextIndex);
	
	var nextFrame = $frame.find('.frameContainer').eq(nextIndex);
	var prevFrame = $frame.find('.frameContainer').eq(prevIndex);
	
	var nextTitle = '';
	var prevTitle = '';
	
	if (nextFrame.attr('data-frameTitle'))
		nextTitle = nextFrame.attr('data-frameTitle');
		
	if (prevFrame.attr('data-frameTitle'))
		prevTitle = prevFrame.attr('data-frameTitle');
	
	//console.log('previous title: ' + prevTitle + ', next title: ' + nextTitle);
	
	$('.controls .nextTitle').text(nextTitle);
	$('.controls .prevTitle').text(prevTitle);
			
}


$(function(){

	$(".m-sly-frame").unbind().waitUntilExists(function(i, k){
		
		/**
		 * Scrollable frames
		 */
		var $frame = $('div.scrollFrame');
		var $wrap  = $frame.parent();

		var options = {
			horizontal: 1,
			itemNav: 'basic',
			smart: 1,
			disabledClass: 'scrollBtnHidden',
			mouseDragging: 0,
			touchDragging: 1,
			releaseSwing: 1,
			startAt: 0,
			scrollBy: 0,
			speed: 600,
			easing: 'easeOutExpo',
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1,
			//Buttons
			prev: $wrap.find('.prev'),
			next: $wrap.find('.next')
		};
		
		if ( window._sly ) {
			
			console.log('destroying previous instance of sly');
			window._sly.destroy();
			window._sly = false;
			
		}
		
		
		var sly = new Sly($frame, options, {
			

			load   : function(){
				
				$(".m-sly-frame").hide().css('visibility', 'visible').fadeIn(700);
				var timeInit = $('.m-sly-frame').attr('data-timeinit');
			
				$('.slyPageLink[data-timeinit=' + timeInit + ']').eq(0).addClass('activeSlyLink');
			
				var slyLinks = $('.slyPageLink[data-timeinit=' + timeInit + ']').on('click', function(){
				
					$('.activeSlyLink').removeClass('activeSlyLink');
					$(this).addClass('activeSlyLink');
					var index = $(this).attr('data-pageindex');
					sly.activatePage(index);
				});
			}
		});
			
		sly.init();
		window._sly = sly;
		
	}, true);
});
