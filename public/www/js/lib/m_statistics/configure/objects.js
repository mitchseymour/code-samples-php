$(function(){

	$('#idType').change(function(){
	
		var val = $(this).val();
		
		if (!val){
		
			$('.hideUnlessDynamic, hideUnlessStatic').hide();
			return;
		
		}
	
		if (val == 'dynamic'){
		
			$('.hideUnlessStatic').hide();
			$('.hideUnlessDynamic').fadeIn();
			$('#save').hide();
		
		}
		
		if (val == 'static'){
		
			$('.hideUnlessDynamic').hide();
			$('.hideUnlessStatic, #save').fadeIn();
		
		}
	});
	
	// Get the IDs when the group changes
	$('#parentGroup').change(function(){
		
		var val = $(this).val();

		if (!val) {
			$('#parentID').find('option').remove();
			$('#parentID').append('<option>Select a group first</option>');
			return;
		}	
		
		$('#parentID').find('option').remove();
		
		if (val == 'None')
			$('#parentID').append("<option></option>");
		
		$.ajax({
			
			url: 'support/configure/get-ids/',
			type: 'GET',
			data: {group: val},
			dataType: 'json',
			success: function(response){
				
				var option = $('<option>').val('all').text('*');
				$('#parentID').append(option);
				
				$.each(response, function(index, id){
					
					option = $('<option>').val(id).text(id);
					$('#parentID').append(option);
				
				});
			
			},
			error: function(xhr){
			
				
				console.log(xhr.responseText);
			
			}
			
		});
		
	});
	
	$('#runQuery').click(function(){
		
		var val = $.trim($('#query').val());
		
		if (!val){
		
			alert('Invalid query');
			return;
		}
		
		$.ajax({
			
			url: 'support/configure/run-query/',
			type: 'GET',
			data: {sql: val},
			dataType: 'json',
			success: function(response){
				
				$('#dataColumn, #idColumn, #timeColumn').find('option').remove();
				
				if (response['status'] == 0){
					
					alert(response['message']);
					return;
				
				}
				
				var columns = response['columns'];
				
				$.each(columns, function(index, c){
				
					var option = $('<option>').val(c).text(c);
					$('#parentIDColumn').append(option);
					$('#objectIDColumn').append(option.clone());
					
				
				});
				
				$('.hideUntilQuery, #save').fadeIn();
			
			},
			error: function(xhr){
			
				
				console.log(xhr.responseText);
			
			}
			
		});
	
	});
	
});