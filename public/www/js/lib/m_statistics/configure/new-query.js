$(function(){

	$('#group').change(function(){
	
		var val = $(this).val();
		
		if (!val) {
			$('#ids').find('option').remove();
			$('#ids').append('<option>Select a group first</option>');
			return;
		}	
		
		$('#ids').find('option').remove();
		
		$.ajax({
			
			url: 'support/configure/get-ids/',
			type: 'GET',
			data: {group: val},
			dataType: 'json',
			success: function(response){
				
				var option = $('<option>').val('').text('*');
				$('#ids').append(option);
				
				$.each(response, function(index, id){
				
					option = $('<option>').val(id).text(id);
					$('#ids').append(option);
				
				});
			
			},
			error: function(xhr){
			
				
				console.log(xhr.responseText);
			
			}
			
		});
	
	});
	
	$('#runQuery').click(function(){
		
		var val = $.trim($('#query').val());
		
		if (!val){
		
			alert('Invalid query');
			return;
		}
		
		$.ajax({
			
			url: 'support/configure/run-query/',
			type: 'GET',
			data: {sql: val},
			dataType: 'json',
			success: function(response){
				
				$('#dataColumn, #idColumn, #timeColumn').find('option').remove();
				
				if (response['status'] == 0){
					
					alert(response['message']);
					return;
				
				}
				
				var columns = response['columns'];
				var timeColumns = response['timeColumns'];
				
				$.each(columns, function(index, c){
				
					var option = $('<option>').val(c).text(c);
					$('#dataColumn').append(option);
					$('#idColumn').append(option.clone());
					
					if (timeColumns[c])
						$('#timeColumn').append(option.clone());
				
				});
				
				$('.hideUntilQuery').fadeIn();
				/*
				var option = $('<option>').val('all').text('*');
				$('#ids').append(option);
				
				$.each(response, function(index, id){
				
					option = $('<option>').val(id).text(id);
					$('#ids').append(option);
				
				});
				*/
				
				console.log(response);

			
			},
			error: function(xhr){
			
				
				console.log(xhr.responseText);
			
			}
			
		});
	
	});

});