$(function(){

	// Initialize tooltips
	$('.tooltip').tooltip({
		position: {my: 'left center', at: 'right+11 center'},
		track: false,
		show: 100
	});

	// Addition shared configuration code should be placed here
	
});