/*! Mitch Seymour */

function mainfunc(func){
    this[func].apply(this, Array.prototype.slice.call(arguments, 1));
}

function submitBean(el, callback){

    var data   = el.serialize();
    var method = 'GET'; // default
    var url    = el.attr('action');

    if (el.attr('method'))
        method = el.attr('method');

    $.ajax({

        url: url,
        type: method,
        data: data,
        dataType: 'json',
        success: function(response){

            var status = $.trim(response.status).toLowerCase();

            if (status == 'success') {

                var bean = response.bean;

                if (callback)
                    mainfunc(callback, bean);
            }

        },
        error: function(xhr){
            alert(xhr.responseText);
        }
    });
}

$(function(){

	$('.r-ajax-form').submit(function(e){
	
	    e.preventDefault();
	    
	    // check to see if there is a callback
	    var callback = $(this).find("#rCallback");
	    
		if (callback && callback.val())
			callback = callback.val();
		else
			callback = false;
	
	    submitBean($(this), callback);

    });
});