/*
A simple jQuery plugin for rendering Google Maps from raw HTML

About License:
Copyright (C) 2013 Mitchell Seymour
You may use gmapify plugin under the terms of the MIT Licese.
*/
(function($){

	var mapObj = this;
	var geocache = {};
	var polyline = [];
	
	var addMarker = function(map, address){
	
		if (geocache[address]) {
		
			// we've already geocoded this address, so used the cached value
			var marker = new google.maps.Marker({
      				position: geocache[address], 
      				map: map,
      				draggable: true
  			});
		
		} else {
		
			geocode(address, function(latlng){
		
				// geocode the address
				var marker = new google.maps.Marker({
      				position: latlng, 
      				map: map,
      				draggable: true
  				});
  				
  				// add the address to the cache
  				geocache[address] = latlng;
			});
		}
	}
	
	var addPolyline = function(map, line) {

    	var polyline = [],
        total = $(line).length,
        count = 0,
        addLine = function(){
            
            var linePath = new google.maps.Polyline({
    			path: polyline,
    			strokeColor: '#FF0000',
    			strokeOpacity: 1.0,
    			strokeWeight: 2
  			});

  			linePath.setMap(map); 
            
        };

    	$.each(line, function(index, address){
        	geocode(address, function(latlng){
            	polyline.push(latlng);
            	count++;
            	if(count === total){
                	addLine();
            	}
        	});
   	 });
	}
	
	var drawMap = function(map, options, callback) {
	
		var gmap = new google.maps.Map(map, options);
		callback(gmap);
	}
	
	var isInt = function(n) {
   		return typeof n === 'number' && parseFloat(n) == parseInt(n, 10) && !isNaN(n);
	}
	
	var geocode = function(address, callback) {

		var geocoder = new google.maps.Geocoder();
		var result = "";
		
		geocoder.geocode( {'address': address}, function(results, status) {
      
			if (status == google.maps.GeocoderStatus.OK)
        		result = results[0].geometry.location;
			else
        		result = "Geocode was not successful for the following reason: " + status;
       
       		callback(result);
		});
	}
	
	
	$.fn.gmapify = function(){
		
		var center = this.attr('data-center');
		var map = this.get(0);
		var mapObj = this;
		var optionsClass = this.attr('data-options');
		var options = $('.' + optionsClass + ' li');
		var type = this.attr('data-type');
		var types = {};
		
		types['hybrid']    = google.maps.MapTypeId.HYBRID;
		types['roadmap']   = google.maps.MapTypeId.ROADMAP;
		types['satellite'] = google.maps.MapTypeId.SATELLITE;
		types['terrain']   = google.maps.MapTypeId.TERRAIN;
		
		if (type) {
			if (types[type])
				type = types[type];
		} else {
			type = types['terrain']; // default
		}
		
		var mapOptions = {};
		
		$.each(options, function(key, val){
		
			var full = $(this).text();
			var arr = full.split(':');
			
			if (arr.length == 2){
				
				var o = arr[0];
				var v = $.trim(arr[1]).toLowerCase();
				
				if (!isNaN(v))
					v = parseInt(v);
				else if (v=="false")
					v = false;
					
				mapOptions[o] = v;
			
			} else if (arr.length == 1){
			
				var o = arr[0];
				var v = true;
				
				mapOptions[o] = v;
			
			}
		});
		
		return this.each(function(){
		
			geocode(center, function(latlng) {
				
				// default options
				var defaultMapOptions = {
    				center: latlng, 
       				zoom: 14,
        			mapTypeId: type,
        			panControl: false,
  					mapTypeControl: false,
  					scaleControl: false,
  					streetViewControl: false,
  					overviewMapControl: false
    			};
    			
    			var options = $.extend({}, defaultMapOptions, mapOptions);
    		
    			drawMap(map, options, function(gmap){
    			
    				// add markers
    				var markerClass = mapObj.attr('data-markers');	
    				var markers = $('.' + markerClass + ' li');
					markers.each(function(){	
						var address = $.trim($(this).text());
						addMarker(gmap, address);
					});
					
					// polylines
					var linesClass = mapObj.attr('data-polylines');	
    				var linePoints = $('.' + linesClass + ' li');
    				var line = [];
					linePoints.each(function(){	
						var address = $.trim($(this).text());
						line.push(address);
					});
					addPolyline(gmap, line);
					
    			});
    		
			});
		
		});	
		
	}
})(jQuery);

$('#map').gmapify();

$('#mapBtn').toggle(function(){

	$('#map').attr('data-center', 'New York, NY').attr('data-type', 'terrain').gmapify();
}, function(){

	$('#map').attr('data-center', 'Atlanta, GA').attr('data-markers', 'atl-markers').gmapify();;
	
});
