$(function(){

	new DialogFx('#dialog-template', {
		animation: 'don',
		trigger: '.prereqs',
		closeSelector: '#close',
		open: function(dialog){
			
			var trigger = dialog.getLastTrigger(),
				course  = $(trigger).attr('data-course') || null;
				
			dialog.el.querySelector('.dialog__content').innerHTML = "Loading...";
            
            //setTimeout(function(){
			$.ajax({
				
				url: 'prereqs/get',
				data: { course: course },
				success: function(response){
					
					dialog.el.querySelector('.dialog__content').innerHTML = response;
				
				},
				error: function(xhr){
					
					dialog.el.querySelector('.dialog__content').innerHTML = 'Please try again later';
					console.log(xhr.responseText);
				}
				
			});
			//}, 3000);
			
		}
	});
	
	/*
	$('.course-date').on('click', function(){
	
	    var tentative =  $(this).hasClass('tentative'),
	        day = $(this).attr('data-day'),
	        course = $(this).attr('data-course');
	    
	    if (tentative || !day || !course)
	        return;
	        
	    window.location.assign('/details?course=' + course);
	
	});
	*/
	

});