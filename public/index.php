<?php
/**
 * Front Controller
 *
 * A very simple front controller for mapping requests to
 * the appropriate controllers and views.
 *
 * Sample Usage:
 *
 * FrontController::run(); // place this in index.php to handle all requests
 *
 * @author Mitch Seymour <me@mitchseymour.com>
 * @version 1.0.4
 */
 
class FrontController {

	/**
	 * Clean path
	 *
	 * @return string $cleanPath - The cleaned path
	 */
	public static function cleanPath(){
		
		$args = func_get_args();
		$path = implode(DIRECTORY_SEPARATOR, $args);
		
		return strtr($path, array('/' => DIRECTORY_SEPARATOR, '\\' =>  DIRECTORY_SEPARATOR));
	
	}
	
	/**
	 * Controller method exists
	 *
	 * Checks to see if a controller class and method exists
	 *
	 * @return boolean $exists
	 */
	public static function controllerMethodExists($controller, $fn){
		
		$ds = DIRECTORY_SEPARATOR;
		
		$paths = explode(PATH_SEPARATOR, get_include_path());

		foreach ($paths as $path){
				
			$file = self::cleanPath($path, "controllers", $controller . ".php");
			
			if (file_exists($file)){
			
				include_once($file);

				// since we found a controller, try to call the relevant method
				$class = strtr($controller, array('.php' => '')) . "_Controller";
			
				if (class_exists($class) && method_exists($class, $fn)){			
					return $class;
				}
			
			}	
		}
		
		return false;
	
	}
		
	/**
	 * Get route
	 *
	 * Extracts the route (i.e. the requested URL) for the current request. Note,
	 * the .htaccess file in this directory specifies the name of the GET parameter
	 * that holds the route information
	 *
	 * @return string $route
	 */
	private static function getRoute(){
	
		isset($_GET['_q']) ? $route = ltrim($_GET['_q'],'/') : $route = 'index';
		return $route;
	}
	
	/**
	 * Handle request
	 *
	 * Maps the client's request to either a controller of view
	 *
	 * @return void
	 */
    private static function handleRequest($route=false){
				
		// If a route wasn't provided, extract it
		if (!$route)
			$route = self::getRoute();
			
		if(!$route)
			return false;

		$pathArr = explode('/', $route);
		
		$path = array_shift($pathArr);
		
		// the next parameter is the function name, or "index" if it doesn't exist
		if(!$fn = array_shift($pathArr)) {
			$fn = "index";
		}
		
		$pathInfo = pathinfo($route);
		
		if (!isset($pathInfo['extension']))
			$route = rtrim($route, '/') . '.php';
		
		// format the function name. for example, "new-blog" would become "newBlog"
		$fn = explode('-', $fn);
		$fn = implode('', array_map('ucfirst', $fn));
		
		// aliases for home
		if (!$path || $path == 'public' || $path == 'home' || $path == '') 
			$path = 'index';
		
		
		$paths = explode(PATH_SEPARATOR, get_include_path());
		
		if ($class = self::controllerMethodExists($path, $fn)){
			
			// A controller and method were mapped, so call it
			return call_user_func(array($class, $fn));
		
		} else if ($viewFile = self::viewExists($route)){
			
			// A view exists, so include it
			include $viewFile;
			return true;
		}
		
		Page::notFound($path);

    }
	
	/**
	 * Init
	 *
	 * Initializes the current environment by including the required files,
	 * setting error handling functions, etc
	 *
	 * @return void
	 */
    private static function init(){
	
		// Set the include path to the current directory
		set_include_path(dirname(__FILE__) . PATH_SEPARATOR . '.');
		
		// compress contents of output buffer if ob_gzhandler is available
		if(!ob_start("ob_gzhandler")) {
			ob_start();
		}
		
		// these parameters are used for mobile apps
		if (isset($_GET['_token']) && isset($_GET['_device'])){
			
			$_SESSION['_device']  = $_GET['_device'];
			$_SESSION['_token']   = $_GET['_token'];
			$_SESSION['_version'] = $_GET['_version'];
			
		}
		
		// These files are needed for most requests
		require('util/xhandler.php');
		require('etc/parsers/Config.php');
		require('view/View.php');
		require('view/Page.php');
		require('util/ArrayColumn.php');
		require('util/Cache.php');
        
		
		$settings = Config::getApplicationSettings();
		
		// Start sessions automatically if the autoStartSessions settings == true
		if (isset($settings['autoStartSessions']) && (bool) $settings['autoStartSessions'] == true){
			if (session_status() == PHP_SESSION_NONE) {
				session_start();
			}
		}

		// Set error handlers
		set_error_handler(array('xhandler', 'handleError'), E_ALL);
		set_exception_handler(array('xhandler', 'handleException'));
		register_shutdown_function(array('xhandler', 'handleFatal'));
		
		
		// set the timezone to whatever value is provided in the settings file
		if (isset($settings['timezone'])){
			date_default_timezone_set($settings['timezone']);
		}

    }
	
	/**
	 * Run
	 *
	 * Initializes the environment, and handles the incoming request
	 *
	 * @return void
	 */
	public static function run(){

		self::init();
		self::handleRequest();

    }
	
	/**
	 * View exists
	 *
	 * Checks to see if a view exists
	 *
	 * @return boolean $exists
	 */
	private static function viewExists($view){

		$ds = DIRECTORY_SEPARATOR;
		
		$paths = explode(PATH_SEPARATOR, get_include_path());

		foreach ($paths as $path){
				
				$file = self::cleanPath("view", "templates", $path, $view);
				
				if (file_exists($file)){
					return $file;
				}
		}
		
		return false;
	
	}

}

/**
 * Autoloader
 *
 * @author <mitchseymour@gmail.com>
 */
function App_Autoloader($classname) {
    
	$ds = DIRECTORY_SEPARATOR;
	
	$settings = Config::getApplicationSettings();
	$paths = explode(PATH_SEPARATOR, get_include_path());
	
	if (!isset($settings['autoloadDirectories'])){
		throw new Exception("Class {$classname} not found");
	}
	
    foreach ($settings['autoloadDirectories'] as $directory){
		
		foreach ($paths as $path){
		
			$file = FrontController::cleanPath($path, $directory, $classname . '.php');
			
			if (file_exists($file)){
				include($file);
				break;
			}
			
		}
	}

}


if (version_compare(PHP_VERSION, '5.1.2', '>=')) {

    // SPL autoloading was introduced in PHP 5.1.2
    if (version_compare(PHP_VERSION, '5.3.0', '>=')){
	
        spl_autoload_register('App_Autoloader', true, true);
		
    } else {
	
        spl_autoload_register('App_Autoloader');
	
	}

} else {

    /**
     * Fall back to traditional autoload for old PHP versions
     * @param string $classname - The name of the class to load
     */
    function __autoload($classname) {
        App_Autoloader($classname);
    }
}

FrontController::run();

?>
